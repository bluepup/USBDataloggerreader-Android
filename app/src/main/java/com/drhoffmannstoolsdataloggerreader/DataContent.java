package com.drhoffmannstoolsdataloggerreader;

/* DataContent.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

public class DataContent {
	private final static String TAG="DataContent";
	public final static int MAX_ANZDATA=100000;
	public int anzdata;
	public int anzsamples;
	public int nfiles;
	public double x[];
	public double y[];
	public double y2[];
	public double y3[];
	public double tp[];
	public ArrayList<Lueftungsevent> eventliste;
	public ArrayList<Label> labels;
	public int compression[];  /*Multiplizität der Daten*/
	public int begins[];
	public boolean y3enabled;


    /* Datenspuren haben gemeinsame X-Basis,
     * unterschiedliche namen, farbe*/

	public double xmin,xmax,ymin,ymax,y2min,y2max,y3min,y3max;
	public double mindx;

	public DataContent(final LoggerData ldata,final int loggertype) {
		x=new double[ldata.anzdata];
		y=new double[ldata.anzdata];
		if(loggertype==Logger.LTYP_TH || loggertype==Logger.LTYP_THP) y2=new double[ldata.anzdata];
		else y2=null;
		if(loggertype==Logger.LTYP_THP) y3=new double[ldata.anzdata];
		else y3=null;

		compression=new int[ldata.anzdata];	
		clear();
		for(int i=0;i<ldata.anzdata;i++) {
			if(loggertype==Logger.LTYP_TH) add(i,ldata.get_temp(i),ldata.get_rh(i));
			else if(loggertype==Logger.LTYP_THP) add(i,ldata.get_temp(i),ldata.get_rh(i),(double)Voltcraft.raw2p(ldata.p[i]));
			else add(i,ldata.get_temp(i));
		}
		eventliste=ldata.eventliste;
	}


	public DataContent(int maxanzdata) {
		x=new double[maxanzdata];
		y=new double[maxanzdata];
		y2=new double[maxanzdata];
		y3=new double[maxanzdata];
		compression=new int[maxanzdata];
		clear();
	}
	public DataContent() {
		x=new double[MAX_ANZDATA];
		y=new double[MAX_ANZDATA];
		y2=new double[MAX_ANZDATA];
		y3=new double[MAX_ANZDATA];
		compression=new int[MAX_ANZDATA];
		clear();
	}
	public void clear() {
		eventliste = null;
		labels=new ArrayList<Label>();
		anzsamples=anzdata=0;
		y3enabled=false;
		tp=null;
		begins=null;
		nfiles=0;
		xmin=2147483647;
		y3min=y2min=ymin=9999;
		y3max=y2max=ymax=xmax=-9999;
		mindx=60*60*24*365;
	}
	

	
	
	public void add_beg(int n) {
		if(begins==null) {
			begins=new int[1];
			begins[0]=n;
		} else {
			int a[]=new int[begins.length+1];
			for(int i=0;i<begins.length;i++) a[i]=begins[i];
			a[begins.length]=n;
			begins=a;
		}
	}

	public boolean add(double nx, double ny, double ny2) {
		x[anzdata]=nx;
		y[anzdata]=ny;
		y2[anzdata]=ny2;
		compression[anzdata]=1;
		if(anzsamples>0) {
			if(mindx>x[anzdata]-x[anzdata-1]) mindx=x[anzdata]-x[anzdata-1];
		}
		anzsamples++;

		if(xmin>nx) xmin=nx;
		if(xmax<nx) xmax=nx;
		if(ymin>ny) ymin=ny;
		if(ymax<ny) ymax=ny;
		if(y2min>ny2) y2min=ny2;
		if(y2max<ny2) y2max=ny2;

		if(anzdata<x.length-1) anzdata++;
		else return true;
		return false;

	}
	public boolean add(double nx, double ny) {
		x[anzdata]=nx;
		y[anzdata]=ny;
		compression[anzdata]=1;
		if(anzsamples>0) {
			if(mindx>x[anzdata]-x[anzdata-1]) mindx=x[anzdata]-x[anzdata-1];
		}
		anzsamples++;

		if(xmin>nx) xmin=nx;
		if(xmax<nx) xmax=nx;
		if(ymin>ny) ymin=ny;
		if(ymax<ny) ymax=ny;

		if(anzdata<x.length-1) anzdata++;
		else return true;
		return false;
	}



	public boolean add(double nx, double ny, double ny2, double ny3) {
		x[anzdata]=nx;
		y[anzdata]=ny;
		y2[anzdata]=ny2;
		y3[anzdata]=ny3;
		compression[anzdata]=1;
		if(anzsamples>0) {
			if(mindx>x[anzdata]-x[anzdata-1]) mindx=x[anzdata]-x[anzdata-1];
		}
		anzsamples++;

		if(xmin>nx) xmin=nx;
		if(xmax<nx) xmax=nx;
		if(ymin>ny) ymin=ny;
		if(ymax<ny) ymax=ny;
		if(y2min>ny2) y2min=ny2;
		if(y2max<ny2) y2max=ny2;
		if(y3min>ny3) y3min=ny3;
		if(y3max<ny3) y3max=ny3;

		if(anzdata<x.length-1) anzdata++;
		else return true;
		return false;
	}
	public void loaddatafile(String filename) {
		boolean skipfirstline=false;
		boolean isxml=false;
		boolean iscsv=false;
		Log.d(TAG,"read file: "+filename);
		InputStream in = null;
		String splitter=" ";
		if(filename.endsWith(".csv")) {
			iscsv=true;
			skipfirstline=true;
		} else if(filename.endsWith(".xml")) {
			isxml=true;
		}
		add_beg(anzdata);
		try { 
			File file= new File(Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_DOWNLOADS)+ File.separator + filename);
			in = new FileInputStream(file);
			if(in!=null)   {
				// prepare the file for reading
				InputStreamReader inputreader = new InputStreamReader(in);
				nfiles++;
				Label l=new Label();
				l.name=filename;
				l.idx=anzdata;
				labels.add(l);
				if(isxml) {
					//   		    	  ArrayList<Category> categories;
					XmlPullParser parser = Xml.newPullParser();
					parser.setInput(inputreader);
					int eventType = parser.getEventType();
					// 		    	  Category currentCategory = null;
					boolean done = false;
					while (eventType != XmlPullParser.END_DOCUMENT && !done) {
						String name = null;
						switch (eventType){
						case XmlPullParser.START_DOCUMENT:
							break;
						case XmlPullParser.START_TAG:
							name = parser.getName();
							if (name.equalsIgnoreCase("log")){
								//		    	                    categories = new ArrayList<Category>();
								//	    	                } else if (categories != null){
								//	    	                    if (name.equalsIgnoreCase("category"))
								//	    	                    currentCategory = new Category(parser.getAttributeValue(0)); 
							} else if (name.equalsIgnoreCase("sample")) {
								Log.d(TAG,"sample = "+parser.getAttributeName(0));
								Log.d(TAG,"sample = "+parser.getAttributeValue(0));
							} else if (name.equalsIgnoreCase("channel")) {
								Log.d(TAG,"channel = "+parser.getAttributeName(0));
								Log.d(TAG,"channel = "+parser.getAttributeValue(0));
							} else if (name.equalsIgnoreCase("value")) {
							}
							break;
						case XmlPullParser.TEXT:
							Log.d(TAG,"TEXT = "+parser.getText());
						case XmlPullParser.END_TAG:
							name = parser.getName();
							//	    	                if (name.equalsIgnoreCase("category") && 
							//	    	                    currentCategory != null){
							//	    	                    categories.add(0, currentCategory);
							//	    	                } else 
							if (name.equalsIgnoreCase("log")){
								done = true;
							} else if (name.equalsIgnoreCase("value")) {
								Log.d(TAG,"value = "+parser.getNamespace());
							}
							break;
						}
						eventType = parser.next();
					} 
				} else {
					BufferedReader reader = new BufferedReader(inputreader);
					String line;
					try {line = reader.readLine();} catch (IOException e) {reader.close();return;}

					while (line!=null) {
						// do something with the settings from the file
						if(!line.startsWith("#") && !skipfirstline) {
							double x,y=0,y2=0,y3=0;
							if(iscsv && line.contains(";")) splitter=";";
							else if(iscsv && line.contains("|")) splitter="|";
							else if(iscsv) splitter=",";
							else splitter=" ";
							String[] sep=line.split(splitter);
							try {
								x=Double.parseDouble(sep[0]);
								try {
									if(sep.length>1) y=Double.parseDouble(sep[1]);
								} catch (NumberFormatException e) {y=0;}
								try {
									if(sep.length>2) y2=Double.parseDouble(sep[2]);
								} catch (NumberFormatException e) {y2=0;}
								try {
									if(sep.length>3) y3=Double.parseDouble(sep[3]);
								} catch (NumberFormatException e) {y3=0;}
								if(add(x,y,y2,y3)) break;
							} catch (NumberFormatException e) {Log.e(TAG,"could not parse line. <"+line+">");}
						} else skipfirstline=false;
						try {line = reader.readLine();} catch (IOException e) {reader.close();return;}
					}
					reader.close();
				}
			}  
			// close the file again
			try {in.close();} catch (IOException e) {return;}  		
		} catch (FileNotFoundException e) {
			Log.e(TAG,"ERROR: filenotfound ");
			return;
		} catch (XmlPullParserException e) {
			Log.e(TAG,"ERROR: XML error ");
			return;
		} catch (IOException e) {
			Log.e(TAG,"ERROR: IO error ");
			return;
		} finally {
			if (in != null) {
				try {in.close();} catch (IOException e) {return;}
			}
		}
	}

	public void shrinkdata() {
		if(anzdata>1) {
			int i,s=0;
			int count=0;
			if(begins!=null) s=begins[begins.length-1];
			Log.d(TAG,"s="+s);
			for(i=1;i<anzdata;i++) {
				if(i>s && y[i]==y[count] && y2[i]==y2[count] && y3[i]==y3[count] && !(i==anzdata-1)) {
					compression[count]+=compression[i]; 
				} else {
					count++;
					x[count]=x[i];
					y[count]=y[i];
					y2[count]=y2[i];
					y3[count]=y3[i];
					compression[count]=compression[i];
				}

			}
			Log.d(TAG,"Data could be shrunk from "+anzdata+" to "+(count+1));
			anzdata=count+1;
		}
	} 
	public void pcorr(boolean dopcorr,float pcorrlatency, float pcorrfactor, boolean lowpassp,int lowpassporder) {
		if(y3enabled) {
			if(dopcorr && anzdata>2) {
				double t;
				double dt=x[1]-x[0];
				for(int i=0;i<anzdata;i++) {
					if(i>0) dt=x[i]-x[i-1];
					if(i<(int)(pcorrlatency/dt)) t=y[0];
					else t=y[i-(int)(pcorrlatency/dt)];
					if(y3[i]>0) y3[i]+=(t-20)*pcorrfactor;
				}
			}
			if(lowpassp) y3=sincfilter(y3,lowpassporder,x);	

		}
	}
	public void tpwater() {
		tp=new double[anzdata];
		int i;
		for(i=0;i<anzdata;i++) {
			tp[i]=Physics.water((float)y[i],(float)y2[i]);
		}
	}
	public void tpdewpoint() {
		tp=new double[anzdata];
		int i;
		for(i=0;i<anzdata;i++) {
			tp[i]=Physics.taupunkt((float)y[i],(float)y2[i]);
		}
	}
	public void tptderiv() {
		tp=new double[anzdata];
		int i;
		double dx,dy;
		for(i=0;i<anzdata-1;i++) {
			dx=x[i+1]-x[i];
			dy=y[i+1]-y[i];
			if(dx>0) tp[i]=Math.max(Math.min(dy/dx*3600,100),-100);
			else tp[i]=0;
		}
		tp=sincfilter(tp,4,x);
	}
	public void tphderiv() {
		tp=new double[anzdata];
		int i;
		double dx,dy;
		for(i=0;i<anzdata-1;i++) {
			dx=x[i+1]-x[i];
			dy=y2[i+1]-y2[i];
			if(dx>0) tp[i]=Math.max(Math.min(dy/dx*3600,100),-100);
			else tp[i]=0;
		}
		tp=sincfilter(tp,4,x);
	}
	public void tppderiv() {
		tp=new double[anzdata];
		int i;
		double dx,dy;
		for(i=0;i<anzdata-1;i++) {
			dx=x[i+1]-x[i];
			dy=y3[i+1]-y3[i];
			if(dx>0) tp[i]=Math.max(Math.min(dy/dx*3600,100),-100);
			else tp[i]=0;
		}
		tp=sincfilter(tp,4,x);
	}

	public void analyze() {
		int i;
		/*Finde raus, ob die dritte Spur da ist.*/
		y3enabled=false;
		if(y3!=null) {
			for(i=0;i<anzdata;i++) {
				y3enabled|=(y3[i]!=0.0);
				if(y3enabled) break;
			}
		}
	}

	private int probe(int i) {
		int j=i;
		while((x[j]-x[i]<5*60 || j-i<3) && j<anzdata-1) j++;
		return j;
	}
	private boolean probebeg(int n1,int n2) {
		if(begins!=null) {
			for(int i=0;i<begins.length;i++) {
				if(begins[i]>n1 && begins[i]<=n2) return true;
			}
		}
		return false;
	}	

	public int calc_events() {
		eventliste = new ArrayList<Lueftungsevent>();
		int j,kk=0;
		Lueftungsevent ev=new Lueftungsevent();
		ev.idx=-1;
		for(int i=0;i<anzdata-3;i++) {
			j=probe(i);
			double w1=Physics.water((float)y[i], (float)y2[i]);
			double w2=Physics.water((float)y[j],(float)y2[j]);
			if(j-i>2 && w2-w1<-0.05*w2 && y[j]-y[i]<-0.2 && !probebeg(i,j)) {
				if(kk==0 || i>kk) {
					if(ev.idx!=-1) {
						eventliste.add(ev);
						ev=new Lueftungsevent();
						Log.d(TAG,"Event at: "+x[ev.idx]);
					}

					ev.idx=i;
					ev.idx2=j;
					ev.dt=y[j]-y[i];
					ev.dr=w2-w2;
				} else  {
					w1=Physics.water((float)y[ev.idx], (float)y2[ev.idx]);
					ev.dr=Math.min(ev.dr,w2-w1);
					ev.dt=Math.min(ev.dt,y[j]-y[ev.idx]);
					ev.idx2=j;
				}
				kk=j;
			}
		}
		if(ev.idx!=-1) {
			eventliste.add(ev);
			Log.d(TAG,"Event at: "+x[ev.idx]);
		}
		Log.d(TAG,""+eventliste.size()+" Events gefunden.");
		return eventliste.size();
	}

	public boolean isinevent(double ss) {
		if(eventliste==null) return false;
		Lueftungsevent a;
		for(int j=0;j<eventliste.size();j++) {
			a=eventliste.get(j);
			if(ss>=x[a.idx] && ss<x[a.idx2]) return true;
		}
		return false;
	}


	public DataCondensat getCondensate(double dt) {
		double d=1;
		DataCondensat con=new DataCondensat(xmin,xmax,dt);
		for(int i=0;i<anzdata;i++) {
			if(i<anzdata-1) d=(x[i+1]-x[i])/compression[i];
			if(compression[i]>1) {
				for(int j=0;j<compression[i];j++)
					con.update(x[i]+(double)j*d,y[i],y2[i],y3[i],1);
			} else con.update(x[i],y[i],y2[i],y3[i],compression[i]);
		}
		con.update_events(this);
		return con;
	}
	// TODO: compression muss bei filter berücksichtigt werden.

	private static double[] filter(double a[],double h[],double x[]) {
		double b[]=new double[a.length];
		int i,j;
		double aa;
		double c,deltamin,deltamax;
		for(i=0;i<a.length;i++) {
			aa=0;
			c=0;
			deltamin=3600*24*400;
			deltamax=0;
			for(j=0;j<h.length;j++) {
				if(i+j-h.length/2>=0 && i+j-h.length/2<a.length) {
					aa+=h[j]*a[i+j-h.length/2];
					c+=h[j];
				}
				if(i+j-h.length/2>=0 && i+j-h.length/2<a.length-1) {
					deltamin=Math.min(x[i+j-h.length/2+1]-x[i+j-h.length/2],deltamin);
					deltamax=Math.max(x[i+j-h.length/2+1]-x[i+j-h.length/2],deltamax);
				}
			}

			if(c>0 && deltamin>0 && deltamax-deltamin<20*deltamin) b[i]=aa/c;
			else {
				// Log.d(TAG,"deltamin="+deltamin+" max="+deltamax);
				b[i]=a[i];
			}
		}
		return b;
	}

	private static double[] sincfilter(double a[],double order,double x[]) {
		double h[]=new double[32];
		int i;
		for(i=0;i<h.length;i++) {
			if(i-h.length/2==0) h[i]=1;
			else {
				if(order>0) h[i]=order*Math.sin((i-h.length/2)/order)/(i-h.length/2);
				else h[i]=0;
			}
		}
		return filter(a,h,x);
	}
}

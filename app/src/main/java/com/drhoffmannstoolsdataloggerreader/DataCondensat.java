package com.drhoffmannstoolsdataloggerreader;

/* DataCondensat.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import android.util.Log;

public class DataCondensat {
	private static final String TAG = DataCondensat.class.getSimpleName();
	public double startx,bin;
	public double y[],ymin[],ymax[];
	public double y2[],y2min[],y2max[];
	public double y3[],y3min[],y3max[];
	public int events[];
	public int samples[];
	public int anzdata;
	public DataCondensat(double xmin,double xmax,double dt) {
		startx=Math.floor(xmin/dt)*dt;
		bin=dt;
	    int maxanzdata=(int)((xmax-startx)/dt)+1;
	    Log.d(TAG,"dt="+dt);
		Log.d(TAG,"startx="+startx+" "+(xmax-startx)+" "+maxanzdata);
		y=new double[maxanzdata];
		y2=new double[maxanzdata];
		y3=new double[maxanzdata];
		samples=new int[maxanzdata];
		events=new int[maxanzdata];
		ymin=new double[maxanzdata];
		ymax=new double[maxanzdata];
		y2min=new double[maxanzdata];
		y2max=new double[maxanzdata];
		y3min=new double[maxanzdata];
		y3max=new double[maxanzdata];		
		anzdata=0;
		for(int i=0;i<maxanzdata;i++) {
			ymin[i]=y2min[i]=y3min[i]=9999;
			ymax[i]=y2max[i]=y3max[i]=-9999;
			
		}
	}
	
	public void update(double nx,double ny,double ny2,double ny3,int compression) {
		int i=(int)((nx-startx)/bin);
		double a,b;
		
		if(i>=0 && i<y.length) {
			if(samples[i]>0) {
 			  a=1/(1+(double)compression/(double)samples[i]);
			  b=1/(1+(double)samples[i]/(double)compression);
			} else {
				a=0;
				b=1;
			}
			if(i==0) Log.d(TAG,"a="+a+" b="+b+" c="+compression);
			y[i]=y[i]*a+ny*b;
			y2[i]=y2[i]*a+ny2*b;
			y3[i]=y3[i]*a+ny3*b;
			ymin[i]=Math.min(ny, ymin[i]);
			ymax[i]=Math.max(ny, ymax[i]);
			y2min[i]=Math.min(ny2, y2min[i]);
			y2max[i]=Math.max(ny2, y2max[i]);
			y3min[i]=Math.min(ny3, y3min[i]);
			y3max[i]=Math.max(ny3, y3max[i]);
			samples[i]+=compression;
			anzdata+=compression;
		}
	}
	public void update_events(DataContent data) {
		Lueftungsevent a;
		for(int i=0;i<events.length;i++) {
			events[i]=0;
			if(data.eventliste!=null) {
				for(int j=0;j<data.eventliste.size();j++) {
					a=data.eventliste.get(j);
				    if(data.x[a.idx]<startx+(i+1)*bin && data.x[a.idx]>=startx+i*bin)	events[i]++;
				}
			} 
		}
	}
}

package com.drhoffmannstoolsdataloggerreader;

/* PlotdataLoader.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import android.os.AsyncTask;
import android.util.Log;

public class PlotdataLoader  extends AsyncTask<Integer, Integer, Integer> {	
	private static final String TAG = "Plotdataloader";

	private int mFilesComplete,mAnzfiles;
	private boolean mCompleted=false;
	private boolean mCancelled=false;
	private boolean isalive=false;
	private PlottoolActivity mActivity;
	DataContent data;
	String[] mFileList;

	boolean[] mcheckitems; 
	boolean mdotaupunkt;
	int mbluecurvetype;
	boolean mdopcorr;
	float mpcorrfactor;
	float mpcorrlatency;
	boolean mlowpassp;
	int mlowpassporder;
	/**
	 * Return a new AsyncTask that is connected to the activity given. 
	 *
	 * @param context MainActivity - the activity that this task is working with
	 * @param numFiles int - the number of files to process 
	 */

	public PlotdataLoader (PlottoolActivity context, String[] FileList,boolean[] checkitems,
			boolean dotaupunkt,int bluecurvetype,
			boolean dopcorr,float pcorrfactor,float pcorrlatency,
			boolean lowpassp,int lowpassporder) {
		mdotaupunkt=dotaupunkt;
		mbluecurvetype=bluecurvetype;
		mdopcorr=dopcorr;
		mpcorrfactor=pcorrfactor;
		mpcorrlatency=pcorrlatency;
		mlowpassp=lowpassp;
		mlowpassporder=lowpassporder;

		mActivity = context;
		mAnzfiles=mFilesComplete=0;
		if(checkitems!=null) {
		  for(int i=0;i<checkitems.length;i++) {
			if(checkitems[i]) mAnzfiles++;
	  	  }
		}
		mFileList=FileList;
		mcheckitems=checkitems;
		data=new DataContent(100000);
	} 
	/**
	 * Disconnect the task from the activity it was set up to work with.
	 * Doing this means that no more calls are made to update the UI thread.
	 * This is done automatically in onCancelled and onPostExecutre when the task ends. 
	 * In the UI thread, you can use this method from your activity code.
	 * 
	 */

	public void disconnect () {
		if (mActivity != null) {
			mActivity = null;
			Log.d ("PlotLoader", "Plotloader has successfully disconnected from the activity.");
		}
	} 

	public boolean isAlive() {
		return isalive;
	}

	@Override
	protected Integer doInBackground(Integer... params) {
		int i=0;
		isalive=true;
		data.clear();
		if(mFileList!=null && mcheckitems!=null && mFileList.length>0) {
			for(i=0;i<mFileList.length;i++) {
				if(mcheckitems[i]) {
					// Show progress on the UI thread.  
					publishProgress(i*1000+100*mFilesComplete/mAnzfiles);
					data.loaddatafile(mFileList[i]);
					data.shrinkdata();
					mFilesComplete++;
				}
			}

			publishProgress(-1);
			data.analyze();
			if(data.y3enabled) { /*Wenn vorhanden und gewuenscht, wende Korrekturen an*/
				data.pcorr(mdopcorr, mpcorrlatency, mpcorrfactor,mlowpassp, mlowpassporder);
			}
			if(mdotaupunkt && data.anzdata>0) {
				if(mbluecurvetype==1) {
					data.tpdewpoint();
				} else if(mbluecurvetype==2) {
					data.tptderiv();
				} else if(mbluecurvetype==3) {
					data.tphderiv();
				} else if(mbluecurvetype==4) {
					data.tppderiv();
				} else if(mbluecurvetype==5) {
					data.tpwater();
				}
			}
			data.calc_events();
		}

		Log.d(TAG, "inBackground completed.");
		isalive=false;
		return i;
	}
	/**
	 * Respond to a progress update by updating something on the screen.
	 * This method runs in the UI thread so it is okay to do that.
	 */

	@Override 
	protected void onProgressUpdate(Integer... info) {
		if (mActivity != null) mActivity.updateProgress(info [0]);
	}

	/**
	 * Report that the task has been cancelled.
	 * This method runs in the UI thread.
	 */

	@Override 
	protected void onCancelled () {
		mCompleted = true;
		mCancelled = true;
		isalive=false;
		if (mActivity != null) mActivity.loadDataComplete (data, mCancelled);
		disconnect();
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (mActivity != null) mActivity.prepare_progressdialog();
	}
	/**
	 * Report that the task has finished.
	 * This method runs in the UI thread.
	 */

	@Override 
	protected void onPostExecute (Integer result)  {
		mCompleted = true;
		if (mActivity != null) mActivity.loadDataComplete (data, mCancelled);
		disconnect();
	}
	/**
	 * Change the activity that the task is associated with.
	 * Typically, this is used when there is a orientation configuration change 
	 * and an activity want to recreate its connection to an already running task.
	 *
	 * Note that the activity gets called with onTaskCompleted immediately if this task 
	 * has already completed its work or has already been cancelled.
	 * 
	 */

	public void resetActivity (PlottoolActivity activity) {
		mActivity = activity;

		// If the task has completed during the configuration change, assume the
		// activity does not know that and tell it.
		if (mCompleted && (mActivity != null)) {
			mActivity.loadDataComplete(data, mCancelled);
			disconnect();
		}
	} 	
}

package com.drhoffmannstoolsdataloggerreader;

/* Freetec.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

import android.util.Log;


public class Freetecwhite {
  private static final String TAG = "FREETECwhite";
  private static String loggerstatus="OK.!";

  private static final int CMD_OUT= 2;
  private static final int CMD_IN=  1;
  private static final int CMD_READ= 1;
  private static final int CMD_WRITE=0;


  private static final int CMD_TIMESTAMPS=4;
  private static final int CMD_CLEAR=     8;
  private static final int CMD_SETDATE= 0x11;
  private static final int CMD_SETTING= 0x22;

  private static final int CMD_READMEM= 0; 
  private static final int CMD_READVAL= 1;  

  /* Capabilities */
  
  public final static int capabilities=	Logger.CAPA_ROLLOVER|
  					Logger.CAPA_STARTTIME|
					Logger.CAPA_STOPTIME|
					Logger.CAPA_LCD30|
					Logger.CAPA_ACTUALVAL|
					Logger.CAPA_MINMAXVAL;


  public static int chk_conf(LoggerConfig config, int loggertype) {
    if(config.set_interval>255*60 ||config.set_interval<=0) return Error.ERR_INTERVAL;
    return Error.OK;
  }


  public static byte[] build_conf(Logger logger) {
    byte cbuf[]= new byte[256];
    return cbuf;
  }
  
  public static void clearData(Logger logger) {
    byte cbuf[];
    byte[] message = new byte[5];
    message[0] = (byte)CMD_OUT;
    message[1] = (byte)0x03;  /* Length*/
    message[2] = (byte)CMD_WRITE;
    message[3] = (byte)CMD_CLEAR;
    message[4] = (byte)(CMD_WRITE+CMD_CLEAR); /* checksum */
    logger.HIDwrite(message);
    logger.log2comproto("LoggerCMD: ClearData.",1);
    cbuf=logger.HIDread(61);  /* get ACK */
    if((cbuf[0]&0xff)!=CMD_IN || (cbuf[1]&0xff)!=2  || (((int)cbuf[1])&0xff)!=0xaa) logger.log2comproto("Protocol ERROR!",0);
  }
  
  
  private static void write_clock(Logger logger) {
    byte cbuf[]; 
    byte[] message = new byte[11];
    message[0] = (byte)CMD_OUT;
    message[1] = (byte)0x09;  /* Length*/
    message[2] = (byte)CMD_WRITE;
    message[3] = (byte)CMD_SETDATE;
    message[4] = bcdencode((byte)(logger.config.time_year-2000));
    message[5] = bcdencode((byte)(logger.config.time_mon));
    message[6] = bcdencode((byte)logger.config.time_mday);
    message[7] = bcdencode((byte)logger.config.time_hour);
    message[8] = bcdencode((byte)logger.config.time_min);
    message[9] = bcdencode((byte)logger.config.time_sec);

    int sum=0;
    for(int i=2;i<10;i++) sum+=message[i];
    message[10] = (byte)(sum); /* checksum */

    logger.HIDwrite(message);
    logger.log2comproto("LoggerCMD: SetDate.",1);
    cbuf=logger.HIDread(61);  /* get ACK */
    if((((int)cbuf[0])&0xff)!=CMD_IN || 
       (((int)cbuf[1])&0xff)!=2  || 
       (((int)cbuf[2])&0xff)!=0xaa) {
      logger.log2comproto("Protocol ERROR!",0);
    } else logger.log2comproto("<- ACK",1);
  }
  private static void write_timestamps(Logger logger) {
    byte cbuf[]; 
    Date dt=new Date();

    byte[] message = new byte[45];
    message[0] = (byte)CMD_OUT;
    message[1] = (byte)0x2b;  /* Length*/
    message[2] = (byte)CMD_WRITE;
    message[3] = (byte)CMD_TIMESTAMPS;

    for(int i=0;i<8;i++) {
      message[4+5*i] = bcdencode((byte)(dt.getYear()+1900-2000));
      message[5+5*i] = bcdencode((byte)(dt.getMonth()+1));
      message[6+5*i] = bcdencode((byte)dt.getDate());
      message[7+5*i] = bcdencode((byte)dt.getHours());
      message[8+5*i] = bcdencode((byte)dt.getMinutes());
    }
    message[4+5*0] = bcdencode((byte)(logger.config.starttime_year-2000));
    message[5+5*0] = bcdencode((byte)(logger.config.starttime_mon));
    message[6+5*0] = bcdencode((byte)logger.config.starttime_mday);
    message[7+5*0] = bcdencode((byte)logger.config.starttime_hour);
    message[8+5*0] = bcdencode((byte)logger.config.starttime_min);

    message[4+5*1] = bcdencode((byte)(logger.config.stoptime_year-2000));
    message[5+5*1] = bcdencode((byte)(logger.config.stoptime_mon));
    message[6+5*1] = bcdencode((byte)logger.config.stoptime_mday);
    message[7+5*1] = bcdencode((byte)logger.config.stoptime_hour);
    message[8+5*1] = bcdencode((byte)logger.config.stoptime_min);

    int sum=0;
    for(int i=2;i<44;i++) sum+=message[i];
    message[44] = (byte)(sum); /* checksum */

    logger.HIDwrite(message);
    logger.log2comproto("LoggerCMD: SetDate.",1);
    cbuf=logger.HIDread(61);  /* get ACK */
    if((cbuf[0]&0xff)!=CMD_IN || (cbuf[1]&0xff)!=2  || (((int)cbuf[2])&0xff)!=0xaa) logger.log2comproto("Protocol ERROR!",0);
    else logger.log2comproto("<- ACK",1);
  }
  
  private static void write_settings(Logger logger,int interval, float tempoffset, float humioffset) {
    byte cbuf[];
    int to=(int)(10*tempoffset+100);
    int ho=(int)humioffset+20;

    int flags=0;      /*  normal=0x2d  Flags */
    int rollover=0;   /* on=0x80*/

    if(logger.config.temp_unit==Logger.UNIT_C)                flags|=0x1;
    if(logger.config.date_format==Logger.DATEFORMAT_MMDDYYYY ||
       logger.config.date_format==Logger.DATEFORMAT_YYYYMMDD) flags|=0x2;
    if(logger.config.time_format==Logger.TIMEFORMAT_24H)      flags|=0x4;
    if((logger.config.stopcondition&Logger.STOP_MEMFULL)==0) {flags|=0x8; rollover=0x80;}
    if(logger.config.startcondition==Logger.START_TIME)       flags|=0x10;
    if(logger.config.displaystate!=Logger.DISPLAY_ON)         flags|=0x20;

    byte[] message = new byte[11];
    message[0] = (byte)CMD_OUT;
    message[1] = (byte)0x09;  /* Length*/
    message[2] = (byte)CMD_WRITE;
    message[3] = (byte)CMD_SETTING;
    message[4] = (byte)flags; 
    message[5] = (byte)((int)(interval/256)|rollover);
    message[6] = (byte)(interval&0xff);
    message[7] = (byte)(to/256);;
    message[8] = (byte)(to&0xff);
    message[9] = (byte)ho;

    int sum=0;
    for(int i=2;i<10;i++) sum+=message[i];
    message[10] = (byte)(sum); /* checksum */

    logger.log2comproto("LoggerCMD: Set Settings.",0);
    logger.log2comproto("### "+logger.bytearray2string(message),0);
    logger.HIDwrite(message);
    cbuf=logger.HIDread(61);  /* get ACK */
    if((cbuf[0]&0xff)!=CMD_IN || (cbuf[1]&0xff)!=2  || (((int)cbuf[2])&0xff)!=0xaa) {
      logger.log2comproto("Protocol ERROR! "+logger.bytearray2string(cbuf),0);
    } else logger.log2comproto("<- ACK",1);
  }


  private static byte[] read_mem(Logger logger,int adr, int len) {
    byte[] message = new byte[8];
    if(len>32) len=32;
    byte mem[]=new byte[len];
    byte cbuf[];
   
    message[0] = (byte)CMD_OUT;
    message[1] = (byte)0x06;  /* Length*/
    message[2] = (byte)CMD_READ;
    message[3] = (byte)CMD_READMEM;
    message[4] = (byte)((adr&0xff00)>>8);
    message[5] = (byte)(adr&0xff);
    message[6] = (byte)(len&0xff);
    int sum=0;
    for(int i=2;i<7;i++) sum+=message[i];
    message[7] = (byte)(sum); /* checksum */
    logger.HIDwrite(message);
    logger.log2comproto("read_mem: "+adr+" "+len,1);
    cbuf=logger.HIDread(61);
    int l=cbuf.length-2;
    if(l<len) len=l;
    if(len>0) {
      for(int i=0;i<len;i++) mem[i]=cbuf[i+2];
    }
    /* Checksumme auswerten */
    if(cbuf[0]!=CMD_IN || cbuf[1]!=(len+1)) logger.log2comproto("Protocol ERROR!",0);
    else {
      sum=0;
      for(int i=2;i<len+2;i++) sum+=cbuf[i];
      if((sum&0xff)!=(cbuf[2+len]&0xff))  logger.log2comproto("Protocol CHK ERROR!",0);
    }
    return mem;
  }
  
  public static void get_minmax_values(Logger logger) {
    byte[] cbuf=read_mem(logger,0x50,6);
    logger.maxval_temp=(float)(cbuf[0]*256+(cbuf[1]&0xff))/10-50;
    logger.maxval_humi=(float)cbuf[2]-20;
    logger.minval_temp=(float)(cbuf[3]*256+(cbuf[4]&0xff))/10-50;
    logger.minval_humi=(float)cbuf[5]-20;
  }
  
  public static void get_actual_values(Logger logger) {
    byte[] message = new byte[5];
    message[0] = (byte)CMD_OUT;
    message[1] = (byte)0x03;  /* Length*/
    message[2] = (byte)CMD_READ;
    message[3] = (byte)CMD_READVAL;
    int sum=0;
    for(int i=2;i<4;i++) sum+=message[i];
    message[4] = (byte)(sum); /* checksum */
    logger.HIDwrite(message);
    logger.log2comproto("read_val: ",1);
    logger.log2comproto("read_val: "+Logger.bytearray2string(message),2);

    byte cbuf[];
    cbuf=logger.HIDread(61);
    logger.log2comproto("read_val: "+Logger.bytearray2string(cbuf),2);
    if(cbuf[0]!=CMD_IN || cbuf[1]!=4) logger.log2comproto("Protocol ERROR!",0);

    /* Checksumme auswerten */
    sum=0;
    for(int i=2;i<3+2;i++) sum+=cbuf[i];
    if((sum&0xff)!=(cbuf[2+3]&0xff))  logger.log2comproto("Protocol CHK ERROR!",0);

    logger.actual_temp=(float)((cbuf[3]<<8)+(cbuf[4]&0xff))/10-50;
    logger.actual_humi=(float)cbuf[2]-20;
  }



  /* Read a whole block (bank) of 256 Bytes from Logger */
  private static byte[] read_block(Logger logger,int n) {
    byte cbuf[]=new byte[256];
    logger.log2comproto("Read Block #"+n,1);
    for(int i=0;i<0x100;i+=0x20) {
      byte mem[]=read_mem(logger,n*256+i,32);
      for(int j=0;j<32;j++) cbuf[i+j]=mem[j];
      logger.log2comproto(Logger.bytearray2string(mem), 2);
    }
    return cbuf;
  }

  public static byte[] read_configblock(Logger logger) {
    return read_block(logger,0);
  }



  public static int write_configblock(byte[] cbuf,Logger logger) {
    int ret=Error.OK;
    Log.d(TAG,"writeconfigblock...");
    if(!logger.isconnected) {
      Log.d(TAG, "connection problem ");
      return Error.ERR;
    }

    logger.updateStatus("Setting the clock...", 2);
    write_clock(logger);

    logger.updateStatus("Write settings...", 2);

    write_settings(logger,logger.config.set_interval, logger.config.temp_offset, logger.config.humi_offset);
    write_timestamps(logger);
    if(logger.config.set_interval!=logger.config.got_interval) {
      /* Interval has changed, so old data should be deleted. 
         We must show a warning !
       */
      logger.updateStatus("Clear Data...", 2);
      clearData(logger);
    }
    return(ret);
  }

  /* Standardprocedure "READ LOGGER" for this loggertype...*/	

  public static int get_config(Logger logger) {
    byte cbuf[]=read_configblock(logger);
    if(cbuf==null) return Error.ERR;
    LoggerConfig config=logger.config;
    Date dt = new Date();
    int ret=Error.OK;
    config.timestamp=dt.getTime()/1000;
    config.configbuf=cbuf.clone();
    config.cbufsize=cbuf.length;
    if(cbuf.length<256) return Error.ERR_BAD;
    loggerstatus="Config OK: ";
    config.time_offset=0;
    config.flag_bits=0;
    config.block_type=0;
    config.block_cmd=0;
    config.templimit_high=60;
    config.templimit_low=-40;
    config.calibration_Mvalue=0;
    config.calibration_Cvalue=0;
    config.humilimit_high=100;
    config.humilimit_low=0;
    config.rollover_count=0;
    config.scaling_factor=(float)1.0;
    config.flag_bits2=0;
    config.mglobal_message="";
    ByteBuffer buffer = ByteBuffer.allocate(cbuf.length);
    buffer.position(0);
    buffer.put(cbuf);
    buffer.position(0);
    buffer.order(ByteOrder.BIG_ENDIAN);

    logger.packet_size=32;
    logger.memory_size=0xf300;
      
        /* white logger: 
	  
	"init_ok"       : (0x00, 2),   0x55aa
        "model"         : (0x02, 2),   0x0201
        "ID"            : (0x05, 4),
        "settings"      : (0x09, 6),   2d 01 2c 00 64 14
                          (0x0a: 2),   0x02,0x58,  // = 600 (Sekunden, evtl. 10 Minuten Interval ?)
        "unknown"       : (0x50, 6),
        "series_counts" : (0x58, 324), 
        "series_dates"  : (0x19C, 2592), 
            Jede Serie beinhaltet 64 Messwerte T,H
	    Es gibt 324 Serien.
            jeweils 8 bytes:  (year-2000), month, day, hour, minute, second  01 2C
	    
	    
        "series"        : (0xD00, 0xFFFF-0xD00) 
	
	*/
      if((buffer.get(0)&0xff)!=0x55 || (buffer.get(1)&0xff)!=0xAA) return Error.ERR_BAD;
      logger.FirmwareVersion=String.format("%04x",buffer.getShort(2));
      config.serial_number=buffer.getInt(5);  /*   */
      config.setname(String.format("%08x",(int)config.serial_number&0xffffffff));
      
      config.temp_offset=((float)buffer.get(13)-100)/(float)10;
      config.humi_offset=(float)buffer.get(14)-20;
      
      byte[] c = new byte[512];	
      System.arraycopy(cbuf, 0, c, 0, cbuf.length);
      cbuf=read_block(logger,1);
      System.arraycopy(cbuf, 0, c, 256, cbuf.length);
      cbuf=c;
      config.configbuf=cbuf.clone();
      config.cbufsize=cbuf.length;
      buffer = ByteBuffer.allocate(cbuf.length);
      buffer.position(0);
      buffer.put(cbuf);
      buffer.position(0);
      buffer.order(ByteOrder.BIG_ENDIAN);
	
      /* Erstes Seriendatum: 
      
        Startblock ermitteln, und dann etsprechenden Timestamp ermitteln.
      */
      
      
      read_timestamp(logger,get_startblock(logger));

      config.num_data_conf=(0x10000-0xd00)/3;  /* = 324 * 64 */

      /* MinMax and actual Values */

      logger.maxval_temp=(float)(buffer.get(0x50)*256+(buffer.get(0x51)&0xff))/10-50;
      logger.maxval_humi=(float)buffer.get(0x52)-20;
      logger.minval_temp=(float)(buffer.get(0x53)*256+(buffer.get(0x54)&0xff))/10-50;
      logger.minval_humi=(float)buffer.get(0x55)-20;

      // logger.actual_temp=0;
      // logger.actual_humi=0;

      /* Segment aus erstem und zweitem Timestamp */

      config.starttime_year=bcddecode(buffer.get(0x19c))+2000;
      config.starttime_mon= bcddecode(buffer.get(0x19d));
      config.starttime_mday=bcddecode(buffer.get(0x19e));
      config.starttime_hour=bcddecode(buffer.get(0x19f));
      config.starttime_min= bcddecode(buffer.get(0x1a0));
      config.starttime_sec= bcddecode(buffer.get(0x1a1));

      config.stoptime_year=bcddecode(buffer.get(0x19c+8))+2000;
      config.stoptime_mon= bcddecode(buffer.get(0x19d+8));
      config.stoptime_mday=bcddecode(buffer.get(0x19e+8));
      config.stoptime_hour=bcddecode(buffer.get(0x19f+8));
      config.stoptime_min= bcddecode(buffer.get(0x1a0+8));
      config.stoptime_sec= bcddecode(buffer.get(0x1a1+8));

      config.time_offset=0;
      
      /* Flags auswerten */
      
      config.temp_unit  =((buffer.get(9)&1)!=0)?Logger.UNIT_C:Logger.UNIT_F;
      config.date_format=((buffer.get(9)&2)!=0)?Logger.DATEFORMAT_MMDDYYYY:Logger.DATEFORMAT_DDMMYYYY;
      config.time_format=((buffer.get(9)&4)!=0)?Logger.TIMEFORMAT_24H:Logger.TIMEFORMAT_12H;
      config.stopcondition=0;
      if((buffer.get(9)&8)==0) {  /* rollover */
        config.stopcondition|=Logger.STOP_MEMFULL;
      }
      /*  Modus */
      if((buffer.get(9)&0x10)!=0) { /* segmented */
        config.stopcondition|=Logger.STOP_TIME;
	config.startcondition=Logger.START_TIME;
      } else config.startcondition=Logger.START_INSTANT;
      
      config.displaystate=((buffer.get(9)&0x20)!=0)?Logger.DISPLAY_30SEKON:Logger.DISPLAY_ON;


      /* Bestimme Gesamtanzahl der Daten im Speicher */

      int i;
      int anz=0;
      int b=0;
      for(i=0;i<324;i++) {
	b=buffer.get(0x58+i);
	if(b>0 && b!=0xff) anz+=b;
      }
      config.num_data_rec=anz;;
    get_actual_values(logger);
    logger.isreadconfig=true;
    return ret;
  }


  private static void read_timestamp(Logger logger, int idx) {
    byte mem[]=read_mem(logger,0x19c+8*idx,32);
    logger.config.time_year=(byte)bcddecode(mem[0]&0xff)+2000;
    logger.config.time_mon= (byte)bcddecode(mem[1]&0xff);
    logger.config.time_mday=(byte)bcddecode(mem[2]&0xff);
    logger.config.time_hour=(byte)bcddecode(mem[3]&0xff);
    logger.config.time_min= (byte)bcddecode(mem[4]&0xff);
    logger.config.time_sec= (byte)bcddecode(mem[5]&0xff);
    
    logger.config.got_interval=(mem[6]&0xff)*256+(mem[7]&0xff);  /* In Sekunden */
    logger.config.set_interval=logger.config.got_interval;
  }

  /* Read one datapacket from Freetec white */

  private static int getdatapacket(Logger logger, int idx, int anz) {
    if(idx>=0 && idx<324 && anz>0 && anz<=64) {
      byte buf[]=new byte[64*3];
      logger.log2comproto("Read Data Chunk #"+idx,1);
      int n;
      for(int i=0;i<64*3;i+=0x20) {
        n=0xd00+idx*3*64+i;
        byte mem[]=read_mem(logger,n,32);
        for(int j=0;j<32;j++) buf[i+j]=mem[j];
        logger.log2comproto(Logger.bytearray2string(mem), 2);
      }
      short t=-1;
      short h=-1;
      for(int i=0;i<anz;i++) {
        h=(short)(buf[i*3+0]&0xff);
        t=(short)(((buf[i*3+1]&0xff)<<8)+(buf[i*3+2]&0xff));
        logger.data.add(t,h);
      }
    }
    return Error.OK;
  }

  private static int get_startblock(Logger logger) {
    if((logger.config.configbuf[0x58+324-1]&0xff)==0xff) return(0); /* kein Rollover */
    int s=get_endblock(logger)+1;
    if(s>=324) s=0;
    
    return(s);
  }
  private static int get_endblock(Logger logger) {
    int s=-1;
    for(int i=0;i<324;i++) {
      if((logger.config.configbuf[0x58+i]&0xff)==0xff) {
        s=i;
	break;
      }
    }
    return(s);
  }

  public static int get_data(Logger logger) {
    int i,q=0,c=1,p1=1,p2=1,p3=0,p4=0;
    boolean do_all=((logger.config.flag_bits2&1)==0);
    final LoggerData data=logger.data;
    final LoggerConfig config=logger.config;
    data.set_calibration((float)0.1,-50, 1, -20);
    logger.updateStatus(0);
    logger.log2comproto("Read Data.",0);

    /* Erst das Längenverzeichnis holen, 
      und dann alle belegten Datenchunks lesen.
      Besser: Reihenfolge anhand der Zeitstempel ermitteln.
    */
    
    /* Daten Anfang finden */
    int data_startblock=get_startblock(logger);
    int data_endblock=get_endblock(logger);
    logger.log2comproto("Endblock: "+data_endblock,0);
    logger.log2comproto("Startblock: "+data_startblock,0);
    if(data_endblock==-1) data_endblock=324;
    
    if(data_startblock>0) {
      int idx;
      for(i=0;i<324;i++) {
        idx=i+data_startblock;
        if(idx>=324) idx-=324;
    	logger.updateProgress(100*i/324);
    	getdatapacket(logger, idx,logger.config.configbuf[0x58+idx]);	
      }
    } else {  /* kein Rollover */
      for(i=data_startblock;i<data_endblock;i++) {
    	logger.updateProgress(100*i/data_endblock);
    	getdatapacket(logger, i,logger.config.configbuf[0x58+i]);
      }
    }
    logger.updateProgress(100);

    /* Nun Qualitaetscheck der Daten machen.*/
    if(logger.loggertype==Logger.LTYP_TH) {
      for(i=0;i<config.num_data_rec;i++) {
    	if(data.get_temp(i)>100 || data.get_temp(i)<-100 || 
    	  		data.get_rh(i)>100 || data.get_rh(i)<0) q++;
      }
    } else q=0;
    logger.data.quality=q;
    return(q);  
  }

  private static int bcddecode(int a) {return(10*((a&0xf0)>>4)+(a&0xf));}
  private static byte bcdencode(byte a) {return (byte)(((a/10)<<4)+(a%10));}

  public static String get_status(Logger logger) {
    String ret="";
    if(logger.isreadconfig) ret=loggerstatus;
    else ret="Config has not yet been read";
    return ret+".";
  }
}

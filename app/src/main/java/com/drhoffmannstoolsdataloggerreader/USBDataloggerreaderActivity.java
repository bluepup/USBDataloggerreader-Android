package com.drhoffmannstoolsdataloggerreader;

/* USBDataloggerreaderActivity.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.io.File;
import java.io.FilenameFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;


import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

// import android.support.v4.provider.DocumentFile;

import static java.util.Locale.*;

/*
 * vdl120: The Voltcraft DL-120TH tool + eltuxusb (mcclibhid)
 *
 *   + write configuration
 *   + read configuration
 *   + read log data
 *   + store log data for plotting
 *
 * 
 * */


public class USBDataloggerreaderActivity extends Activity {
  private static final String TAG = "USBDL";
  private static boolean usesystime=true;
  private static boolean do_autoreadout=false;
  public static boolean helpdialogisopen=false;
  private Button getconfig,writeconfig,savedata;
  private Button getdata;

  private Button loggerdate,startdate,stopdate;
  private Button loggertime,starttime,stoptime;

  private EditText numconfig,logname,minterval,temphigh,templow,rhhigh,rhlow,presshigh,presslow,delay;
  private TextView status,numrec,maxnumrec,manzdata,mmessage,blinktext;
  private static OLEDView valuedisplay;
  private static OLEDView minmaxdisplay1;
  private static OLEDView minmaxdisplay2;
  private static OLEDView minmaxdisplay3;
  private RadioButton celsius,fahrenheit;
  private RadioButton a5,a10,a15,a20,a25,a30;
  private RadioButton binstant,bbutton,bstart;
  private RadioButton lcdon,lcdoff,lcd30sec;
  private RadioGroup radioGroupLCD;
  private RadioGroup radioGroupLED;
  private RadioGroup radioGroupSTART;
  private CheckBox alarm,systime,rollover,bstop,bstoptime,bstopusb,pause;
  private CheckBox alarmt,alarmh,alarmp;
  private ProgressBar progress1,progress2;
  private static PlotView plot;
  private LinearLayout extrafunctions;
  private LinearLayout loggertimegroup;
  private LinearLayout starttimegroup;
  private LinearLayout stoptimegroup;
  private LinearLayout numdataconfgroup;
  private ImageButton shot,batshot,clear;

  private static int mProgressStatus = 0;
  private int msysstatus;
  private String mfilename;

  private static String mEmailBody;

  private static String mSysMessage;
  private static int msysstatus2;
  
  private byte[] confmessagebuf;

  final static Handler h=new Handler();
  final static Logger logger=new Logger();
  final static LoggerData data=logger.data;
  final static LoggerConfig config=logger.config;

  public static Logger getLogger(){
       return logger;
  }
  public static LoggerConfig getConfig(){
       return config;
  }
    
  public static String csvsplitter;

  @Override
  public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.launcher);
    /* Disable Home Button while on the home screen:
     */
    ActionBar actionBar = getActionBar();
    //  actionBar.setHomeButtonEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(false);

    logger.UsbManager = (UsbManager)getSystemService(Context.USB_SERVICE);
    logger.mActivity=this;

    loggertime=(Button)findViewById(R.id.loggertime);
    starttime=(Button)findViewById(R.id.starttime);
    stoptime=(Button)findViewById(R.id.stoptime);
    startdate=(Button)findViewById(R.id.startdate);
    loggerdate=(Button)findViewById(R.id.loggerdate);
    stopdate=(Button)findViewById(R.id.stopdate);

    getconfig = (Button)findViewById(R.id.getconfig);
    getdata = (Button)findViewById(R.id.getdata);
    savedata = (Button)findViewById(R.id.savedata);
    shot = (ImageButton)findViewById(R.id.shot);
    batshot = (ImageButton)findViewById(R.id.batshot);
    clear = (ImageButton)findViewById(R.id.clear);

    writeconfig = (Button)findViewById(R.id.writeconfig);
    status=(TextView)findViewById(R.id.status);
    numconfig=(EditText)findViewById(R.id.numconfig);
    logname=(EditText)findViewById(R.id.logname);
    minterval=(EditText)findViewById(R.id.interval);
    templow=(EditText)findViewById(R.id.templow);
    temphigh=(EditText)findViewById(R.id.temphigh);
    rhlow=(EditText)findViewById(R.id.rhlow);
    rhhigh=(EditText)findViewById(R.id.rhhigh);
    presslow=(EditText)findViewById(R.id.plow);
    presshigh=(EditText)findViewById(R.id.phigh);
    delay=(EditText)findViewById(R.id.delay);

    numrec=(TextView)findViewById(R.id.numrec);
    maxnumrec=(TextView)findViewById(R.id.maxnumrec);
    manzdata=(TextView)findViewById(R.id.anzdata);
    mmessage=(TextView)findViewById(R.id.message);
    blinktext=(TextView)findViewById(R.id.blinktext);
    valuedisplay=(OLEDView)findViewById(R.id.valuedisplay);
    minmaxdisplay1=(OLEDView)findViewById(R.id.minmaxdisplay1);
    minmaxdisplay2=(OLEDView)findViewById(R.id.minmaxdisplay2);
    minmaxdisplay3=(OLEDView)findViewById(R.id.minmaxdisplay3);

    celsius=(RadioButton)findViewById(R.id.celsius);
    fahrenheit=(RadioButton)findViewById(R.id.fahrenheit);
    a5=(RadioButton)findViewById(R.id.led5);
    a10=(RadioButton)findViewById(R.id.led10);
    a15=(RadioButton)findViewById(R.id.led15);
    a20=(RadioButton)findViewById(R.id.led20);
    a25=(RadioButton)findViewById(R.id.led25);
    a30=(RadioButton)findViewById(R.id.led30);
    binstant=(RadioButton)findViewById(R.id.startimmedeately);
    bbutton=(RadioButton)findViewById(R.id.startonbutton);
    bstart=(RadioButton)findViewById(R.id.startonstarttime);

    lcdon=(RadioButton)findViewById(R.id.lcdon);
    lcdoff=(RadioButton)findViewById(R.id.lcdoff);
    lcd30sec=(RadioButton)findViewById(R.id.lcd30sec);

    radioGroupLCD=(RadioGroup)findViewById(R.id.radioGroupLCD);
    radioGroupLED=(RadioGroup)findViewById(R.id.radioGroupLED);
    radioGroupSTART=(RadioGroup)findViewById(R.id.radioGroupStartBed);
    alarm=(CheckBox)findViewById(R.id.alarm);
    alarmt=(CheckBox)findViewById(R.id.alarmT);
    alarmh=(CheckBox)findViewById(R.id.alarmH);
    alarmp=(CheckBox)findViewById(R.id.alarmP);
    systime=(CheckBox)findViewById(R.id.systime); 
    rollover=(CheckBox)findViewById(R.id.rollover); 
    pause=(CheckBox)findViewById(R.id.pause); 
    bstop=(CheckBox)findViewById(R.id.StopBed); 
    bstoptime=(CheckBox)findViewById(R.id.StopBedtime); 
    bstopusb=(CheckBox)findViewById(R.id.StopBedUSB); 
    extrafunctions=(LinearLayout)findViewById(R.id.extrafunctions);
    loggertimegroup=(LinearLayout)findViewById(R.id.loggertimegroup); 
    starttimegroup=(LinearLayout)findViewById(R.id.starttimegroup); 
    stoptimegroup=(LinearLayout)findViewById(R.id.stoptimegroup); 
    numdataconfgroup=(LinearLayout)findViewById(R.id.numdataconfgroup); 

    progress1=(ProgressBar)findViewById(R.id.progress1);  
    progress2=(ProgressBar)findViewById(R.id.progress2);  

    plot=(PlotView) findViewById(R.id.plot); 
    plot.setXlabel(getResources().getString(R.string.word_samples));
    valuedisplay.setTextSize(50);
    valuedisplay.setTextColor(4);
    minmaxdisplay1.setTextSize(40);
    minmaxdisplay1.setTextColor(1);
    minmaxdisplay2.setTextSize(40);
    minmaxdisplay2.setTextColor(2);
    minmaxdisplay3.setTextSize(40);
    minmaxdisplay3.setTextColor(3);
    systime.setChecked(usesystime);

    loggertimegroup.setEnabled(!usesystime);
    loggertime.setEnabled(!usesystime);
    loggerdate.setEnabled(!usesystime);
    delay.setEnabled(!usesystime);

    /* Für den USBDetector-Broadcast-receiver teilen wir das aktuelle 
     * Activity Objekt mit, damit er uns zurueckrufen kann. */
    USBDetector.god=this;
		
    loggerdate.setOnClickListener(new OnClickListener() {
      public void onClick(View v) { 
        SetDate_dialog(USBDetector.god,loggerdate);
        updateTimeoffset();
      }
    });
    loggertime.setOnClickListener(new OnClickListener() {
      public void onClick(View v) { 
        SetTime_dialog(USBDetector.god,loggertime);
        updateTimeoffset();
      }
    });
    startdate.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
    	SetDate_dialog(USBDetector.god,startdate);
      }
    });
    starttime.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
    	SetTime_dialog(USBDetector.god,starttime);
      }
    });
    stopdate.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
    	SetDate_dialog(USBDetector.god,stopdate);
      }
    });
    stoptime.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
    	SetTime_dialog(USBDetector.god,stoptime);
      }
    });

    getconfig.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {getconfig();}
    });
    
    systime.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
	usesystime=systime.isChecked();
	Log.d(TAG,"systime: "+usesystime);
	loggerdate.setEnabled(!usesystime);
	loggertime.setEnabled(!usesystime);
	delay.setEnabled(!usesystime);
	if(!usesystime) {
	   //aktuelle Zeit reinschreiben und delay aktualisieren.
	  updateTimeoffset();
	}
      }
    });
    
    clear.setOnClickListener(new OnClickListener() {public void onClick(View v) {showDialog(9);}});
    shot.setOnClickListener(new OnClickListener() {public void onClick(View v) { 
        if(logger.protocol==Logger.PROTO_FREETECWHITE) {
	  Freetecwhite.get_actual_values(logger);
	  Freetecwhite.get_minmax_values(logger);
	  // TODO: Show the values
	  getconfig();
	} else if(logger.protocol==Logger.PROTO_ELV) {
	  Lascar.read_actual_value(logger);
	  // make a pause....
	  getconfig();
	}
    }});
    batshot.setOnClickListener(new OnClickListener() {public void onClick(View v) { 
      if(logger.protocol==Logger.PROTO_ELV) {
	valuedisplay.setText("Bat:"+(float)Math.round(3.6*Lascar.read_battery_value(logger)/700*1000)/1000.0+"V");
	valuedisplay.invalidate();
      }
    }});

    writeconfig.setOnClickListener(new OnClickListener() {public void onClick(View v) {sendConfig();}});

    getdata.setOnClickListener(new OnClickListener() {
      public void onClick(View v) { 
	// Start lengthy operation in a background thread
	new Thread(new Runnable() {public void run() {getdata();}}).start();
    }});

    savedata.setOnClickListener(new OnClickListener() {
      public void onClick(View v) { 
	new Thread(new Runnable() {public void run() {savedata();}}).start();
    }});
      
    progress2.setVisibility(View.GONE);
    savedata.setEnabled(false);
    // timepicker.setIs24HourView(true);
    extrafunctions.setVisibility(View.GONE);
    config2UI();
    Calendar cal=Calendar.getInstance();
    Date dt = new Date();
    cal.set(dt.getYear()+1900, dt.getMonth(),dt.getDate(), dt.getHours(),dt.getMinutes(),dt.getSeconds());
    loggertime.setText(String.format(Locale.US,"%02d:%02d:00",dt.getHours(),dt.getMinutes()));
    loggerdate.setText(String.format(Locale.US,"%d-%02d-%02d",(dt.getYear()+1900),(dt.getMonth()+1),dt.getDate()));
    starttime.setText(String.format(Locale.US,"%02d:%02d:00",dt.getHours(),dt.getMinutes()));
    startdate.setText(String.format(Locale.US,"%d-%02d-%02d",(dt.getYear()+1900),(dt.getMonth()+1),dt.getDate()));
    stoptime.setText(String.format(Locale.US,"%02d:%02d:00",dt.getHours(),dt.getMinutes()));
    stopdate.setText(String.format(Locale.US,"%d-%02d-%02d",(dt.getYear()+1900),(dt.getMonth()+1),dt.getDate()));

    /* Switch off Keyboard at beginning */
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    numconfig.addTextChangedListener(new TextWatcher() {
      public void onTextChanged(CharSequence s, int start, int before, int count) {}
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      public void afterTextChanged(Editable s) {
        int ds=0;
    	if(s.length()>0) {
		try {ds=(int)Double.parseDouble(s.toString());}
		catch (NumberFormatException nfe) {ds=0;}
	} 
        try {
          config.set_interval =(int)Double.parseDouble(minterval.getText().toString());
        } catch (NumberFormatException nfe) {config.set_interval=60;}
        maxnumrec.setText("-> ("+Physics.duration(ds,config.set_interval)+")");

      }});
    minterval.addTextChangedListener(new TextWatcher() {
      public void onTextChanged(CharSequence s, int start, int before, int count) {}
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      public void afterTextChanged(Editable s) {
        int ds=0;
    	if(s.length()>0) {
		try {ds=(int)Double.parseDouble(s.toString());}
		catch (NumberFormatException nfe) {}
	} 
        try {
          config.num_data_conf=(int)Double.parseDouble(numconfig.getText().toString());
        } catch (NumberFormatException nfe) {config.num_data_conf=1;}

        maxnumrec.setText("-> ("+Physics.duration(config.num_data_conf,ds)+")");
      }});

    delay.addTextChangedListener(new TextWatcher() {
      public void onTextChanged(CharSequence s, int start, int before, int count) {}
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      public void afterTextChanged(Editable s) {
	int ds=0;
	if(s.length()>0) {
		try {ds=(int)Double.parseDouble(s.toString());}
		catch (NumberFormatException nfe) {}
	} 
	Calendar cal = Calendar.getInstance();
	Date dt = new Date();

	cal.set(dt.getYear()+1900, dt.getMonth(),dt.getDate(), dt.getHours(),dt.getMinutes(),dt.getSeconds());
	long ss=cal.getTimeInMillis()/1000;
	if(ds>0) ss=ss+ds;
	cal.setTimeInMillis(ss*1000);
	//timepicker.setCurrentHour(cal.getTime().getHours());
	//timepicker.setCurrentMinute(cal.getTime().getMinutes());
	//TODO was machen wir mit den restsekunden zu einer vollen Minute?
	//datepicker.updateDate(cal.getTime().getYear()+1900, 
	//		cal.getTime().getMonth(), cal.getTime().getDate());
    }});

/*   TODO: this doesnt work as expected 
    presshigh.addTextChangedListener(new TextWatcher() {
      public void onTextChanged(CharSequence s, int start, int before, int count) {}
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      public void afterTextChanged(Editable s) {
	float p=1200,op;
	if(s.length()>0) {
	  try {p=(float)Double.parseDouble(s.toString());}
	  catch (NumberFormatException nfe) {}
          op=p;
	  if(p<700) p=700;
	  if(p>1200) p=1200;
	  if(p!=op) presshigh.setText(""+p);
	} else presshigh.setText(""+p);
    }});
 */ 
    presshigh.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
	// super.onTouch(v, event);
	float p=1200,op;
	  try {p=(float)Double.parseDouble(presshigh.getText().toString());}
	  catch (NumberFormatException nfe) {p=9999;}
          op=p;
	  if(p<700) p=700;
	  if(p>1200) p=1200;
	  if(p!=op) presshigh.setText(""+p);
	  return(false);
      }
    });
    presslow.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
	// super.onTouch(v, event);
	float p=700,op;
	  try {p=(float)Double.parseDouble(presslow.getText().toString());}
	  catch (NumberFormatException nfe) {p=0;}
          op=p;
	  if(p<700) p=700;
	  if(p>1200) p=1200;
	  if(p!=op) presslow.setText(""+p);
	  return(false);
      }
    });
    rhhigh.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
	// super.onTouch(v, event);
	float p=100,op;
	try {p=(float)Double.parseDouble(rhhigh.getText().toString());}
	catch (NumberFormatException nfe) {p=999;}
        op=p;
	if(p<0) p=0;
	if(p>100) p=100;
	if(p!=op) rhhigh.setText(""+p);
	return(false);
      }
    });
    rhlow.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
	// super.onTouch(v, event);
	float p=0,op;
	try {p=(float)Double.parseDouble(rhlow.getText().toString());}
	catch (NumberFormatException nfe) {p=-1;}
        op=p;
	if(p<0) p=0;
	if(p>100) p=100;
	if(p!=op) rhlow.setText(""+p);
	return(false);
      }
    });
    temphigh.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
	// super.onTouch(v, event);
	float p=0,op;
	try {p=(float)Double.parseDouble(temphigh.getText().toString());}
	catch (NumberFormatException nfe) {p=555;}
        op=p;
	if(p<-50) p=-50;
	if(p>60) p=60;
	if(p!=op) temphigh.setText(""+p);
	return(false);
      }
    });
    templow.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
	// super.onTouch(v, event);
	float p=0,op;
	try {p=(float)Double.parseDouble(templow.getText().toString());}
	catch (NumberFormatException nfe) {p=-555;}
        op=p;
	if(p<-50) p=-50;
	if(p>60) p=60;
	if(p!=op) templow.setText(""+p);
	return(false);
      }
    });

    
  }

  public void updateMessage(String s,int n) {
    msysstatus2=n;
    mSysMessage=s;
    h.post(new Runnable() {
      public void run() {displaymessage(mSysMessage,msysstatus2);}
    });
  }
  public void updateStatus(String s,int n) {
    msysstatus2=n;
    mSysMessage=s;
    h.post(new Runnable() {
      public void run() {displaymessage(mSysMessage,msysstatus2);}
    });
  }

  public void updateProgress(int i) {
    mProgressStatus=i;
    h.post(new Runnable() {
      public void run() {
	progress1.setProgress(mProgressStatus);
	manzdata.setText(""+data.anzdata);
      }
    });
  }
  public void updateStatus(int i) {
    msysstatus=i;
    h.post(new Runnable() {public void run() {
      if(msysstatus==Error.OK) displaystatus("OK.",0);
      else if(msysstatus==Error.ERR) displaymessage("ERROR: transmission timeout.",5);
      else displaystatus("OK, size="+msysstatus,0);
    }});
  }

  @Override
  public void onStop() {
    super.onStop();
    // if(logger.isreadconfig && !logger.issaved) baseconfigfrominput(); /* Get uncritical Settings from UI */
    // else 
    
    configfrominput(); /* Invalidates the config , deactivates the read data and save data buttons */
    if(logger.isconnected) Toast.makeText(getApplicationContext(), getResources().getString(R.string.message_rememberunplug), Toast.LENGTH_LONG).show();
  }
  @Override
  public void onStart() {
    super.onStart();
    if(!logger.isconnected) {
      setdisconnected();
      logger.checkforUSBdevices();
      if(logger.isconnected) setconnected();
    }
    if(!logger.isconnected) {
      helpdialogisopen=true;
      showDialog(1);  /* Show help and explain possible logger detection procedure (mediaLogger)*/
    }
    //  Weiter geht es mit OnResume
  }
  
  @Override
  public void onResume() {
    super.onResume();
    Log.d(TAG,"Resume...");
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

    /* Wurde Autoreadout eingeschaltet?*/
    do_autoreadout=prefs.getBoolean("do_autoreadout", false);
    if(do_autoreadout) {
      BackgroundService.set_interval(Integer.valueOf(prefs.getString("readoutinterval","60")));
      BackgroundService.set_uri(prefs.getString("readoutdestination","none://"));
    }
    /* Wurde der Simulator eingeschaltet?*/
    boolean na=prefs.getBoolean("simulate", false);
    if(!na && logger.do_simulate) {
      Toast.makeText(getBaseContext(), "Action: Logger Simulator detached.", Toast.LENGTH_LONG).show();
      logger.close();
      logger.do_simulate=false;
      setdisconnected();
    } else if(na) {
      int lt=(int)Double.parseDouble(prefs.getString("select_simumodel", "0"));
      if(lt!=logger.simulate_type && logger.do_simulate) logger.close();
      logger.do_simulate=true;
      logger.simulate_type=lt;
      logger.checkforUSBdevices();
      setconnected();
    }
    logger.fakeconfigdir=prefs.getBoolean("fakeloggerstorage", false);
    csvsplitter=prefs.getString("csvsplitter",",");
  
    /* Wurde ein Logger bereits an- oder abgesteckt ?*/
    if(!logger.do_simulate) {
      Intent intent = getIntent();
      Log.d(TAG, "intent: " + intent);
      String action = intent.getAction();
      if(UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
    	usb_attached(intent); 
      } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
    	usb_detached(intent);
      } else if (Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
    	usb_storage_attached(intent);
      } else if (Intent.ACTION_MEDIA_UNMOUNTED.equals(action)) {
    	usb_storage_detached(intent);
      }
    }
  
    /* Jetzt auf jeden Fall nochmal nach MEDIA-Loggern suchen */

    if(!logger.isconnected) {
      /* Klassische Methode, direkter Zugriff, geht bis Android 9*/
      logger.search_storage_location();
      if(logger.isconnected) setconnected();
      else if(Build.VERSION.SDK_INT >Build.VERSION_CODES.LOLLIPOP) { /* 21 */
        /* Ab Android 10 geht es nur noch so. Wenn der Helpdialog offen ist, 
	dann  verzögere erstmal die CL1 Auswahl.*/
	if(!logger.treeopened && !helpdialogisopen) {
          logger.log2comproto("no logger so far. Start SAF ",1);
//          openTree();
            openFile();
	  logger.treeopened=true;
        }
      }
    }

    /*Die Preferences könnten veraendert worden sein.*/
    plot.setHistEnable(prefs.getBoolean("histogramkl", false));
    plot.setEventEnable(prefs.getBoolean("eventkl", false));
    plot.setLabelEnable(prefs.getBoolean("labelkl", false));
    config2UI();
  }

  public void invalidate_config() {
    savedata.setEnabled(false); /* Config doesnt fit to data, */ 
    getdata.setEnabled(false);  /* so you must not save it anymore.*/
    //   isreadconfig=false;
  }    

  public void displaymessage(String text,int fl) {
    if(fl==0) mmessage.setTextColor(Color.GREEN);
    else if(fl==1) mmessage.setTextColor(Color.YELLOW);
    else if(fl==2) mmessage.setTextColor(Color.YELLOW);
    else if(fl==3) mmessage.setTextColor(Color.rgb(0xff, 0x7f, 0x00));
    else if(fl==4) mmessage.setTextColor(Color.MAGENTA);
    else if(fl==5) mmessage.setTextColor(Color.RED);
    else if(fl==10) mmessage.setTextColor(Color.rgb(0x00, 0xff, 0xa0));
    mmessage.setText(text);
    Log.d(TAG,"Message: "+text);
  }
  public void displaystatus(String text,int fl) {
    if(fl==0) status.setTextColor(Color.GREEN);
    else if(fl==1) status.setTextColor(Color.YELLOW);
    else if(fl==2) status.setTextColor(Color.YELLOW);
    else if(fl==3) status.setTextColor(Color.rgb(0xff, 0x7f, 0x00));
    else if(fl==4) status.setTextColor(Color.MAGENTA);
    else if(fl==5) status.setTextColor(Color.RED);
    else if(fl==10) status.setTextColor(Color.rgb(0x00, 0xff, 0xa0));
    status.setText(text);
    Log.d(TAG,"Status: "+text);
  }
  
  /* This can be called whenever a new USB Device was attached... */

  public void usb_attached(Intent intent) {
    /*A (new) logger has been connected..*/ 
    UsbDevice device=(UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
    logger.filterdevice(device);
  		/* TODO */
  		/* Erzwinge ein neueinlesen der Config, da 
  		 * die Calibration bei Lascar loggern verloren geht.*/
    getdata.setEnabled(false);
    if(!logger.isconnected) {
      setdisconnected();
      logger.checkforUSBdevices();
      if(logger.isconnected) setconnected();
    }
  }
  public void usb_detached(Intent intent) {
    UsbDevice device=(UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
    if(logger.Device!=null && logger.Device.equals(device)) {
      // setDevice(null);
      setdisconnected();
    }
  }
  /* This can be called whenever a new USB DStorage device was attached... */
  public void usb_storage_attached(Intent intent) {
    if(!logger.isconnected) {
      displaymessage("MEDIA mount detected...",4);
      logger.Path=intent.getDataString();
      displaystatus(logger.Path,4);
      logger.treeopened=false;
      if(logger.Path!=null) {
  	setdisconnected();
  	displaystatus(logger.Path,4);
  	logger.check_media_logger();
  	if(logger.isconnected) setconnected();
      }
    }
  }
  public void usb_storage_detached(Intent intent) {
    if(logger.isconnected && logger.protocol==Logger.PROTO_FILE
       && logger.Path.equals(intent.getDataString())) {
       setdisconnected();
    }
  }
	
	
  public void setconnected() {
    displaymessage("Device attached. Protocol="+logger.protocol,10);
    getconfig.setEnabled(true);
    writeconfig.setEnabled(true);

    getdata.setEnabled(false);
    logger.isreadconfig=false;

    if(do_autoreadout) {
      BackgroundService.set_logger(logger);
      Log.d(TAG,"Start background service ");
      startService(new Intent(this, BackgroundService.class));
    }
  }
  /* Handle a Device disconnect event... */
  public void setdisconnected() {
    displaymessage("Device detached.",3);
    displaystatus(" - ",10);
    getdata.setEnabled(false);
    getconfig.setEnabled(false);
    writeconfig.setEnabled(false);
    shot.setEnabled(false);
    batshot.setEnabled(false);
    clear.setEnabled(false);
    extrafunctions.setVisibility(View.GONE);
    logger.close();
    stopService(new Intent(this, BackgroundService.class));
    BackgroundService.set_logger(null);
  }


  @Override
  public void onDestroy() {
    super.onDestroy();
    logger.close();
    Log.d(TAG,"Destroy...");
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    super.onCreateOptionsMenu(menu);
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu, menu);
    return true;
  }
  @Override
  public boolean onOptionsItemSelected(final MenuItem item) {
    switch (item.getItemId())  {
    case R.id.vdl_options_preferences:
      startActivity(new Intent(this, PreferencesActivity.class));
      return true;
    case R.id.vdl_options_loggersettings:
      configfrominput();
      startActivity(new Intent(this, LoggerPreferencesActivity.class));
      return true;
    case R.id.vdl_options_about:
      startActivity(new Intent(this, InfoActivity.class));
      // showDialog(0);
      return true;
    case R.id.vdl_options_help:
      showDialog(1);
      return true;
    case R.id.vdl_options_statistics:
      Dialog dialog = Tools.scrollableDialog(this,getResources().getString(R.string.menu_statistics),logger.getStatistics());
      dialog.show();
      return true;
    case R.id.vdl_options_finish:
      finish();
      return true;
    case R.id.vdl_options_saveconfig:
      saveconfig();
      return true;
    case R.id.vdl_options_loadconfig:
      loadconfig();
      return true;
    case R.id.vdl_options_plottool:
      startActivity(new Intent(this, PlottoolActivity.class));
      return true;
    case R.id.vdl_options_tabletool:
      startActivity(new Intent(this, TabletoolActivity.class));
      return true;
    default: 
      return super.onOptionsItemSelected(item);
    }
  }

  private static String[] mFileList;
  private static String mChosenFile;


  private void loadFileList(){
    File mConfigPath= new File(getApplicationContext().getFilesDir().getAbsolutePath());
    if(mConfigPath.exists()){
      FilenameFilter filter = new FilenameFilter(){
  	public boolean accept(File dir, String filename){
  	  File sel = new File(dir, filename);
  	  return filename.contains(LoggerConfig.FTYPE_CONFIG) || sel.isDirectory();
  	}
      };
      mFileList = mConfigPath.list(filter);
    } else {
      Log.d(TAG,"Path not found!");
      mFileList= new String[0];
    }
  }


  @Override
  protected Dialog onCreateDialog(final int id) {
    Dialog dialog=null;
    if(id==0)       dialog=Tools.scrollableDialog(this,"",getResources().getString(R.string.aboutdialog)+getResources().getString(R.string.impressum));
    else if(id==1)  dialog=Tools.scrollableHelpDialog(this,"",getResources().getString(R.string.helpdialog),logger,this);
    else if(id==3)  dialog=Tools.scrollableDialog(this,"",getResources().getString(R.string.hardware_notsupported));
    else if(id==4)  dialog=Tools.scrollableDialog(this,"",getResources().getString(R.string.hardware_nottested));
    else if(id==10) dialog=Tools.scrollableDialog(this,"",String.format(getResources().getString(R.string.quality_warning),(int)100*data.quality/Math.max(1,data.anzdata)));
    else if(id==9) {
      AlertDialog.Builder builder = new Builder(this);
      builder.setTitle(getResources().getString(R.string.word_warning));
      builder.setMessage(getResources().getString(R.string.message_willclear));
      builder.setPositiveButton(getResources().getString(R.string.word_procceed), new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          if(logger.protocol==Logger.PROTO_FREETECWHITE) {
            Freetecwhite.clearData(logger);
            // make a pause....
            getconfig();
          } else if(logger.protocol==Logger.PROTO_ELV) {
            Lascar.erase_data(logger);
            // make a pause....
            getconfig();
          }
        } }); 
      builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          displaymessage("clear canceled.",1);
        } }); 
      dialog = builder.create();
      dialog.setCanceledOnTouchOutside(false);
    } else if(id==6) {
      AlertDialog.Builder builder = new Builder(this);
      builder.setTitle(getResources().getString(R.string.word_warning));
      builder.setMessage(getResources().getString(R.string.message_notsaved));
      builder.setPositiveButton(getResources().getString(R.string.word_procceed), new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          confmessagebuf = build_conf();
          if(confmessagebuf!=null) realsendconfig();
        } }); 
      builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          displaymessage("Send-Config canceled.",1);
          progress2.setVisibility(View.GONE);
        } }); 
      dialog = builder.create();
      dialog.setCanceledOnTouchOutside(false);
    } else if(id==5) {
      AlertDialog.Builder builder = new Builder(this);
      builder.setTitle(getResources().getString(R.string.word_attention));
      builder.setMessage(getResources().getString(R.string.message_notread));
      builder.setPositiveButton(getResources().getString(R.string.word_procceed), new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          realsendconfig();
        } }); 
      builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          displaymessage("Send-Config canceled.",1);
          progress2.setVisibility(View.GONE);
        } }); 
      dialog = builder.create();
      dialog.setCanceledOnTouchOutside(false);
    } else if(id>=1000) {                    /* >=1000 : Allgemeine Logger-Mitteilung */
      AlertDialog.Builder builder = new Builder(this);
      builder.setTitle(getResources().getString(R.string.word_loggermessage));
      builder.setMessage(config.mglobal_message);
      builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          //  confmessagebuf = build_conf();
          //  if(confmessagebuf!=null) realsendconfig();
        } });  
      dialog = builder.create();
      dialog.setCanceledOnTouchOutside(true);
    } else {                                 /* >= 20  Konfigurationsdialog / Warning ? */
      AlertDialog.Builder builder = new Builder(this);
      builder.setTitle("Select configuration:");
      if(mFileList == null){
        dialog = builder.create();
        return dialog;
      }
      builder.setItems(mFileList, new DialogInterface.OnClickListener(){
        public void onClick(DialogInterface dialog, int which){
          mChosenFile = mFileList[which];
          Log.d(TAG,"File chosen: "+mChosenFile);
          loadconffile(mChosenFile);
        }
      });
      dialog = builder.show();
    }       
    return dialog;
  }

    
  /* Zusätzliche Einstellmöglichkeit beim Draufklicken auf den Datepicker */

  void SetDate_dialog(final Context mContext, final Button returnbut) {
    final Dialog dialog = new Dialog(mContext);
    dialog.setTitle("Select Date");
    LinearLayout ll = new LinearLayout(mContext);
    ll.setOrientation(LinearLayout.VERTICAL);
		
    final DatePicker dp = new DatePicker(mContext);
    dp.setSpinnersShown(true);
    dp.setCalendarViewShown(false);
    int year,mon,day;
    String date=returnbut.getText().toString();
    String[] sep=date.split("-");
    year=(int)Double.parseDouble(sep[0]);
    mon=(int)Double.parseDouble(sep[1])-1;
    day=(int)Double.parseDouble(sep[2]);

    dp.updateDate(year,mon,day);
		
    ll.addView(dp);
    Button b1 = new Button(mContext);
    b1.setText(R.string.word_ok);
    b1.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        returnbut.setText(String.format(Locale.US,"%d-%02d-%02d",dp.getYear(),(dp.getMonth()+1),dp.getDayOfMonth()));
	dialog.dismiss();
      }
    });        
    ll.addView(b1);
    Button b2 = new Button(mContext);
    b2.setText(R.string.word_cancel);
    b2.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
    ll.addView(b2);
    dialog.setContentView(ll);        
    dialog.show();        
  }

  /*Zusätzliche Einstellmöglichkeit beim Draufklicken auf den Timepicker
   * Bei kleinen Bildschirmen im Scrollview geht es sonst nicht, bzw nur durch Doppelklick und mit
   * Tastatur.*/

  void SetTime_dialog(final Context mContext, final Button returnbut) {
    final Dialog dialog = new Dialog(mContext);
    dialog.setTitle("Select Time");
    LinearLayout ll = new LinearLayout(mContext);
    ll.setOrientation(LinearLayout.VERTICAL);
		
    final TimePicker tp = new TimePicker(mContext);
    tp.setIs24HourView(true);
    int hour,minute;
    String time=returnbut.getText().toString();
    String[] sep=time.split(":");
    hour=(int)Double.parseDouble(sep[0]);
    minute=(int)Double.parseDouble(sep[1]);
    tp.setCurrentHour(hour); 
    tp.setCurrentMinute(minute);
    ll.addView(tp);
    Button b1 = new Button(mContext);
    b1.setText(R.string.word_ok);
    b1.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        returnbut.setText(String.format(Locale.US,"%02d:%02d:00",tp.getCurrentHour(),tp.getCurrentMinute()));
        dialog.dismiss();
      }
    });
    ll.addView(b1);
    Button b2 = new Button(mContext);
    b2.setText(R.string.word_cancel);
    b2.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
    ll.addView(b2);
    dialog.setContentView(ll);
    dialog.show();
  }



  private int dialogi=0;

  private void loadconfig() {
    loadFileList();
    mChosenFile="";
    /* When data has not been saved yet, display a warning !*/
    showDialog(20+dialogi++);
  }
  /* Lade Konfigurationsfile.
   * Aus irgendeinem Grund gab es einen Absturz, wo wahrscheinlich sep nur 1 lang ist, also kein "="
   * in der Zeile gefunden wurde....
   * 
   * */
  private void loadconffile(String filename) {
    int ret=config.load(this, filename);
    if(ret==-1) displaymessage("ERROR loading config from file "+filename+".",5);
    else	displaymessage("Config "+filename+" loaded.",0);
    config2UI();
    invalidate_config();
  }

  private void saveconfig() {  
    /* When data has not been saved yet, display a warning !*/
    configfrominput();
    mfilename=config.getname()+LoggerConfig.FTYPE_CONFIG;
    config.save(this);
    h.post(new Runnable() {
      public void run() {
  	progress1.setProgress(100);
  	Toast.makeText(getApplicationContext(), "File "+mfilename+" saved.", Toast.LENGTH_LONG).show();
  	displaymessage("Config "+mfilename+" saved.",0);
      }
    });
  }


  /*Read configuration from device
   * */
  private void getconfig() {
    progress2.setVisibility(View.VISIBLE);
    int ret=logger.get_config(); 
    if(ret==Error.ERR)  displaymessage("ERROR: Get config failed. transmission timeout.",5);
    else if(ret==Error.ERR_BAD) displaymessage("ERROR: Config bad.",5);
    else if(ret==Error.OK || ret==Error.WARN_NOTSUP || ret==Error.WARN_NOTTESTED|| ret>0) displaystatus(logger.get_status(),0);
    if(ret==Error.WARN_NOTSUP) showDialog(3);  /*Notsupported/Not tested Message*/ 
    else if(ret==Error.WARN_NOTTESTED) showDialog(4);  /*Notsupported/Not tested Message*/ 
    config2UI();
    plot.setXRange(0,config.num_data_rec);
    if(logger.loggertype==Logger.LTYP_TEMP ||logger.loggertype==Logger.LTYP_TH) plot.setUnit("Temperature ["+(config.temp_unit==Logger.UNIT_C?"°C":"°F")+"]");
    else if(logger.loggertype==Logger.LTYP_VOLT) plot.setUnit("Voltage ["+config.getunit()+"]");
    else if(logger.loggertype==Logger.LTYP_CURR) plot.setUnit("Current ["+config.getunit()+"]");
    else if(logger.loggertype==Logger.LTYP_GAS) plot.setUnit("Gas concentration ["+config.getunit()+"]");
    plot.setAutoGridX();

    if((config.flag_bits&Lascar.SENSOR_FAIL)==Lascar.SENSOR_FAIL) config.mglobal_message=config.mglobal_message+getResources().getString(R.string.message_sensorerror);
    if((config.flag_bits&Lascar.BATTERY_FAIL)==Lascar.BATTERY_FAIL)   config.mglobal_message=config.mglobal_message+getResources().getString(R.string.message_batterycritical);
    else if((config.flag_bits&Lascar.BATTERY_LOW)==Lascar.BATTERY_LOW) config.mglobal_message=config.mglobal_message+getResources().getString(R.string.message_batterylow);

    if(config.num_data_rec>0) getdata.setEnabled(true);
    if(config.mglobal_message.length()>0) showDialog(1000+dialogi++);
    progress2.setVisibility(View.GONE);
    savedata.setEnabled(false);
  }
  /**********************************************************/


  private void getdata() {
    /*Lese die Maximalanzahl Daten nochmal ein, der User könnte Sie geaendert haben*/
    if(logger.protocol==Logger.PROTO_HID) {
      config.num_data_conf=(int)Double.parseDouble(numconfig.getText().toString());
      if(alarm.isChecked()) config.flag_bits2&=~1; else config.flag_bits2|=1;
    }

    if(config.num_data_rec==0) {
      data.clear();
      h.post(new Runnable() {
  	public void run() {
  	  displaymessage(getResources().getString(R.string.message_nodata),1); 
  	  Toast.makeText(getApplicationContext(), getResources().getString(R.string.message_nodata), Toast.LENGTH_LONG).show();
  	}
      });
      return;
    }
    h.post(new Runnable() {
      public void run() {
  	displaymessage(getResources().getString(R.string.message_receiving),0); 
      }
    });
    logger.get_data();
    data.set_interval(config.got_interval);
    if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) data.calc_events();
    h.post(new Runnable() {
      public void run() { 
  	manzdata.setText(""+data.anzdata);
  	displaymessage(getResources().getString(R.string.message_receivedall),0);
  	savedata.setEnabled(data.anzdata>0);
  	DataContent pdata=new DataContent(data,logger.loggertype);
  	pdata.analyze();
  	plot.setData(pdata);
  	plot.setAutoRange();
  	plot.setAutoGrid();
  	plot.postInvalidate();
  	progress1.setProgress(100);
  	if(data.quality>0) showDialog(10);
      }
    });
  }

  private void savedata() {
    FileOutputStream fos=null;
    OutputStreamWriter osw=null;
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

    String endung=prefs.getString("select_fileformat", "txt");

    String filename="VDL"+logger.loggertype+"-"+config.getname()+"-"+
    		    String.format(US,"%04d-%02d-%02d-%02d-%02d",config.time_year,(int)(config.time_mon&0xff),(int)(config.time_mday&0xff),
    				    (int)(config.time_hour&0xff),(int)(config.time_min&0xff))+"."+endung;

    h.post(new Runnable() {
      public void run() {
    	displaymessage(getResources().getString(R.string.message_saving),0);
      }
    });
    boolean append=!prefs.getBoolean("check_a", true);
    Calendar cal = Calendar.getInstance();
    cal.set(config.time_year, (int)(config.time_mon&0xff)-1, (int)(config.time_mday&0xff), (int)(config.time_hour&0xff), (int)(config.time_min&0xff), (int)(config.time_sec&0xff));
    long ss=cal.getTimeInMillis()/1000;

    /* Header für das Datenfile erstellen
     * 
     * */

    String titlestring="generated by "+getPackageName()+" V."+applicationVersion()+" "+
    		    getResources().getString(R.string.word_copyrmeldung);
    String infostring="["+String.format(US,"%04d-%02d-%02d %02d:%02d:%02d",config.time_year,(int)(config.time_mon&0xff),
    		    (int)(config.time_mday&0xff),(int)(config.time_hour&0xff),(int)(config.time_min&0xff),
    		    (int)(config.time_sec&0xff))+"] "+config.num_data_rec+" points @ "+config.got_interval+" sec.";
    String loggerinfostring=logger.Manufacturer+"-"+logger.Product+"/"+logger.loggertype+" #"+logger.Serial_id+" blocktype="+config.block_type
    		    +" Session: "+config.getname();
    String calibrationinfostring="A="+data.calibration_Avalue+" B="+data.calibration_Bvalue+" "+
    		    "A2="+data.calibration_A2value+" B2="+data.calibration_B2value+" "+
    		    "M="+config.calibration_Mvalue+" C="+config.calibration_Cvalue;
    String separator=" ";
    if(endung.equalsIgnoreCase("csv")) separator=prefs.getString("select_csvseparator",", ");
    String legende="UNIX time [s]"+separator;
    if(logger.loggertype==Logger.LTYP_VOLT) 
    	    legende=legende+"Voltage ["+config.getunit()+"]"
    			    +separator+separator+separator;
    else if(logger.loggertype==Logger.LTYP_CURR) 
    	    legende=legende+"Current ["+config.getunit()+"]"
    			    +separator+separator+separator;
    else {
      legende=legende+"Temperature ["+((config.temp_unit==Logger.UNIT_F)?"°F":"°C")+"]"+
			separator;
      if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
    	legende=legende+"Humidity [%]"+separator;
    	if(logger.loggertype==Logger.LTYP_THP) legende=legende+"Pressure [hPa]"+separator;
    	else legende=legende+separator; 				
      } else legende=legende+separator+separator;
    }
    legende=legende+"Date"+separator+"Time";
    /*Weil gewünscht hier noch Taupunkt und Wassergehalt:*/
    if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
      legende=legende+separator+"Dew Point ["+((config.temp_unit==Logger.UNIT_F)?"°F":"°C")+"]"+separator+"Water content [g/m³]";
    }

    String settingsinfo="";
    String statistics="";
    String eventstat="";
    if(logger.loggertype==Logger.LTYP_THP) 
	settingsinfo+=String.format(US,"# Pressure limits: [%.2f:%.2f] hPa\n",config.preslimit_low,config.preslimit_high);
    if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH) 
	settingsinfo+=String.format(US,"# Humidity limits: [%.1f:%.1f] %%\n",config.humilimit_low,config.humilimit_high);
    if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_TEMP) 
	settingsinfo+=String.format(US,"# Temperature limits: [%.1f:%.1f] %s\n",config.templimit_low,config.templimit_high,
	((config.temp_unit==Logger.UNIT_F)?"°F":"°C"));
    if(logger.loggertype==Logger.LTYP_VOLT || logger.loggertype==Logger.LTYP_CURR)
	settingsinfo+=String.format(US,"# Limits: [%f:%f] %s\n",config.templimit_low,config.templimit_high,config.getunit());

    if(config.start==1) settingsinfo+=String.format(US,"# Start delay=%d sec\n",config.time_delay);

    /* Datenstatistik erstellen */
    int duration=data.anzdata*config.got_interval;
    statistics=statistics+"# Time range: ["+ss+":"+(ss+duration)+"] "+Physics.duration_dhms(duration)+"\n";
    

    float tmin=9999,tmax=-9999,t;
    for(int i=0;i<data.anzdata;i++) {
    	    t=data.get_temp(i);
    	    tmin=Math.min(tmin,t);
    	    tmax=Math.max(tmax,t);
    }
    if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_TEMP) 
      statistics=statistics+String.format(US,"# Temperature range: [%.1f:%.1f] %s\n",
	tmin,tmax,((config.temp_unit==Logger.UNIT_F)?"°F":"°C"));
    else if(logger.loggertype==Logger.LTYP_VOLT || logger.loggertype==Logger.LTYP_CURR) 
      statistics=statistics+String.format(US,"# Range: [%f:%f] %s\n",tmin,tmax,config.getunit());
    if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH) {
      float rhmin=9999,rhmax=-9999,r;
      for(int i=0;i<data.anzdata;i++) {
        r=data.get_rh(i);
        rhmin=Math.min(rhmin,r);
        rhmax=Math.max(rhmax,r);
      } 
      statistics=statistics+String.format(US,"# Humidity range:    [%.1f:%.1f] %%\n",
        	      rhmin,rhmax);
      /* Events zusammenfassen   */

      if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH) {
	if(data.eventliste!=null && data.eventliste.size()>0) {
	  eventstat+="# Detected "+data.eventliste.size()+" Events:\n";
	  Lueftungsevent a;
	  for(int i=0;i<data.eventliste.size();i++) {
	    a=data.eventliste.get(i);
	    eventstat+="# "+i+": "+Physics.timestamp2datetime_short(ss+config.got_interval*a.idx)+
	  		    " for "+Physics.gerundet(config.got_interval*(a.idx2-a.idx)/60,1)+" minutes.\n";
	  }
	  eventstat+="# \n";
	}
      }
    }
    if(logger.loggertype==Logger.LTYP_THP) {
      float pmin=9999,pmax=-9999;
      for(int i=0;i<data.anzdata;i++) {
              pmin=Math.min(pmin,data.p[i]);
              pmax=Math.max(pmax,data.p[i]);
      } 
      statistics=statistics+String.format(US,"# Pressure range:    [%.2f:%.2f] hPa\n",
        	      logger.raw2p((short)pmin),logger.raw2p((short)pmax));
    }
    if(eventstat.length()>0) statistics+=eventstat;

    /*Falls Daten als Email gesendet werden, hier die Begleitinformationen zum Anhang
     * */
    mEmailBody=loggerinfostring+"\n"+
    		    "fileformat="+prefs.getString("select_fileformat", "dat")+"\n"+
    		    infostring+"\n"+settingsinfo+"\n"+statistics+"\n";
    File file=null;
    try {

      /*Das ist nur fuer interne Daten...*/
      //	      fos=openFileOutput(filename,MODE_APPEND|MODE_WORLD_READABLE);
      //	      osw=new OutputStreamWriter(fos);
      /* Hier jetzt ein externe von aussen lesbares verzeichnis*/
      File dirdata=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
      dirdata.mkdirs();

      file=new File(dirdata,filename);
      fos=new FileOutputStream(file,append);
      osw=new OutputStreamWriter(fos);

      if(endung.equalsIgnoreCase("xml")) {
        osw.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"+
          		"<metadata>\n");
        osw.write("  <link href=\"https://play.google.com/store/apps/details?id=com.drhoffmannstoolsdataloggerreader\">\n");
        osw.write("    <text>"+titlestring+"</text>\n");
        osw.write("  </link>\n");
        osw.write("  <time>"+String.format(US,"%04d-%02d-%02dT%02d:%02d:%02dZ",config.time_year,(int)(config.time_mon&0xff),
          		(int)(config.time_mday&0xff),(int)(config.time_hour&0xff),(int)(config.time_min&0xff),
          		(int)(config.time_sec&0xff))+"</time>\n");
        osw.write("  <datalogger>\n");
        osw.write("    <device>"+loggerinfostring+"</device>\n");
        osw.write("    <Sampletime>"+config.got_interval+"</Sampletime>\n");
        osw.write("    <serial>"+logger.Serial_id+"</serial>\n");
        osw.write("  <calibration>"+calibrationinfostring+"</calibration>\n");
        osw.write("  </datalogger>\n");
        osw.write("  <filename>"+filename+"</filename>\n");
        osw.write("</metadata>\n");
        osw.write("<log>\n");
        osw.write("  <name>"+config.getname()+"</name>\n");
        osw.write("  <subset>"+infostring+"</subset>\n");
        osw.write("  <legend>"+legende+"</legend>\n");
      } else if(endung.equalsIgnoreCase("csv")) {
        osw.write(legende+"\n"); /*erste Zeile ist legende*/
      } else {
        osw.write("# "+titlestring+"\n");
        osw.write("# loggertype:  "+loggerinfostring+"\n");
        osw.write("# Rawinput="+config.rawinputreading+" flagbits="+config.flag_bits+"\n");
        osw.write("# Calibration: "+calibrationinfostring+"\n");
        osw.write("# fileformat="+prefs.getString("select_fileformat", "dat")+" append="+append+"\n");
        osw.write(settingsinfo);
        osw.write("# Statistics:\n"+statistics);
        // Anzahl Schwellenüberschreitungen
        // ANzahl Lüftungsevents, Taupunkt Min/Max, Wassergehalt min/max
        // Evtl Histogramm
        osw.write("# "+infostring+"\n");
        osw.write("# "+legende+"\n#\n");
      }
      int i;
      String dateS,timeS;
      for(i=0;i<data.anzdata;i++) {
        cal = Calendar.getInstance();
        cal.setTimeInMillis(ss*1000);
        dateS=String.format(US,"%02d.%02d.%04d",
          		cal.getTime().getDate(),
          		(cal.getTime().getMonth()+1),
          		(cal.getTime().getYear()+1900));
        timeS=String.format(US,"%02d:%02d:%02d",
          		cal.getTime().getHours(),
          		cal.getTime().getMinutes(),
          		cal.getTime().getSeconds());
        if(endung.equalsIgnoreCase("xml")) {
          osw.write("  <sample timestamp=\""+ss+"\">\n");
          osw.write("	 <channel num=\"1\">\n");
          osw.write("	   <rawdata>"+data.temp[i]+"</rawdata>\n");
          osw.write("	   <value>"+data.get_temp(i)+"</value>\n");
          if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_TEMP) {
            osw.write("      <temperature>"+data.get_temp(i)+"</temperature>\n");
            osw.write("      <unit>"+((config.temp_unit==Logger.UNIT_F)?"°F":"°C")+"</unit>\n");
          } else osw.write("      <unit>"+config.getunit()+"</unit>\n");
          osw.write("    </channel>\n");
          if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
            osw.write("    <channel num=\"2\">\n");
            osw.write("      <rawdata>"+data.rh[i]+"</rawdata>\n");
            osw.write("      <value>"+data.get_rh(i)+"</value>\n");
            osw.write("      <humidity>"+data.get_rh(i)+"</humidity>\n");
            osw.write("      <unit>%</unit>\n");
            osw.write("    </channel>\n");
            osw.write("    <dewpoint>"+Physics.taupunkt(data.get_temp(i),data.get_rh(i))+"</dewpoint>\n");
            osw.write("    <water>"+Physics.water(data.get_temp(i),data.get_rh(i))+"</water>\n");
          }
          if(logger.loggertype==Logger.LTYP_THP) {
            osw.write("    <channel num=\"3\">\n");
            osw.write("      <rawdata>"+data.p[i]+"</rawdata>\n");
            osw.write("      <value>"+logger.raw2p(data.p[i])+"</value>\n");
            osw.write("      <pressure>"+logger.raw2p(data.p[i])+"</pressure>\n");
            osw.write("      <unit>hPa</unit>\n");
            osw.write("    </channel>\n");
          } 
          osw.write("	 <time>"+dateS+"T"+timeS+"Z</time>\n");
          osw.write("  </sample>\n");
        } else {
          osw.write(""+ss+separator+Physics.gerundet(data.get_temp(i),5)+separator);
          if(logger.loggertype==Logger.LTYP_THP) {
            osw.write(Physics.gerundet(data.get_rh(i),2)+separator+logger.raw2p(data.p[i])+separator);
          } else if(logger.loggertype==Logger.LTYP_TH) {
            osw.write(Physics.gerundet(data.get_rh(i),2)+separator+"0"+separator);
          } else osw.write("0"+separator+"0"+separator);
          osw.write(dateS+separator+timeS);
          if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
	     if((config.temp_unit==Logger.UNIT_F)) osw.write(separator+
              Physics.gerundet(Physics.celsius2fahr(Physics.taupunkt((float)Physics.fahr2celsius(data.get_temp(i)),data.get_rh(i))),2)
              +separator+
              Physics.gerundet(Physics.water((float)Physics.fahr2celsius(data.get_temp(i)),data.get_rh(i)),2)
              );
            else osw.write(separator+Physics.gerundet(Physics.taupunkt(data.get_temp(i),data.get_rh(i)),2)+
          	separator+Physics.gerundet(Physics.water(data.get_temp(i),data.get_rh(i)),2));
          }
          osw.write("\n");
        }
        ss+=config.got_interval;
        mProgressStatus=i*100/data.anzdata;
        h.post(new Runnable() {
          public void run() {
            progress1.setProgress(mProgressStatus);
          }
        });
      }
      if(endung.equalsIgnoreCase("xml")) {
        osw.write("</log>\n");
      } else if(endung.equalsIgnoreCase("csv")) {
        osw.write("# "+titlestring+"\n");
        osw.write("# loggertype:  "+loggerinfostring+"\n");
        osw.write("# Calibration: "+calibrationinfostring+"\n");
        osw.write("# Rawinput="+config.rawinputreading+" flagbits="+config.flag_bits+"\n");
        osw.write(settingsinfo);
        osw.write("# "+infostring+"\n");
      }
      data.issaved=true;
    } catch (Throwable thr) {
      // FilenotFound oder IOException
      Log.e(TAG,"open/save. ",thr);
      mfilename=file.getAbsolutePath();
      mfilename+=" : "+mfilename+" : "+thr.toString();
      h.post(new Runnable() {
        public void run() {
          progress1.setProgress(100);
          Toast.makeText(getApplicationContext(), "ERROR saving "+mfilename+".", Toast.LENGTH_LONG).show();
          displaymessage("ERROR saving file "+mfilename+". ",5);
        }
      });
    } finally {
      if(osw!=null) {try {osw.close();} catch (IOException e) {Log.e(TAG,"osw.close ",e);}}
      if(fos!=null) {try {fos.close();} catch (IOException e) {Log.e(TAG,"fos.close ",e);}}
      if(data.issaved) {
        mfilename=filename;
        h.post(new Runnable() {
          public void run() {
            progress1.setProgress(100);
            File f=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),mfilename);
            Toast.makeText(getApplicationContext(), "File "+f.getAbsolutePath()+" saved.", Toast.LENGTH_LONG).show();
            displaymessage("Saved data to file "+f.getAbsolutePath()+".",0);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            boolean dosendemail=prefs.getBoolean("do_sendemail", false);
            if(dosendemail) {
              String recipient=prefs.getString("recipient", "dr.markus.hoffmann@gmx.de");
              String subject="Data from "+getPackageName()+" V."+applicationVersion();
              Tools.sendEmail(progress1.getContext(),recipient,subject, mEmailBody, mfilename);
            }
          }
        });
      }
    }
  }

/* Berechnet delay aus aktueller Zeit minus eingestellte logger-zeit.*/

  private void updateTimeoffset() {
    String date=loggerdate.getText().toString();
    String[] sep=date.split("-");
    int year=(int)Double.parseDouble(sep[0]);
    int mon=(int)Double.parseDouble(sep[1]);
    int mday=(int)Double.parseDouble(sep[2]);
    String time=loggertime.getText().toString();
    sep=time.split(":");
    int hour=(int)Double.parseDouble(sep[0]);
    int min=(int)Double.parseDouble(sep[1]);
    int sec=0;

    Log.d(TAG,"Eingestellter Monat ist: "+mon);
    Calendar cal = Calendar.getInstance();
    cal.set(year,mon-1,mday,hour,min,sec);
    long ss1=cal.getTimeInMillis()/1000;
    Date dt = new Date();
    long ss2=dt.getTime()/1000;
    Log.d(TAG,"Timestamp ist: "+ss2);
    long d=(int)(ss1-ss2);
    if(d<0) {
      d=0;
      delay.setTextColor(Color.RED);
    } else {
      delay.setTextColor(Color.WHITE);
    }
    delay.setText(""+d);
    Log.d(TAG,"Berechneter Time-Offset: "+d+" Sekunden");
  }
  
  private void configfrominput() {
    baseconfigfrominput();
    try {
      config.set_interval =(int)Double.parseDouble(minterval.getText().toString());
    } catch (NumberFormatException nfe) {config.set_interval=60;}
    config.setname(logname.getText().toString());
    if(usesystime) {
      Date dt = new Date();
      config.time_year=dt.getYear()+1900;
      config.time_mon=(byte) (dt.getMonth()+1);
      config.time_mday=(byte)dt.getDate();
      config.time_hour=(byte)dt.getHours();
      config.time_min=(byte)dt.getMinutes();
      config.time_sec=(byte)dt.getSeconds();
      config.time_offset=0;
    } else {
      String date=loggerdate.getText().toString();
      String[] sep=date.split("-");
      config.time_year=(int)Double.parseDouble(sep[0]);
      config.time_mon =(byte)Double.parseDouble(sep[1]);
      config.time_mday=(byte)Double.parseDouble(sep[2]);
      String time=loggertime.getText().toString();
      sep=time.split(":");
      config.time_hour=(byte)Double.parseDouble(sep[0]);
      config.time_min =(byte)Double.parseDouble(sep[1]);
      config.time_sec =0;
      try {
        config.time_offset=(long)Double.parseDouble(delay.getText().toString());
      } catch (NumberFormatException nfe) {config.time_offset=0;}
    }
    invalidate_config();
  }
  /* Basiskonfiguration, die geaendert werden darf, ohne Auswirkung auf Daten, die 
     gespeichert weren.
   */
  private void baseconfigfrominput() {
    if(logger.has_numdataconf()) {
      try {
        config.num_data_conf=(int)Double.parseDouble(numconfig.getText().toString());
      } catch (NumberFormatException nfe) {config.num_data_conf=1;}
    }
    if(logger.has_starttime()) {
      String date=startdate.getText().toString();
      String time=starttime.getText().toString();
      String[] sep=date.split("-");
      config.starttime_year=(int)Double.parseDouble(sep[0]);
      config.starttime_mon =(int)Double.parseDouble(sep[1]);
      config.starttime_mday=(int)Double.parseDouble(sep[2]);
      sep=time.split(":");
      config.starttime_hour=(int)Double.parseDouble(sep[0]);
      config.starttime_min =(int)Double.parseDouble(sep[1]);
      config.starttime_sec =0;
    }
    if(logger.has_stoptime()) {
      String  date=stopdate.getText().toString();
      String  time=stoptime.getText().toString();
      String[] sep=date.split("-");
      config.stoptime_year=(int)Double.parseDouble(sep[0]);
      config.stoptime_mon =(int)Double.parseDouble(sep[1]);
      config.stoptime_mday=(int)Double.parseDouble(sep[2]);
      sep=time.split(":");
      config.stoptime_hour=(int)Double.parseDouble(sep[0]);
      config.stoptime_min =(int)Double.parseDouble(sep[1]);
      config.stoptime_sec =0;
    }
    config.temp_unit=(fahrenheit.isChecked()?Logger.UNIT_F:Logger.UNIT_C);
    try {
      config.templimit_low=(float)Double.parseDouble(templow.getText().toString());
      config.templimit_high=(float)Double.parseDouble(temphigh.getText().toString());
    } catch (NumberFormatException nfe) {}

    config.startcondition=(binstant.isChecked()?Logger.START_INSTANT:0)+
    			  (bbutton.isChecked()?Logger.START_BUTTON:0)+
    			  (bstart.isChecked()?Logger.START_TIME:0);
    config.stopcondition=(bstop.isChecked()?Logger.STOP_BUTTON:0)+
    			 (bstoptime.isChecked()?Logger.STOP_TIME:0)+
    			 (bstopusb.isChecked()?Logger.STOP_PDF:0)+
    			 ((!rollover.isChecked())?Logger.STOP_MEMFULL:0);

    config.displaystate=(lcdon.isChecked()?Logger.DISPLAY_ON:Logger.DISPLAY_OFF);
    if(lcd30sec.isChecked()) config.displaystate=Logger.DISPLAY_30SEKON;

    config.pause=(pause.isChecked()?1:0);
		
    if(logger.protocol!=Logger.PROTO_ELV) {
      config.led_conf=(byte) (
        	      (alarm.isChecked()?0x80:0)+
        	      (a5.isChecked()?5:0)+
        	      (a10.isChecked()?10:0)+
        	      (a15.isChecked()?15:0)+
        	      (a20.isChecked()?20:0)+
        	      (a25.isChecked()?25:0)+
        	      (a30.isChecked()?30:0));
      config.start=(byte)(binstant.isChecked()?2:1);
    } else {
      if(alarm.isChecked()) config.flag_bits2&=~1; 
      else config.flag_bits2|=1;
    }
    config.alarmtyp=(alarm.isChecked()?Logger.ALARM_ON:Logger.ALARM_OFF)+
      		    (alarmt.isChecked()?Logger.ALARM_T:Logger.ALARM_OFF)+
      		    (alarmh.isChecked()?Logger.ALARM_H:Logger.ALARM_OFF)+
      		    (alarmp.isChecked()?Logger.ALARM_P:Logger.ALARM_OFF);
    
    
    
    if(logger.loggertype==Logger.LTYP_TH ||logger.loggertype==Logger.LTYP_THP) {
      try {
    	config.humilimit_low=(float)Double.parseDouble(rhlow.getText().toString());
    	config.humilimit_high=(float)Double.parseDouble(rhhigh.getText().toString());
      } catch (NumberFormatException nfe) {}
    }
    if(logger.loggertype==Logger.LTYP_THP) {
      try {
    	    config.preslimit_low=(float)Double.parseDouble(presslow.getText().toString());
    	    config.preslimit_high=(float)Double.parseDouble(presshigh.getText().toString());
      } catch (NumberFormatException nfe) {}
    }
  }

  private byte[] build_conf() {
    configfrominput();
    if(logger.do_repair) {
      displaystatus("Create repair block blocktype="+config.block_type,4);
      displaymessage("Create repair block blocktype="+config.block_type,4);
    }
    if(logger.protocol==Logger.PROTO_VOLTCRAFT_WEATHER && config.set_interval<60) {
      displaymessage("Config WARNING: Sample rate will be set to 1 Minute.",2);
    }  
    if((logger.protocol==Logger.PROTO_ELV && config.set_interval<10) || 
       (logger.protocol==Logger.PROTO_FREETECWHITE && config.set_interval<8)) {
      displaymessage("WARNING: Sample rate is low (<10s). Logger might or might not work correctly.",2);
    }  
    int e=logger.chk_conf();
    if(e==Error.OK) return logger.build_conf();
    if(e==Error.ERR_CNONAME) displaymessage("Config ERROR: no Name!",5);
    else if(e==Error.ERR_CNAME) displaymessage("Config ERROR: Name too long! only 16 chars.",5);
    else if(e==Error.ERR_ILLNAME) displaymessage("Config ERROR: Name contains illegal characters.",5);
    else if(e==Error.ERR_NUMDATA) {
      if(logger.loggertype==Logger.LTYP_THP) 
    	displaymessage("Config ERROR: num data. [0:"+logger.memory_size/6+"]",5);
      else if(logger.loggertype==Logger.LTYP_TH) 
    	displaymessage("Config ERROR: num data. [0:"+logger.memory_size/4+"]",5);
      else displaymessage("Config ERROR: num data. [0:"+logger.memory_size/2+"]",5);
    } else if(e==Error.ERR_INTERVAL) displaymessage("Config ERROR: interval.",5);
    return null;
  }


  /* Send configuration to Logger device
   */

  private void sendConfig() {
    if(logger.isconnected) {
      displaymessage("Send-Config...",0);
      progress2.setVisibility(View.VISIBLE);
      /* When data has not been saved yet, display a warning !*/
      if(!data.issaved && data.anzdata>0) showDialog(6);  /* Handle real request*/
      else {
    	confmessagebuf = build_conf();
    	if(confmessagebuf!=null) {
    	  /* Wenn alte config noch nicht gelesen war, warnung ausgeben*/
    	  if(!logger.isreadconfig || !logger.isreaddata) showDialog(5);  /* Handle real request*/
    	  else realsendconfig();
    	}
      }
    } else Log.d(TAG, "connection problem ");
  }

  /*Konfiguriere den Logger für Messung/Datennahme*/

  private void realsendconfig() {
    writeconfig.setEnabled(false);
    new Thread(new Runnable() {public void run() {
      int ret=0;
      if(logger.protocol==Logger.PROTO_FILE && Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP_MR1 /* 22 */ && !logger.fakeconfigdir) {
        newFile();
      } else ret=logger.send_config(confmessagebuf);
      if(ret==0) {
	h.post(new Runnable() {
	  public void run() {	  
	    progress2.setVisibility(View.GONE);
	    writeconfig.setEnabled(true);
	    displaymessage(getResources().getString(R.string.message_sendconfigdone),0);
	    if(logger.protocol!=Logger.PROTO_ELV && logger.protocol!=Logger.PROTO_HID && config.start==1) Toast.makeText(getApplicationContext(), 
	  	  	    getResources().getString(R.string.message_datennahme2), Toast.LENGTH_LONG).show();
	    if(logger.protocol==Logger.PROTO_ELV && config.time_offset>0) Toast.makeText(getApplicationContext(),String.format(
	  	  	    getResources().getString(R.string.message_datennahme1),config.time_offset), Toast.LENGTH_LONG).show();
	  }
	});
      } else if(ret==Error.ERR) {
	h.post(new Runnable() {
	  public void run() {
	    progress2.setVisibility(View.GONE);
	    writeconfig.setEnabled(true);
	    displaymessage("ERROR: Send-Config failed!",5);
	  }
	});
      }
    }}).start();
  }

  /* Aktualisiere Eingabefelder */

  private void config2UI() {
    if(logger.has_startbutton()) {
      bbutton.setVisibility(View.VISIBLE);
      bbutton.setChecked(config.startcondition==Logger.START_BUTTON);
    } else {
      bbutton.setVisibility(View.GONE);
    }
    if(logger.has_starttime()) {
      starttimegroup.setVisibility(View.VISIBLE);
      bstart.setVisibility(View.VISIBLE);
      bstart.setChecked(config.startcondition==Logger.START_TIME);
    } else {
      starttimegroup.setVisibility(View.GONE);
      bstart.setVisibility(View.GONE);
    }
    binstant.setChecked(config.startcondition==Logger.START_INSTANT);
    if(logger.has_stopbutton()) {
      bstop.setVisibility(View.VISIBLE);
      bstop.setChecked((config.stopcondition&Logger.STOP_BUTTON)!=0);
    } else {
      bstop.setVisibility(View.GONE);
    }
    if(logger.has_stoptime()) {
      stoptimegroup.setVisibility(View.VISIBLE);
      bstoptime.setVisibility(View.VISIBLE);
    } else {
      stoptimegroup.setVisibility(View.GONE);
      bstoptime.setVisibility(View.GONE);
    }
    if(logger.has_rollover()) {
      rollover.setVisibility(View.VISIBLE);
      rollover.setChecked((config.stopcondition&Logger.STOP_MEMFULL)==0);
    } else rollover.setVisibility(View.GONE);
    if(logger.has_delay()) {
      delay.setVisibility(View.VISIBLE);
      delay.setText(""+config.time_offset);
    } else delay.setVisibility(View.GONE);

    if(logger.has_lcd() || logger.has_lcd30()) {
      radioGroupLCD.setVisibility(View.VISIBLE);
      lcdon.setVisibility(View.VISIBLE);
      lcdon.setChecked(config.displaystate==Logger.DISPLAY_ON);
      if(logger.has_lcd()) {
        lcdoff.setVisibility(View.VISIBLE);
	lcdoff.setChecked(config.displaystate==Logger.DISPLAY_OFF);
      } else lcdoff.setVisibility(View.GONE);
    } else radioGroupLCD.setVisibility(View.GONE);

    if(logger.has_lcd30()) {
      lcd30sec.setVisibility(View.VISIBLE);
      lcd30sec.setChecked(config.displaystate==Logger.DISPLAY_30SEKON);
    } else lcd30sec.setVisibility(View.GONE);

    if(logger.has_led()) {
      blinktext.setVisibility(View.VISIBLE);
      radioGroupLED.setVisibility(View.VISIBLE);
      a5.setChecked((((int)config.led_conf) & 0x1f)==5);
      a10.setChecked((((int)config.led_conf) & 0x1f)==10);
      a15.setChecked((((int)config.led_conf) & 0x1f)==15);
      a20.setChecked((((int)config.led_conf) & 0x1f)==20);
      a25.setChecked((((int)config.led_conf) & 0x1f)==25);
      a30.setChecked((((int)config.led_conf) & 0x1f)==30);
    } else {
      blinktext.setVisibility(View.GONE);
      radioGroupLED.setVisibility(View.GONE);
    }
    if(logger.has_numdataconf()) {
      numdataconfgroup.setVisibility(View.VISIBLE);
      numconfig.setText(""+config.num_data_conf);
      maxnumrec.setText(""+config.num_data_conf+" ("+Physics.duration(config.num_data_conf,config.set_interval)+")");
    } else {
      numdataconfgroup.setVisibility(View.GONE);
      maxnumrec.setText("-");
    }
    if(logger.has_pause()) pause.setVisibility(View.VISIBLE);
    else pause.setVisibility(View.GONE);

    if(logger.has_actualval()) {
      valuedisplay.setVisibility(View.VISIBLE);
      if(logger.loggertype==Logger.LTYP_TEMP)
        valuedisplay.setText(String.format(US,"%.1f C",logger.actual_temp));
      else if(logger.loggertype==Logger.LTYP_TH)
        valuedisplay.setText(String.format(US,"%.1f C, %.1f %%",logger.actual_temp,logger.actual_humi));
      else if(logger.loggertype==Logger.LTYP_THP)
        valuedisplay.setText(String.format(US,"%.1f C, %.1f %%, %.1f hPa",
	  logger.actual_temp,logger.actual_humi,logger.actual_pres));
        valuedisplay.invalidate();
    } else valuedisplay.setVisibility(View.GONE);
    if(logger.has_minmaxval()) {
      minmaxdisplay1.setVisibility(View.VISIBLE);
      minmaxdisplay1.setText(String.format(US,"%.1f C, %.1f C",logger.minval_temp,logger.maxval_temp));
      minmaxdisplay1.invalidate();
      if(logger.loggertype==Logger.LTYP_THP || logger.loggertype==Logger.LTYP_TH) {
        minmaxdisplay2.setVisibility(View.VISIBLE);
        minmaxdisplay2.setText(String.format(US,"%.1f %%, %.1f %%",logger.minval_humi,logger.maxval_humi));
        minmaxdisplay2.invalidate();
        if(logger.loggertype==Logger.LTYP_THP) {
          minmaxdisplay3.setVisibility(View.VISIBLE);
          minmaxdisplay3.setText(String.format(US,"%.1f hPa, %.1f hPa",logger.minval_pres,logger.maxval_pres));
          minmaxdisplay3.invalidate();
	} else minmaxdisplay3.setVisibility(View.GONE);
      } else {
        minmaxdisplay2.setVisibility(View.GONE);
        minmaxdisplay3.setVisibility(View.GONE);
      }
    } else {
      minmaxdisplay1.setVisibility(View.GONE);
      minmaxdisplay2.setVisibility(View.GONE);
      minmaxdisplay3.setVisibility(View.GONE);
    }
    celsius.setChecked(config.temp_unit==Logger.UNIT_C);
    fahrenheit.setChecked(config.temp_unit==Logger.UNIT_F);



    if(logger.has_limits()) {
      alarm.setVisibility(View.VISIBLE);
      switch(logger.loggertype) {
      case Logger.LTYP_THP:
	presshigh.setVisibility(View.VISIBLE);
	presslow.setVisibility(View.VISIBLE);
	alarmp.setVisibility(View.VISIBLE);
	alarmp.setChecked((config.alarmtyp&Logger.ALARM_P)!=0);
	presshigh.setText(""+config.preslimit_high);
	presslow.setText(""+config.preslimit_low);
      case Logger.LTYP_TH:
	rhhigh.setVisibility(View.VISIBLE);
	rhlow.setVisibility(View.VISIBLE);
	alarmh.setVisibility(View.VISIBLE);      
	alarmh.setChecked((config.alarmtyp&Logger.ALARM_H)!=0);
	rhhigh.setText(""+config.humilimit_high);
	rhlow.setText(""+config.humilimit_low);
      case Logger.LTYP_TEMP:
        alarmt.setVisibility(View.VISIBLE);
	alarmt.setChecked((config.alarmtyp&Logger.ALARM_T)!=0);
	temphigh.setVisibility(View.VISIBLE);
	templow.setVisibility(View.VISIBLE);
	temphigh.setText(""+config.templimit_high);
	templow.setText(""+config.templimit_low);
      default:
      }
      switch(logger.loggertype) {
      case Logger.LTYP_TEMP:
        alarmh.setVisibility(View.GONE);
        rhhigh.setVisibility(View.GONE);
        rhlow.setVisibility(View.GONE);
      case Logger.LTYP_TH:
        alarmp.setVisibility(View.GONE);
        presshigh.setVisibility(View.GONE);
        presslow.setVisibility(View.GONE);
      case Logger.LTYP_THP:
      default:
      }
    } else {
      alarm.setVisibility(View.GONE);
      alarmt.setVisibility(View.GONE);
      alarmh.setVisibility(View.GONE);
      alarmp.setVisibility(View.GONE);
      temphigh.setVisibility(View.GONE);
      templow.setVisibility(View.GONE);
      rhhigh.setVisibility(View.GONE);
      rhlow.setVisibility(View.GONE);
      presshigh.setVisibility(View.GONE);
      presslow.setVisibility(View.GONE);
    }


    if(logger.protocol==Logger.PROTO_FILE) bstopusb.setVisibility(View.VISIBLE);
    else bstopusb.setVisibility(View.GONE);

    switch(logger.protocol) {
      case Logger.PROTO_VOLTCRAFT:
      case Logger.PROTO_VOLTCRAFT_NEW:
      case Logger.PROTO_VOLTCRAFT_WEATHER:
	binstant.setChecked((config.start&255)==2);
	bbutton.setChecked((config.start&255)!=2);
	systime.setText(getResources().getString(R.string.use_systime));
	extrafunctions.setVisibility(View.GONE);
	alarm.setChecked((((int)config.led_conf) & 0x80)==0x80);
	break;
      case Logger.PROTO_FREETECWHITE:
	binstant.setChecked((config.start&255)==2);
	bbutton.setChecked((config.start&255)!=2);
	systime.setText(getResources().getString(R.string.use_systime));
	alarm.setChecked((((int)config.led_conf) & 0x80)==0x80);
	extrafunctions.setVisibility(View.VISIBLE);
	batshot.setVisibility(View.GONE);
	break;
      case Logger.PROTO_HID:
	if(logger.config.rawinputreading==0) valuedisplay.setVisibility(View.GONE);
	else {
	  valuedisplay.setVisibility(View.VISIBLE);
	  valuedisplay.setText(""+Freetec.raw2temp(logger.config.rawinputreading)+
	    String.format(US,"C,%d%%,%dm",
	    (logger.config.rawinputreading&0xff00)>>8,
	    logger.config.rawinputreading&0xff));
	}
	extrafunctions.setVisibility(View.GONE);
	break;
      case Logger.PROTO_ELV:
	if(logger.config.rawinputreading==0) valuedisplay.setVisibility(View.GONE);
	else {
	  valuedisplay.setVisibility(View.VISIBLE);
	  valuedisplay.setText(""+(float)Math.round(1000.0*(float)logger.raw2t((short)logger.config.rawinputreading))/1000.0+" "+logger.config.getunit());
	}
	systime.setText(getResources().getString(R.string.use_systime2));
	alarm.setChecked((config.flag_bits2&1)==0);
	if(config.block_type==6) {
	  extrafunctions.setVisibility(View.VISIBLE);
	  batshot.setVisibility(View.VISIBLE);
	  batshot.setEnabled(true);
	  shot.setEnabled(true);
	  clear.setEnabled(true);
	} else extrafunctions.setVisibility(View.GONE);
	break;
      case Logger.PROTO_CP210X:
	binstant.setChecked((config.start&255)==2);
	bbutton.setChecked((config.start&255)!=2);
	extrafunctions.setVisibility(View.GONE);
	systime.setText(getResources().getString(R.string.use_systime));
	alarm.setChecked((((int)config.led_conf) & 0x80)==0x80);
	break;
      case Logger.PROTO_FILE:
	extrafunctions.setVisibility(View.GONE);
	systime.setText(getResources().getString(R.string.use_systime));
	alarm.setChecked((config.alarmtyp&Logger.ALARM_ON)!=0);
	binstant.setChecked(config.startcondition==Logger.START_INSTANT);
	bbutton.setChecked(config.startcondition==Logger.START_BUTTON);
	bstart.setChecked(config.startcondition==Logger.START_TIME);
	bstop.setChecked((config.stopcondition&Logger.STOP_BUTTON)!=0);
	bstoptime.setChecked((config.stopcondition&Logger.STOP_TIME)!=0);
	bstopusb.setChecked((config.stopcondition&Logger.STOP_PDF)!=0);
        break;
      default:
        binstant.setChecked((config.start&255)==2);
        bbutton.setChecked((config.start&255)!=2);
        systime.setText(getResources().getString(R.string.use_systime));
        extrafunctions.setVisibility(View.GONE);
    }
    numrec.setText(""+config.num_data_rec+" ("+Physics.duration(config.num_data_rec,config.got_interval)+")");
    manzdata.setText(""+data.anzdata);
    minterval.setText(""+config.set_interval);
    logname.setText(""+config.getname());
    loggerdate.setText(String.format(Locale.US,"%d-%02d-%02d",(config.time_year&0xffff),((config.time_mon & 255)),(config.time_mday & 255)));
    loggertime.setText(String.format(Locale.US,"%02d:%02d:00",(config.time_hour&255),(config.time_min&255)));
  }

/*********** Storage Access Framework *************************/
  public void newFile() {
    if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.KITKAT) { /* 19 */
      Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
      intent.addCategory(Intent.CATEGORY_OPENABLE);
      intent.setType("application/octet-stream");
      intent.putExtra(Intent.EXTRA_TITLE, "SetLog.bn1");
      startActivityForResult(intent, CREATE_REQUEST_CODE);
    }
  }
  public void saveFile() {
    if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.KITKAT) { /* 19 */
      Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
      intent.addCategory(Intent.CATEGORY_OPENABLE);
      intent.setType("application/octet-stream");
      intent.putExtra(Intent.EXTRA_TITLE, "SetLog.bn1");
      startActivityForResult(intent, SAVE_REQUEST_CODE);
    }
  }
  public void openFile() {
    if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.KITKAT) { /* 19 */
      Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
      intent.addCategory(Intent.CATEGORY_OPENABLE);
      intent.setType("application/octet-stream");
      startActivityForResult(intent, OPEN_REQUEST_CODE);
    }
  }
  public void openTree() {
    if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP) { /* 21 */
      Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
      startActivityForResult(intent, OPEN_TREE_CODE);
    }
  }

  private static final int CREATE_REQUEST_CODE = 40;
  private static final int OPEN_REQUEST_CODE = 41;
  private static final int SAVE_REQUEST_CODE = 42;
  private static final int OPEN_TREE_CODE = 43;

  public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
    Uri currentUri=null;
    if(resultCode==Activity.RESULT_OK) {
      if(requestCode==OPEN_TREE_CODE) {
    	Uri treeUri = resultData.getData();
        logger.log2comproto("User granted TREE access to URI="+treeUri,1);
//        DocumentFile pickedDir = DocumentFile.fromTreeUri(this, treeUri);
//        logger.log2comproto("Directory root tree: "+pickedDir,0);
        // List all existing files inside picked directory
//        for (DocumentFile file : pickedDir.listFiles()) {
//            logger.log2comproto("Found file " + file.getName() + " with size " + file.length(),0);
//        }

		  
      } else if(requestCode==CREATE_REQUEST_CODE) {
    	if(resultData!=null) {
	  currentUri=resultData.getData();
	  Toast.makeText(getApplicationContext(), "Success: CREATE "+currentUri, Toast.LENGTH_LONG).show();
     	  writeFileContent(currentUri);
    	}
      } else if(requestCode==SAVE_REQUEST_CODE) {
    	if(resultData!=null) {
    	  currentUri=resultData.getData();
	  Toast.makeText(getApplicationContext(), "Success: WRITE "+currentUri, Toast.LENGTH_LONG).show(); 
     	  writeFileContent(currentUri);
    	}
      } else if(requestCode==OPEN_REQUEST_CODE) {
        if(resultData!=null) {
          currentUri=resultData.getData();
          try {
	    logger.log2comproto("User granted access to URI="+currentUri,1);
            String content =readFileContent(currentUri);
	    Toast.makeText(getApplicationContext(), "Success: READ ", Toast.LENGTH_LONG).show(); 
            // textView.setText(content);
	    logger.Configfilecontent=content;
	    logger.Path=currentUri.getPath();
	    logger.log2comproto("File content: ="+content,2);
	    logger.check_media_logger_content();
	    if(logger.isconnected) setconnected();
          } catch (IOException e) {
	    Toast.makeText(getApplicationContext(), "IO-Error", Toast.LENGTH_LONG).show();
	    logger.log2comproto("IO-Error: "+e,0); 
          }
        }
      }
    }
  }

/* ganzes File einlesen und als String zurueckgeben. */

  private String readFileContent(Uri uri) throws IOException {
    InputStream inputStream =getContentResolver().openInputStream(uri);
    BufferedReader reader =new BufferedReader(new InputStreamReader(inputStream));
    StringBuilder stringBuilder = new StringBuilder();
    String currentline;
    while((currentline = reader.readLine()) != null) {
      stringBuilder.append(currentline + "\n");
    }
    inputStream.close();
    return stringBuilder.toString();
  }

/* File schreiben: in diesem Fall die Konfiguration eines MediaLoggers */

  private void writeFileContent(Uri uri) {
    try{	      
      ParcelFileDescriptor pfd = this.getContentResolver().openFileDescriptor(uri, "w");
      FileOutputStream fileOutputStream =new FileOutputStream(pfd.getFileDescriptor());

      /* build the config block again to set actual time/date */
      confmessagebuf=logger.build_conf();
      fileOutputStream.write(confmessagebuf);
      fileOutputStream.close();
      pfd.close();
    } 
    catch (FileNotFoundException e) {e.printStackTrace();} 
    catch (IOException e)           {e.printStackTrace();}
  }


/*********** Versionsnummer der App zurueckgeben *************************/
  private String applicationVersion() {
    try { return getPackageManager().getPackageInfo(getPackageName(), 0).versionName; }
    catch (NameNotFoundException x)  { return "unknown"; }
  }
}

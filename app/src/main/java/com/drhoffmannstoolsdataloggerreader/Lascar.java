package com.drhoffmannstoolsdataloggerreader;

/* Lascar.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.util.Log;

public class Lascar {
  private static final String TAG = "LASCAR"; 
  /*Flag-Bits*//*
   *	    x xxxx xxxx xxxx
   *			|||+--HIGH_ALARM_STATE: 1 = High Alarm Enabled, 0 = High Alarm Disabled
   *			||+---LOW_ALARM_STATE:  1 = Low Alarm Enabled, 0 = Low Alarm Disabled
   *			|+----HIGH_ALARM_LATCH: 1 = High Alarm Indication Hold Enabled, 0 disabled
   *			+-----LOW_ALARM_LATCH:  1 = Low Alarm Indication Hold Enabled, 0 disabled
   *		      +-------CH2_HIGH_ALARM_STATE  EL-USB-2 Only
   *		     +--------CH2_LOW_ALARM_STATE   EL-USB-2 Only
   *		    +---------CH2_HIGH_ALARM_LATCH  EL-USB-2 Only
   *		   +----------CH2_LOW_ALARM_LATCH   EL-USB-2 Only
   *		 +------------LOGGING_STATE:	1 = Delayed Start or Logging,  0 = Off
   *		+-------------UNREAD:		1 = Data has NOT been downloaded.
   *	       +--------------BATTERY_LOW:	1 = During the last acquistion battery level dropped to a low level 
  					//     and logging stopped
   *	      +---------------BATTERY_FAIL:	1= During the last acquistion battery level dropped to a critical level 
  					//     and logging stopped
   *	    +-----------------SENSOR_FAIL:	1 = During last acquisition there were sensor errors. EL-USB-2 Only

   */ 
  public final static int HIGH_ALARM_STATE=1;
  public final static int LOW_ALARM_STATE=2;
  public final static int HIGH_ALARM_LATCH=4;
  public final static int LOW_ALARM_LATCH=8;
  public final static int CH2_HIGH_ALARM_STATE=0x10;
  public final static int CH2_LOW_ALARM_STATE=0x20;
  public final static int CH2_HIGH_ALARM_LATCH=0x40;
  public final static int CH2_LOW_ALARM_LATCH=0x80;
  public final static int LOGGING_STATE=0x100;
  public final static int UNREAD=0x200;
  public final static int BATTERY_LOW=0x400;
  public final static int BATTERY_FAIL=0x800;
  public final static int SENSOR_FAIL=0x1000;


  /* Block Types: */
  public final static int BLOCK_TYPE_UNKNOWN=0;
  public final static int BLOCK_TYPE_ELUSB1A=1;
  public final static int BLOCK_TYPE_ELUSB1B=2;
  public final static int BLOCK_TYPE_ELUSB2=3;
  public final static int BLOCK_TYPE_ELUSB3A=4;
  public final static int BLOCK_TYPE_ELUSB3B=6;
  public final static int BLOCK_TYPE_ELUSB4A=5;
  public final static int BLOCK_TYPE_ELUSB4B=7;
  public final static int BLOCK_TYPE_ELUSBLITE=8;
  public final static int BLOCK_TYPE_ELUSBCO=9;
  public final static int BLOCK_TYPE_ELUSBTC=10;
  public final static int BLOCK_TYPE_ELUSBCO300=11;
  public final static int BLOCK_TYPE_ELUSB2LCD=12;
  public final static int BLOCK_TYPE_ELUSB2PLUS=13;
  public final static int BLOCK_TYPE_ELUSB1PRO=14;
  public final static int BLOCK_TYPE_ELUSBTCLCD=15;
  public final static int BLOCK_TYPE_ELUSB2LCDPLUS=16;
  public final static int BLOCK_TYPE_ELUSB5=17;
  public final static int BLOCK_TYPE_ELUSB1RCG=18;
  public final static int BLOCK_TYPE_ELUSB1LCDA=19;
  public final static int BLOCK_TYPE_ELOEM3=20;
  public final static int BLOCK_TYPE_ELUSB1LCDB=21;
  

  public static String blocktype2string(int bt) {
     switch(bt) {
     case BLOCK_TYPE_UNKNOWN:     return "unknown";
     case BLOCK_TYPE_ELUSB1A:     return "EL-USB-1a";
     case BLOCK_TYPE_ELUSB1B:     return "EL-USB-1b";
     case BLOCK_TYPE_ELUSB2:      return "EL-USB-2";
     case BLOCK_TYPE_ELUSB3A:     return "EL-USB-3a";
     case BLOCK_TYPE_ELUSB3B:     return "EL-USB-3b";
     case BLOCK_TYPE_ELUSB4A:     return "EL-USB-4a";
     case BLOCK_TYPE_ELUSB4B:     return "EL-USB-4b";
     case BLOCK_TYPE_ELUSBLITE:   return "EL-USB-LITE";
     case BLOCK_TYPE_ELUSBCO:     return "EL-USB-CO";
     case BLOCK_TYPE_ELUSBTC:     return "EL-USB-TC";
     case BLOCK_TYPE_ELUSBCO300:  return "EL-USB-CO300";
     case BLOCK_TYPE_ELUSB2LCD:   return "EL-USB-2-LCD";
     case BLOCK_TYPE_ELUSB2PLUS:  return "EL-USB-2+";
     case BLOCK_TYPE_ELUSB1PRO:   return "EL-USB-1-PRO";
     case BLOCK_TYPE_ELUSBTCLCD:  return "EL-USB-TC-LCD";
     case BLOCK_TYPE_ELUSB2LCDPLUS: return "EL-USB-2-LCD+";
     case BLOCK_TYPE_ELUSB5:      return "EL-USB-5";
     case BLOCK_TYPE_ELUSB1RCG:   return "EL-USB-1-RCG";
     case BLOCK_TYPE_ELUSB1LCDA:  return "EL-USB-1a-LCD";
     case BLOCK_TYPE_ELOEM3:      return "EL-OEM-3";
     case BLOCK_TYPE_ELUSB1LCDB:  return "EL-USB-1b-LCD";
     default: return "?";
    }
  }


  /* Transmission Commands*/

  public final static int TCMD_GETCONFIG=0x00; // Read device configuration block.
  public final static int TCMD_SETCONFIG=0x01; // Write device configuration block.
  public final static int TCMD_GETDATA=0x03;   // Read Data



  /* Config Kommandotypen*/
  
  public final static int CMD_STD=0x00; // Transfer of standard parameter block
  public final static int CMD_LIVE=0x01; // Take reading (factory use only)*
  public final static int CMD_BAT=0x02; // Take a battery level reading (factory use only)*
  public final static int CMD_CAL=0x04; // Transmit calibration parameter block (factory use only)
  public final static int CMD_CLR=0x07; // Clear Readings (factory use only, Sets Sample Count[30] to zero)


  public static int chk_conf(LoggerConfig config, int loggertype, boolean do_repair) {
    if(do_repair) return Error.OK;
    String n=config.getname();
    if(n.length()==0) return Error.ERR_CNONAME;
    if(n.length()>16) return Error.ERR_CNAME;
    if(config.set_interval>86400 ||config.set_interval<=0) return Error.ERR_INTERVAL;
    return Error.OK;
  }

  public static byte[] build_conf(Logger logger) {
    //int size=64;
    //if(block_type==1) size=64;
    //else if(block_type==2) size=64;
    //else if(block_type==3) size=128;
    //else if(block_type>=4 && block_type<8) size=256;
    byte cbuf[]=read_configblock(logger);
    final LoggerConfig config=logger.config;
    ByteBuffer mes;
    if(cbuf==null) cbuf=new byte[logger.packet_size];
    mes=ByteBuffer.allocate(cbuf.length);
    mes.position(0);
    mes.put(cbuf);
    mes.position(0);
    mes.order(ByteOrder.LITTLE_ENDIAN);
    String n="EasyLog USB";
    config.block_cmd=CMD_STD;
    if(logger.do_repair) {
      if(config.block_type<4) {
        if(config.temp_unit==Logger.UNIT_C) {
          config.calibration_Mvalue=(float)0.5;
          config.calibration_Cvalue=(float)-40;
        } else {
          config.calibration_Mvalue=(float)1;
          config.calibration_Cvalue=(float)-40;
        }
        config.setversion("v2.0");
      } else if(config.block_type==BLOCK_TYPE_ELUSB3B) {
	//config.block_cmd=CMD_BAT;
	config.calibration_Mvalue=(float)1/26783;
	config.calibration_Cvalue=(float)-11.25*config.calibration_Mvalue;
	config.scaling_factor=(float)12.914893;
	config.setversion("[C@4");
      } else {
        config.calibration_Mvalue=(float)1.0; // 0.5
        config.calibration_Cvalue=(float)-40;
        config.setversion("v1.0");
      }

      config.flag_bits=0;
      config.set_interval=60;
      config.time_offset=0;
      config.serial_number=10023801;
      mes.put(0,(byte) config.block_type);
      if(config.block_type<4) {  // Nur EL-USB 1,2 
	if(config.temp_unit==Logger.UNIT_C) {
	  config.calibration_Mvalue=(float)0.5;
	  config.calibration_Cvalue=(float)-40;
	} else {
	  config.calibration_Mvalue=(float)1;
	  config.calibration_Cvalue=(float)-40;
	}
      }
      mes.putFloat(36,config.calibration_Mvalue);
      mes.putFloat(40,config.calibration_Cvalue);
      if(cbuf.length>=112) mes.putFloat(108,config.scaling_factor);
      mes.position(48);
      String v=config.version.toString();
      mes.put(v.getBytes());

      mes.putLong(52,config.serial_number);
      mes.put(1,(byte) config.block_cmd);
      Log.d(TAG,"Create repair block blocktype="+config.block_type+" (was: "+cbuf[0]+") name="+n);
      logger.do_repair=false;
    } else {
      n=config.getname();
      if(n.length()==0 || n.length()>16) {
        return null;
      }
    }
    mes.put(1,(byte) config.block_cmd); 
    mes.position(2);
    mes.put(n.getBytes());
    mes.put(2+n.length(),(byte)0);

    mes.put(18,config.time_hour);
    mes.put(19,config.time_min);
    mes.put(20,config.time_sec);
    mes.put(21,config.time_mday);
    mes.put(22,config.time_mon);
    mes.put(23,(byte)(config.time_year-2000));
    // time_offset=0;
    mes.putLong(24,config.time_offset);
    mes.putShort(28,(short)config.set_interval);
    mes.putShort(30,(short)config.num_data_conf);
    config.flag_bits |= (Lascar.LOGGING_STATE);
    mes.putShort(32,(short)config.flag_bits);
    if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP ||logger.loggertype==Logger.LTYP_TEMP) {
      if(config.temp_unit==Logger.UNIT_C) {
        mes.put(34,(byte)(config.templimit_high*2+80));
        mes.put(35,(byte)(config.templimit_low*2+80));
      } else {
        mes.put(34,(byte)(config.templimit_high+40));
        mes.put(35,(byte)(config.templimit_low+40));
      }
    }
    // mes.putFloat(36,config.calibration_Mvalue);
    // mes.putFloat(40,config.calibration_Cvalue);
    //      mes.putShort(44,(short)rawinputreading);	    
    mes.put(46,(byte)config.temp_unit);
    // mes.position(48);
    ///     String v=version.toString();
    //      mes.put(v.getBytes());
    // mes.putLong(52,config.serial_number);
    if(logger.loggertype==Logger.LTYP_TH || logger.loggertype==Logger.LTYP_THP) {
      mes.put(56,(byte)(config.humilimit_high*2));
      mes.put(57,(byte)(config.humilimit_low*2));
    }
    return mes.array();
  }
  public static byte[] read_configblock(Logger logger) {
    byte cbuf[]=null;
    int size;
    Log.d(TAG,"readconfigblock...");
    logger.unlock_device();
    logger.sendCommand((byte)TCMD_GETCONFIG, (byte)0xff, (byte)0xff);
    size=logger.readresponse((byte)0x00, (byte)0xff, (byte)0xff);
    if(size<0) {
      logger.updateMessage("ERROR: transmission timeout.",5);
    }
    else if(size==0) logger.updateMessage("Config? no data available",1);
    else logger.updateMessage("Config OK. size=0x"+String.format("%x",size)+" "+size+" Bytes can be read.",0);  	    
    if(size>0 && logger.isconnected) {
			cbuf=new byte[size];
			int ret=0;
			int trys=0;
			while(ret!=size && trys<10) {
				ret=logger.receive(cbuf);
				if(ret<0) Log.d(TAG,"Error in reading configuration block from USB 500");
				trys++;
			} 
			if(ret!=size) cbuf=null;
		} 
		logger.lock_device();
		return cbuf;
	}
  public static int write_configblock(byte[] cbuf,Logger logger) {
    int ret=Error.OK;
    Log.d(TAG,"writeconfigblock...");
    if(logger.isconnected) {
      logger.unlock_device();
      logger.sendCommand((byte)TCMD_SETCONFIG,(byte)(cbuf.length&0xff),(byte)((cbuf.length>>8)&0xff));
      logger.send(cbuf);
      if(logger.readack()!=1) ret=Error.ERR;
      logger.lock_device();
    } else {
      Log.d(TAG, "connection problem ");
      ret=Error.ERR;
    }
    return(ret);
  }

    //Experimentell
	public static int read_actual_value(Logger logger) {
		byte cbuf[]=read_configblock(logger);
		if(cbuf==null) return Error.ERR;
		Log.d(TAG,"Read actual value...");
		cbuf[1]=CMD_LIVE; 
		write_configblock(cbuf,logger);
		cbuf=read_configblock(logger);
		if(cbuf==null) return Error.ERR;
		return ((cbuf[44]&0xff)<<8)|(cbuf[45]&0xff);
	}
	//Experimentell
	public static int read_battery_value(Logger logger) {
		byte cbuf[]=read_configblock(logger);
		if(cbuf==null) return Error.ERR;
		Log.d(TAG,"Read battery value...");
		cbuf[1]=CMD_BAT; 
		write_configblock(cbuf,logger);
		cbuf=read_configblock(logger);
		if(cbuf==null) return Error.ERR;
		return ((cbuf[45]&0xff)<<8)|(cbuf[44]&0xff);
	}
	 //Experimentell
		public static void erase_data(Logger logger) {
			byte cbuf[]=read_configblock(logger);
			if(cbuf==null) return;
			Log.d(TAG,"Clear all data...");
			cbuf[1]=CMD_CLR; 
			write_configblock(cbuf,logger);
		}


	public static int stop_logger(Logger logger) {
		byte cbuf[]=read_configblock(logger);
		if(cbuf==null) return Error.ERR;
		Log.d(TAG,"Stop Logger...");
		cbuf[24]=0; /*time_offset=0*/
		cbuf[25]=0;
		cbuf[26]=0;
		cbuf[27]=0;
		//  flag_bits=(short)(((int)flag_bits)&(~LOGGING_STATE));
		cbuf[32]=cbuf[32];
		cbuf[33]=(byte)(cbuf[33]&0xfe); //LOGGING_STATE loeschen
		return write_configblock(cbuf,logger);
  }

  public static String get_status(Logger logger) {
    String ret="";
    if(logger.isreadconfig) {
      ret=blocktype2string(logger.config.block_type)+" "+logger.typestring();
      if((logger.config.flag_bits&LOGGING_STATE)>0 && logger.config.time_offset==0) ret=ret+": LOGGING,";
      else if((logger.config.flag_bits&LOGGING_STATE)>0 && logger.config.time_offset>=0) ret=ret+": NOT yet STARTED (start in "+logger.config.time_offset+"s),";
      else if((logger.config.flag_bits&LOGGING_STATE)==0) ret=ret+": STOPPED,";
      if(logger.config.num_data_rec==0) ret=ret+" NO DATA";
      else if((logger.config.flag_bits&UNREAD)==0)  ret=ret+" DATA already downloaded";
      else ret=ret+" DATA not downloaded";
      if(logger.config.rawinputreading!=0) {
        ret=ret+" - Latest reading: "+logger.raw2t((short)logger.config.rawinputreading)+" "+logger.config.getunit()+" ("+logger.config.rawinputreading+")";
      }
    } else ret="Config has not yet been read";
    return ret+".";
  }


  public static int get_config(Logger logger) {
    byte cbuf[]=read_configblock(logger);
    if(cbuf==null) return Error.ERR;
    if(cbuf.length<64) return Error.ERR_BAD;
    /* Jetzt ELV protokollbuffer entschluesseln*/
			LoggerConfig config=logger.config;
			config.configbuf=cbuf.clone();
			int ret=Error.OK;
			config.mglobal_message="";
			ByteBuffer buffer = ByteBuffer.allocate(cbuf.length);
			buffer.position(0);
			buffer.put(cbuf);
			buffer.position(0);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			/* Jetzt ELV protokollbuffer entschluesseln*/
			int i;
			for(i=0;i<16;i++) {
				config.name[i]=(char)(cbuf[2+i]&255);
				if(config.name[i]==0) break;
			}

			Log.d(TAG,"name     ="+config.getname());
			config.time_hour=buffer.get(18);
			config.time_min=buffer.get(19);
			config.time_sec=buffer.get(20);
			config.time_mday=buffer.get(21);
			config.time_mon=buffer.get(22);
			config.time_year=(int)buffer.get(23)+2000;
			//time_offset=buffer.getInt(24);
			config.got_interval=(int)buffer.getShort(28);
			config.set_interval=config.got_interval;
			config.num_data_rec=(int)buffer.getShort(30);
			//flag_bits=buffer.getShort(32);

			config.time_offset=buffer.getInt(24);
			config.flag_bits=buffer.getShort(32);
			Log.d(TAG,"flag_bits="+config.flag_bits+" / "+Lascar.LOGGING_STATE+" / "+(config.flag_bits &Lascar.LOGGING_STATE));

			config.block_type=cbuf[0]&0xff;
			config.block_cmd=cbuf[1]&0xff;
			Log.d(TAG,"block_type="+config.block_type+"; block_cmd="+config.block_cmd);
			if(config.block_type<4) {  // nur EL-USB 1,2
				config.templimit_high=(cbuf[34]&0xff);
				config.templimit_low=(cbuf[35]&0xff);
			} else config.templimit_high=buffer.getShort(34);
			
			config.calibration_Mvalue=buffer.getFloat(36);
			config.calibration_Cvalue=buffer.getFloat(40);
			config.rawinputreading=buffer.getShort(44);
			config.temp_unit=buffer.get(46);

			if(config.block_type<4) { // nur EL-USB 1,2
				if(config.temp_unit==Logger.UNIT_C) {
					config.templimit_high=(config.templimit_high-80)/2;
					config.templimit_low=(config.templimit_low-80)/2;
				} else {
					config.templimit_high=(config.templimit_high-40);
					config.templimit_low=(config.templimit_low-40);
				}
			}
			config.flag_bits2=cbuf[47];
			for(i=0;i<4;i++) config.version[i]=(char)(cbuf[48+i]&0xff);
			for(i=32;i<48;i++) Log.d(TAG,":"+i+": "+(cbuf[i]&0xff));
			config.serial_number=buffer.getInt(52);
			if(config.block_type<4) { // nur EL-USB 1,2
				config.humilimit_high=(buffer.get(56)&0xff);
				config.humilimit_low=(buffer.get(57)&0xff);
				config.humilimit_high=config.humilimit_high/2;
				config.humilimit_low=config.humilimit_high/2;
			} else {
				config.templimit_low=buffer.getShort(56);
			}
			config.rollover_count=buffer.get(58);
			config.scaling_factor=(float)1.0;
			config.setunit("unit");
			config.setdefunit("default unit");
			config.setdefiunit("def in-unit");
			config.setcal1("");
			config.setcal2("");
			config.setcal3("");
			config.setcal4("");
			config.sethalmtxt("");
			config.setlalmtxt("");

			config.setdefcal1("");
			config.setdefcal2("");
			config.setdefcal3("");
			config.setdefcal4("");
			config.setdefhalmtxt("");
			config.setdeflalmtxt("");
			config.setdefrange("");

			if(cbuf.length>=128) config.scaling_factor=buffer.getFloat(108);
			if(config.scaling_factor==0) config.scaling_factor=(float)1.0;

  //		      0   1   2  3   4   5   6   7    
  final int csizes[]={64,64, 64,128,256,256,256,256};
  final int psizes[]={64,64,512,128,512,512,512,512};
  final int msizes[]={0xfe00,0x4000,0x4000,0x8000,0xfe00,0xfe00,0xfe00,0xfe00};
			
			if(config.block_type<csizes.length) {
				config.cbufsize=csizes[config.block_type];
				logger.packet_size=psizes[config.block_type];
				logger.memory_size=msizes[config.block_type];
			} else {
				config.cbufsize=256;
				logger.packet_size=256;
				logger.memory_size=0xfe00;
			}
			


    /* Find out which ELV Logger type is connected: */
    
    switch(config.block_type) {
    case BLOCK_TYPE_ELUSB1LCDA:
    case BLOCK_TYPE_ELUSB1LCDB:
      ret=Error.WARN_NOTTESTED; 
       /*... and continue */
    case BLOCK_TYPE_ELUSB1A:
    case BLOCK_TYPE_ELUSB1B:
      logger.loggertype=Logger.LTYP_TEMP;
      config.setdefunit("°C");
      config.setdefiunit("°C");
      if(config.temp_unit==Logger.UNIT_C) config.setunit("°C");
      else config.setunit("°F");
      break;
    case BLOCK_TYPE_ELUSB2LCD:
    case BLOCK_TYPE_ELUSB2PLUS:
      ret=Error.WARN_NOTTESTED;
      logger.packet_size=512;
      logger.memory_size=0x8000;
      // logger.set_calibration((float)0.5,-40,(float)0.5,0);
       /*... and continue */
    case BLOCK_TYPE_ELUSB2:
      logger.loggertype=Logger.LTYP_TH;
      logger.calibration_tA=config.calibration_Mvalue;
      logger.calibration_tB=config.calibration_Cvalue;
      logger.calibration_rA=(float)0.5;
      logger.calibration_rB=0;
      config.setdefunit("°C");
      config.setdefiunit("°C");
      if(config.temp_unit==Logger.UNIT_C) config.setunit("°C");
      else config.setunit("°F");
      break;
    case BLOCK_TYPE_ELUSB3A:
    case BLOCK_TYPE_ELUSB3B:
      logger.loggertype=Logger.LTYP_VOLT;
      logger.calibration_tA=config.calibration_Mvalue*config.scaling_factor; //(float)0.0004822;
      logger.calibration_tB=config.calibration_Cvalue*config.scaling_factor; // -35*logger.data.calibration_Avalue;
      config.setdefunit("Volts");
      config.setdefiunit("Volts");
      if(config.temp_unit==Logger.UNIT_C) config.setunit("Volts");
      else config.setunit("Custom");
      break;
    case BLOCK_TYPE_ELUSB4A:
    case BLOCK_TYPE_ELUSB4B:
      logger.loggertype=Logger.LTYP_CURR;
      logger.calibration_tA=config.calibration_Mvalue*config.scaling_factor;
      logger.calibration_tB=config.calibration_Cvalue*config.scaling_factor;
      config.setdefunit("Amps");
      config.setdefiunit("Amps");
      if(config.temp_unit==Logger.UNIT_C) config.setunit("Amps");
      else config.setunit("Custom");
      break;
    case BLOCK_TYPE_ELUSBLITE:
      logger.loggertype=Logger.LTYP_TEMP;
      logger.packet_size=512;
      logger.memory_size=0x1400;
      logger.calibration_tA=1; //TODO:
      logger.calibration_tB=0;
      ret=Error.WARN_NOTTESTED;
      break;
    case BLOCK_TYPE_ELUSBCO:
    case BLOCK_TYPE_ELUSBCO300:
      logger.loggertype=Logger.LTYP_GAS;
      logger.calibration_tA=config.calibration_Mvalue*config.scaling_factor;
      logger.calibration_tB=config.calibration_Cvalue*config.scaling_factor;
      config.setdefunit("ppmCO");
      config.setdefiunit("ppmCO");
      if(config.temp_unit==Logger.UNIT_C) config.setunit("ppmCO");
      else config.setunit("Custom");
      ret=Error.WARN_NOTTESTED;
      break;
    case BLOCK_TYPE_ELUSBTC:
      logger.loggertype=Logger.LTYP_TEMP;
      logger.calibration_tA=config.calibration_Mvalue*config.scaling_factor;
      logger.calibration_tB=config.calibration_Cvalue*config.scaling_factor;
      ret=Error.WARN_NOTTESTED;
      break;
    default:
      logger.loggertype=Logger.LTYP_TEMP;
      config.setdefunit("unit");
      config.setdefiunit("unit");
      ret=Error.WARN_NOTSUP;
    }

    if(cbuf.length>=128) {
    	    for(i=0;i<12;i++) config.unit[i]=(char)(cbuf[64+i]&0xff);
    	    for(i=0;i<8;i++) config.cal1[i]=(char)(cbuf[76+i]&0xff);
    	    for(i=0;i<8;i++) config.cal2[i]=(char)(cbuf[84+i]&0xff);
    	    for(i=0;i<8;i++) config.cal3[i]=(char)(cbuf[92+i]&0xff);
    	    for(i=0;i<8;i++) config.cal4[i]=(char)(cbuf[100+i]&0xff);
    	    for(i=0;i<8;i++) config.halmtxt[i]=(char)(cbuf[112+i]&0xff);
    	    for(i=0;i<8;i++) config.lalmtxt[i]=(char)(cbuf[120+i]&0xff);
    }


    if(cbuf.length>=256) {
    	    for(i=0;i<14;i++) config.defrange[i]=(char)(cbuf[128+i]&0xff);
    	    for(i=0;i<12;i++) config.defiunit[i]=(char)(cbuf[142+i]&0xff);
    	    for(i=0;i<12;i++) config.defunit[i]=(char)(cbuf[154+i]&0xff);
    	    for(i=0;i<8;i++) config.defcal1[i]=(char)(cbuf[166+i]&0xff);
    	    for(i=0;i<8;i++) config.defcal2[i]=(char)(cbuf[174+i]&0xff);
    	    for(i=0;i<8;i++) config.defcal3[i]=(char)(cbuf[182+i]&0xff);
    	    for(i=0;i<8;i++) config.defcal4[i]=(char)(cbuf[190+i]&0xff);
    	    for(i=0;i<8;i++) config.defhalmtxt[i]=(char)(cbuf[198+i]&0xff);
    	    for(i=0;i<8;i++) config.deflalmtxt[i]=(char)(cbuf[206+i]&0xff);
    	    config.battery_alarm=buffer.getShort(214);
    }

    /*Das ist auskommentiert, da es reicht, den logger kurz vor der Neukonfiguration zu stoppen
     * */
    //  if((flag_bits &LOGGING_STATE)==LOGGING_STATE && false) {
    //    	Log.d(TAG,"Stop Logger...");
    //    	/*Logger stoppen.*/
    //    	buffer.putLong(24,0);
    //    	    flag_bits=(short)(((int)flag_bits)&(~LOGGING_STATE));
    //    	    buffer.putShort(32,(short)flag_bits);
    //    	    Log.d(TAG,"flag_bits="+flag_bits);
    //    	writeconfigblock_elv(buffer.array());
    //  }
    logger.do_repair=false;
    logger.isreadconfig=true;
    return ret;
  }
  
  /*Read one datapacket Lascar Version*/

  private static int getdatapacket(Logger logger) {
    Log.d(TAG,"getdatapacket: size="+logger.packet_size);
    final LoggerData data=logger.data;
    byte buf[]=new byte[logger.packet_size];
    int ret=logger.receive(buf);
    if(ret==0) {
      Log.d(TAG,"retry...");
      ret=logger.receive(buf);
    }
    Log.d(TAG,"ret="+ret+" anzdata="+data.anzdata);
    if(ret>0) {
      int i;
      if(logger.loggertype==Logger.LTYP_TH) {
    	for(i=0;i<ret/2;i++) data.add((short) (buf[i*2]&0xff),(short) (buf[i*2+1]&0xff));
      } else if(logger.loggertype==Logger.LTYP_TEMP) {
    	for(i=0;i<ret;i++)   data.add((short) (buf[i]&0xff));
      } else if(logger.loggertype==Logger.LTYP_VOLT) {
    	for(i=0;i<ret/2;i++) data.add((short) (buf[i*2+1]&0xff | (buf[i*2]&0xff)<<8));
      } else if(logger.loggertype==Logger.LTYP_CURR) {
    	for(i=0;i<ret;i++)   data.add((short) (buf[i]&0xff));
      } else  {
    	for(i=0;i<ret/2;i++) data.add((short) (buf[i*2+1]&0xff | (buf[i*2]&0xff)<<8));
      }
      Log.d(TAG,"Got Datapacket ("+ret+" bytes): OK. "+data.anzdata);
      return Error.OK;
    } else return Error.ERR;  /*Something is wrong: timeout*/
  }



  public static int get_data(Logger logger) {
    int i,q=0;
    logger.unlock_device();
    logger.sendCommand((byte)TCMD_GETDATA, (byte)0xff, (byte)0xff);
    int msysstatus=logger.readresponse((byte)TCMD_GETDATA, (byte)0xff, (byte)0xff);
    if(msysstatus<0) return(-2);
    final LoggerData data=logger.data;
    final LoggerConfig config=logger.config;
    logger.updateStatus(msysstatus);
    for(i=0;i<msysstatus/logger.packet_size;i++) {
      Log.d(TAG,"Get packet "+i+"/"+msysstatus/logger.packet_size+"anzdata="+data.anzdata);
      getdatapacket(logger);
      // Update the progress bar
      logger.updateProgress(100*i*logger.packet_size/msysstatus);
    }
    logger.lock_device();
    
    /*Nun Qualitaetscheck der Daten machen.*/
    
    if(logger.loggertype==Logger.LTYP_TH) {
      for(i=0;i<config.num_data_rec;i++) {
    	if(data.get_temp(i)>300 || data.get_temp(i)<-100 || 
    	  data.get_rh(i)>104 || data.get_rh(i)<-1) q++;
      }
    } else if(logger.loggertype==Logger.LTYP_TEMP) {
      for(i=0;i<config.num_data_rec;i++) {
    	if(data.get_temp(i)>300 || data.get_temp(i)<-100) q++;  		
      } 	      
    } else q=0;
    logger.data.quality=q;
    Log.d(TAG,"read data Quality: "+q);
    return(q);    	
  }
}

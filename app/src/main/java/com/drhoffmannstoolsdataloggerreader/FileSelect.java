package com.drhoffmannstoolsdataloggerreader;

/* FileSelect.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * ============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 


import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnShowListener;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class FileSelect {
	public String[] mFileList;
	public boolean[] mcheckitems;
	private static final String TAG = FileSelect.class.getSimpleName(); 
	private String mChosenFile;
	private static String[] blacklist;
	private static int defaultfileselect=1;
	private AlertDialog dialog;

	private static final String[] mitem={
		"delete","send via email","Cancel"
	};

	public FileSelect(int n) {
		clear();
		defaultfileselect=n;
		
	}
	public FileSelect() {
		clear();
	}
	private void clear() {
		dialog=null;
	}

	public void setNumFiles(int n) {
		defaultfileselect=n;
	}
	private String operateonfile;
	private Context context;
	private AlertDialog filemenudialog(String filename) {
		operateonfile=filename;

		AlertDialog.Builder builder = new Builder(context);
		builder.setTitle(filename);
		//	builder.setIcon(R.drawable.bombe);
		builder.setItems(mitem, new DialogInterface.OnClickListener(){
			@Override 
			public void onClick(DialogInterface ldialog, int which){
				Log.d(TAG,"Item #"+which+"was clicked for "+operateonfile);
				if(mitem[which].equalsIgnoreCase("delete")) { /*delete*/
					deletefiledialog(context,operateonfile).show();
					// if(dialog!=null) dialog.dismiss();
		
				} if(mitem[which].equalsIgnoreCase("send via email")) {
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
						String recipient=prefs.getString("recipient", "dr.markus.hoffmann@gmx.de");
						String subject="Data File: "+operateonfile;
						Tools.sendEmail(context,recipient,subject,"Find the file with logger data in the attachment.", operateonfile);
				}

			}});
		
		return builder.create();
	}
	public AlertDialog deletefiledialog(Context c,String filename) {
		AlertDialog.Builder builder = new Builder(c);
		operateonfile=filename;
		
		File f=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/"+operateonfile);
		builder.setTitle(c.getResources().getString(R.string.message_really_delete));
		builder.setMessage(String.format(c.getResources().getString(R.string.message_delete), operateonfile,f.length()));
		builder.setPositiveButton(c.getResources().getString(R.string.word_procceed), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface ldialog, int which) {
				File f=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/"+operateonfile);
				if(f.isFile()) f.delete();
				if(dialog!=null) dialog.dismiss();
			} }); 
		builder.setNeutralButton(c.getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				;
			} }); 
		AlertDialog ddialog = builder.create();
		ddialog.setCanceledOnTouchOutside(false);
		return(ddialog);
	}
	public void loadFileList() {
		File dirdata=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		if(dirdata.exists()) {
			FilenameFilter filter = new FilenameFilter(){
				public boolean accept(File dir, String filename){
					File sel = new File(dir, filename);

					return (filename.startsWith("VDL") && (
							filename.endsWith(".txt") || 
							filename.endsWith(".dat") || 
							filename.endsWith(".xml") ||
							filename.endsWith(".csv"))) || 
							sel.isDirectory();
				}
			};
			/*Nach datum Sortieren*/
			File[] files = dirdata.listFiles(filter);
			Arrays.sort(files, new Comparator<File>(){
				public int compare(File f1, File f2)
				{
					return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
				} });

			mFileList=new String[files.length];
			mcheckitems=new boolean[files.length];
			int selcount=0;
			for(int i=0;i<mFileList.length;i++) {
				mFileList[i]=files[i].getName();
				mcheckitems[i]=(!mFileList[i].endsWith(".xml")) && files[i].isFile();
				if(matchblacklist(mFileList[i])) mcheckitems[i]=false;
				if(mcheckitems[i]) selcount++;
				if(selcount>defaultfileselect) mcheckitems[i]=false;
			}    	     
		} else {
			Log.d(TAG,"Path not found!");
			mFileList= new String[0];
			mcheckitems=new boolean[0];
		}
	}
	private boolean matchblacklist(String a) {
		int i=0;
		while(blacklist!=null && i<blacklist.length) {
			if(a.equals(blacklist[i])) return true;
			i++;
		}
		return false;
	}
	public Dialog fileselector(Context c) {
		AlertDialog.Builder builder = new Builder(c);
		context=c;
		builder.setTitle(c.getResources().getString(R.string.word_fileselect2));
		if(mFileList == null){
			return builder.create();
		}
		builder.setMultiChoiceItems(mFileList, mcheckitems, new DialogInterface.OnMultiChoiceClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which,boolean isChecked){
				mChosenFile = mFileList[which];
				//you can do stuff with the file here too
				Log.d(TAG,"File chosen: "+mChosenFile);
			}
		});
		builder.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface arg0) {
				int i=0;
				int count=0;
				while(mFileList!=null && i<mFileList.length) {
					if(mcheckitems[i]==false) count++;
					i++;
				}
				i=0;
				blacklist=new String[count];
				count=0;
				while(mFileList!=null && i<mFileList.length) {
					if(mcheckitems[i]==false) {
						blacklist[count++]=mFileList[i];
						Log.d(TAG,"exclude:"+mFileList[i]);
					}
					i++;
				}
			}
		});
		dialog=builder.create();

		dialog.setOnShowListener(new OnShowListener() {       
			@Override
			public void onShow(DialogInterface ldialog) {
				ListView lv = dialog.getListView(); //this is a ListView with your "buds" in it
				lv.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent, View view, int which, long id) {
						Log.d(TAG,"List Item #"+which+" was long clicked");
						filemenudialog(mFileList[which]).show();
						//dialog.dismiss();
						return true;
					}

				});     
			}
		}); 
		return dialog;
	}
}

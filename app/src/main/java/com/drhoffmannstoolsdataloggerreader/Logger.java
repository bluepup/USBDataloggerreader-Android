package com.drhoffmannstoolsdataloggerreader;

/* Logger.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * =============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.io.File;
import java.io.FilenameFilter;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Locale;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import android.os.Build;
import android.util.Log;


/* Diese Klasse beschreibt alles, was zu einem Logger bzw. 
   dessen USB-Verbindung gehört */

public class Logger {
  private static final String TAG = "LOGGER";
  private static final int DEBUG=0;   /* Debug level, the higher the more verbose....*/
  public USBDataloggerreaderActivity mActivity=null;

  /* Properties for raw USB access */

  public UsbManager UsbManager;
  public UsbDevice Device;
  private UsbDeviceConnection Connection=null;
  private UsbEndpoint EndpointOut;
  private UsbEndpoint EndpointIn;
  public int Vid=-1;   /*Vendor ID*/
  public int Pid=-1;   /*Product ID*/

  /* Properties for media/file access */

  public String Path=null;   /* Path for Media */
  public String Configfile=null;
  public String ProfileId=null;
  public String FirmwareVersion=null;
  public String Configfilecontent=null;

  /* Device properties */

  public String Serial_id="00_0000000000";
  public String Manufacturer="UKN";
  public String Product="unknown logger";
  public int packet_size=64;
  public int memory_size=0xfe00;

  /*Logger spezifisch*/

  public int loggertype=-1;
  public int protocol=-1;

  /* Action configuration */

  public boolean do_repair=false;
  public boolean do_simulate=false;
  public boolean do_calibrate=false;
  public boolean be_quiet=false;
  public int simulate_type;
  public boolean fakeconfigdir=false;

  /* Verschiedene Protokoll-Typen*/

  public final static int PROTO_VOLTCRAFT=0;
  public final static int PROTO_VOLTCRAFT_NEW=1;
  public final static int PROTO_VOLTCRAFT_WEATHER=2;
  public final static int PROTO_ELV=4;  /* Lascar logger*/
  public final static int PROTO_HID=5;  /* Freetec logger*/
  public final static int PROTO_TFD=6;  /* ELV TFD*/
  public final static int PROTO_CP210X=7;  /* e.g. Voltcraft DL 141*/
  public final static int PROTO_FILE=8;  /* e.g. Voltcraft DL-2?0-T* */
  public final static int PROTO_FREETECWHITE=9;  /* new FreeTec Logger */

  /*Verschiedene Logger-Typen*/

  public final static int LTYP_TEMP=100;
  public final static int LTYP_TH=120;
  public final static int LTYP_THP=180;
  public final static int LTYP_VOLT=200;
  public final static int LTYP_CURR=300;
  public final static int LTYP_GAS=400;
  public final static int LTYP_SOUND=500;

  /* Verschiedene Capabilities */

  public final static int CAPA_NONE=0;
  public final static int CAPA_STARTBUTTON=1;
  public final static int CAPA_STOPBUTTON=2;
  public final static int CAPA_STARTTIME=4;
  public final static int CAPA_STOPTIME=8;
  public final static int CAPA_LED=0x10;
  public final static int CAPA_LCD=0x20;
  public final static int CAPA_LIMITS=0x40;
  public final static int CAPA_FAHRENHEIT=0x80;
  public final static int CAPA_ROLLOVER=0x100;
  public final static int CAPA_NUMDATACONF=0x200;
  public final static int CAPA_DELAY=0x400;
  public final static int CAPA_PAUSE=0x800;
  public final static int CAPA_LCD30=0x1000;
  public final static int CAPA_ACTUALVAL=0x2000;
  public final static int CAPA_MINMAXVAL=0x4000;

  /* Start conditions */

  public final static int START_INSTANT=0;
  public final static int START_BUTTON=1;
  public final static int START_TIME=2;

  /* Stop conditions */

  public final static int STOP_NONE=0;
  public final static int STOP_BUTTON=1;
  public final static int STOP_TIME=2;
  public final static int STOP_MEMFULL=4;
  public final static int STOP_PDF=8;

  /* Alarm conditions */

  public final static int ALARM_OFF=0;
  public final static int ALARM_ON=1;
  public final static int ALARM_T=2;  /* on T boundaries */
  public final static int ALARM_H=4;  /* on H boundaries */
  public final static int ALARM_P=8;  /* on P boundaries */

  /* Display conditions */

  public final static int DISPLAY_OFF=0;
  public final static int DISPLAY_ON=1;
  public final static int DISPLAY_30SEKON=3;

  /* Time Format  */

  public final static int TIMEFORMAT_AUTO=0;
  public final static int TIMEFORMAT_12H=1;
  public final static int TIMEFORMAT_24H=2;

  /* Date Format  */

  public final static int DATEFORMAT_AUTO=0;
  public final static int DATEFORMAT_YYYYMMDD=1;
  public final static int DATEFORMAT_DDMMYYYY=2;
  public final static int DATEFORMAT_MMDDYYYY=3;

  /* Units  */

  public final static int UNIT_C=0;
  public final static int UNIT_F=1;
  public final static int UNIT_LUX=0;
  public final static int UNIT_FG=1;
  public final static int UNIT_HPA=0;
  public final static int UNIT_MMHG=1;
  public final static int UNIT_KPA=2;

  /* Languages */

  public final static int LANGUAGE_DEFAULT=0;
  public final static int LANGUAGE_ENGLISH=2;
  public final static int LANGUAGE_GERMAN=1;
  public final static int LANGUAGE_FRENSH=3;
  public final static int LANGUAGE_ITALIAN=4;
  public final static int LANGUAGE_DUTCH=5;

  /* PDF Name Parts */

  public final static int PDFNAME_NONE=0;
  public final static int PDFNAME_MODEL=1;
  public final static int PDFNAME_SERIAL=2;
  public final static int PDFNAME_DATE=3;
  public final static int PDFNAME_TIME=4;
  public final static int PDFNAME_LOCATION=5;
  public final static int PDFNAME_OWNER=6;

  /* Status properties */

  public boolean isconnected=false;
  public boolean isreadconfig=false;
  public boolean isreaddata=false;
  public boolean issaved=false;
  
  
  public boolean treeopened=false;


  public final LoggerData     data=new LoggerData();
  public final LoggerConfig config=new LoggerConfig();
  public final Simulator simulator=new Simulator(this);

  /* Current readings and Min/Max Values */

  public float actual_temp=0;
  public float actual_humi=0;
  public float actual_pres=0;
  public float minval_temp=0;
  public float minval_humi=0;
  public float minval_pres=0;
  public float maxval_temp=0;
  public float maxval_humi=0;
  public float maxval_pres=0;
  


  /* Calibrationsdaten dieses Loggers 
     (darf von denen in den LoggerData differieren)
   */

  public float calibration_tA=(float)0.1; /* Temperature */
  public float calibration_tB=(float)0.0; 
  public float calibration_rA=(float)0.1; /* Humidity */
  public float calibration_rB=(float)0.0; 
  public float calibration_pA=(float)0.1; /* Pressure */
  public float calibration_pB=(float)1013.25; 

  public final static int WAIT_READ=500;
  public final static int WAIT_WRITE=100;

  /*Zum Debuggen: Kommunikationsprotokoll*/

  private String comproto="";

  public void set_calibration(float a,float b, float a2,float b2,float a3,float b3) {
    calibration_tA=a;
    calibration_tB=b; 
    calibration_rA=a2; 
    calibration_rB=b2;
    calibration_pA=a3; /* Pressure */
    calibration_pB=b3;
  }
  public void set_calibration(float a,float b, float a2,float b2) {
    calibration_tA=a;
    calibration_tB=b; 
    calibration_rA=a2; 
    calibration_rB=b2;
  }
  public void set_calibration(float a,float b) {
    calibration_tA=a;
    calibration_tB=b; 
  }

  /* Hilfsfunktionen */

  public static String bytearray2string(byte[] a) {
    String b="("+a.length+"){";
    for(int i=0;i<a.length;i++) {
      b=b+String.format("0x%02x",a[i]);
      if(i+1<a.length) b=b+",";
    }
    return b+"}";
  }

  public String toString() {
    String mes="# Device Information:\n"+
	       "vendor_id="+Vid+" ("+Manufacturer+")"+"\n"+
	       "product_id="+Pid+" ("+Product+")"+"\n"+
	       "serial_id="+Serial_id+"\n"+
	       "protocol="+protocol+"\n"+
	       "loggertype="+loggertype+"\n"+
	       "memory_size="+memory_size+"\n"+
	       "packet_size="+packet_size+"\n"+
	       "version="+config.getversion()+"\n"+
	       "FirmwareVersion="+FirmwareVersion+"\n"+
	       "Path="+Path+"\n"+
	       "Configfile="+Configfile+"\n"+
	       "ProfileId="+ProfileId+"\n"+
	       "cbuf=";
    if(config.configbuf==null) mes=mes+"null";
    else mes=mes+bytearray2string(config.configbuf);
    mes=mes+";\n"+
	    "data_saved="+data.issaved+"\n"+
	    "data_anzdata="+data.anzdata+"\n"+
	    "data_quality="+data.quality+"\n"+
	    "";
    if(comproto.length()>0) mes=mes+"\nCommunication protocol:\n"+comproto;
    return mes;
  }

  /*  Collect Log-Messages, so that they can be send to the author for debugging
   */

  public void log2comproto(String a,int prio) {
    if(DEBUG>=prio) comproto=comproto+a+"\n";
    // if(DEBUG>prio) Log.d(TAG,a);
  }
	
  /* Check if there is a (raw) USB device connected */

  public boolean checkforUSBdevices() {
    boolean flag=false;
    log2comproto("Check for USB devices...",0);
    // check for existing devices
    for(UsbDevice device :  UsbManager.getDeviceList().values()) {
      flag|=filterdevice(device);
    }

    /* Wenn kein echter Logger gefunden wurdem dann 
       nimm ggf. den Simulator... 
     */
    if(!flag && do_simulate) {
      /*Wenn noch eine alte Verbindung besteht, diese schliessen.*/
      if(Connection!=null) close();
      // setze simulation auf.
      simulator.init(simulate_type);
    }
    return(flag||do_simulate);
  }

  /* Check if there has been a valid logger plugged on the USB
     Storage device ... 
     The logger.Path is assumed to have been set already.
   */
  public boolean check_media_logger() {
    boolean flag=false;
    log2comproto("Check for USB-Storage logger on "+Path,0);
    if(Path!=null) {
      String dirname=Path.substring(8); /* Omit the "file://" part*/
      File dir= new File(dirname);
      if(dir.exists()) {
  	FilenameFilter filter=new FilenameFilter() {
  	  public boolean accept(File dir, String filename) {
  	    File sel = new File(dir, filename);
  	    return(!sel.isDirectory() &&
	      (filename.endsWith(".cl1") || filename.endsWith(".CL1")));
  	  }
  	};

  	File[] files=dir.listFiles(filter);
  	if(files!=null) {
  	  for(int i=0;i<files.length;i++) {
	    log2comproto("found: "+files[i].getName(),0);
	    /* Extract the logger type from the file name and store the
	       full config filename...*/
	    Manufacturer="Voltcraft";
	    Configfile=files[i].getName();
	    String[] parts = Configfile.split("-");
	    Product=parts[0]+"-"+parts[1];
	    protocol=PROTO_FILE;
	    if(parts[1].equalsIgnoreCase("220THP")) loggertype=LTYP_THP;
	    else if(parts[1].equalsIgnoreCase("210TH")) loggertype=LTYP_TH;
	    else if(parts[1].equalsIgnoreCase("200T")) loggertype=LTYP_TEMP;
  	    else if(parts[1].equalsIgnoreCase("230L")) loggertype=LTYP_TEMP; /* Light detector */
  	    else if(parts[1].equalsIgnoreCase("240K")) loggertype=LTYP_TEMP; /* External Sensor */
  	    else loggertype=-1;
	    memory_size=0x10000;
	    packet_size=512;
	    isconnected=true;
	    flag=true;
	    break;
  	  }
  	} else log2comproto("... not accessible... ",0);
      }
    }
    return(flag);
  }
  
  /* Anhand des Filenamens wird auf den loggertyp geschlossen.*/
  
  public boolean check_media_logger_content() {
    boolean flag=false;
    log2comproto("Check USB-Storage logger on "+Path,0);
    if(Path!=null) {
      String[] separated = Path.split(":");
      String filename=separated[1];
      if(filename!=null) {
        /* INFO: .cl1xxx just for debugging*/
        if(filename.endsWith(".cl1") || filename.endsWith(".CL1") || filename.endsWith(".cl1xxx")) {
          /* Extract the logger type from the file name and store the
	     full config filename...*/
	  Configfile=filename;
	  String[] parts = filename.split("-");
	  Product=parts[0]+"-"+parts[1];
	  protocol=PROTO_FILE;
	  if(parts[1].equalsIgnoreCase("220THP")) loggertype=LTYP_THP;
	  else if(parts[1].equalsIgnoreCase("210TH")) loggertype=LTYP_TH;
	  else if(parts[1].equalsIgnoreCase("200T")) loggertype=LTYP_TEMP;
  	  else if(parts[1].equalsIgnoreCase("230L")) loggertype=LTYP_TEMP; /* Light detector */
  	  else if(parts[1].equalsIgnoreCase("240K")) loggertype=LTYP_TEMP; /* External Sensor */
  	  else loggertype=-1;
          Manufacturer="Voltcraft";
	  memory_size=0x10000;
	  packet_size=512;
	  isconnected=true;
	  flag=true;
	  log2comproto("This seems to be a "+Product,0);
        }	
      }
    }
    return(flag);
  }
  /* Search for USB-Devices attached using the file system.
     Starting at "/storage" and processing all mount points there.
     */
  public void search_storage_location() {
    log2comproto("Search media...",0);
    File dir= new File("/storage");
    if(dir.exists()) {
      FilenameFilter filter=new FilenameFilter() {
  	public boolean accept(File dir, String filename) {
  	  File sel = new File(dir, filename);
  	  return(sel.isDirectory());
  	}
      };
      File[] files=dir.listFiles(filter);
      boolean flag=false;
      for(int i=0;i<files.length && !flag;i++) {
  	  Path="file:///storage/"+files[i].getName();
  	  flag=check_media_logger();
      }
      if(!flag) Path=null;
    }
  }

  /* Returns true if a known Logger was found.
     Sets the device and protocol type.
   */
	
  public boolean filterdevice(UsbDevice device) {
    log2comproto("Checking device: c:"+device.getDeviceClass()+
		 ", v:"+device.getVendorId()+", p:"+device.getProductId(),0);
    if(device.getVendorId()==4292 && device.getProductId()==2) {
      /*Die ELV Reihe*/
      protocol=PROTO_ELV;  /*ELV-USB*/
      setDevice(device);
      return true;
    } else if(device.getVendorId()==4292 && device.getProductId()==3) {
      protocol=PROTO_VOLTCRAFT;
      setDevice(device);
      return true;
    } else if(device.getVendorId()==4292 && device.getProductId()==60000) {
      protocol=PROTO_CP210X;
      setDevice(device);
      return true;
    } else if(device.getVendorId()==4292 && device.getProductId()==60001) {
      protocol=PROTO_VOLTCRAFT_NEW;
      setDevice(device);
      return true;
    } else if(device.getVendorId()==6465 && (device.getProductId()==2010 || device.getProductId()==32801)) {
      protocol=PROTO_HID;
      setDevice(device);
      return true;
    } else if(device.getVendorId()==4292 && device.getProductId()==33896 ) { /* FreeTec white (new) */
      protocol=PROTO_FREETECWHITE;
      setDevice(device);
      return true;
    } else if(device.getVendorId()==1027 && device.getProductId()==23389) { /* TFD (not implemented yet) */
      protocol=PROTO_TFD;
      setDevice(device);
      return true;
    }
    return false;
  }

  /* Tries to connect to a USB device .*/

  public void setDevice(UsbDevice device) {
    /*Wenn noch eine alte Verbindung besteht, diese schliessen.*/
    if(Connection!=null) close();
    if(do_simulate) { simulator.init(simulate_type); return; }
    UsbEndpoint epOut = null;
    UsbEndpoint epIn = null;
    Vid=-1;
    Pid=-1;
    if(device==null) {
      mActivity.displaystatus(" - ",5);
      mActivity.displaymessage("ERROR: Device not connected.",5);
      return;
    }
    log2comproto("setDevice " + device+", protocol="+protocol,1);
    if(device.getInterfaceCount() != 1) {
      log2comproto("ERROR: no interfaces. ("+device.getInterfaceCount()+")",0);
      mActivity.displaymessage("ERROR: could not find interface",5);
      return;
    }
    UsbInterface intf = device.getInterface(0);
    log2comproto("found "+intf.getEndpointCount()+" endpoints.",1);
    if(protocol!=PROTO_HID) {
      // device should have two endpoints
      if(intf.getEndpointCount() != 2) {
        log2comproto("ERROR: could not find (2) endpoints.",0);
	mActivity.displaymessage("ERROR: could not find (2) endpoints.",5);
	return;
      }
      // look for our bulk endpoints
      for(int i=0; i<intf.getEndpointCount(); i++) {
	log2comproto("Endpoint: "+intf.toString(),2);
	UsbEndpoint ep=intf.getEndpoint(i);
	if(ep.getType()==UsbConstants.USB_ENDPOINT_XFER_BULK) {
	  if (ep.getDirection()==UsbConstants.USB_DIR_OUT) epOut=ep;
	  else epIn=ep;
	} else if(protocol==PROTO_FREETECWHITE && ep.getType()==UsbConstants.USB_ENDPOINT_XFER_INT) {
	  if (ep.getDirection()==UsbConstants.USB_DIR_OUT) epOut=ep;
	  else epIn=ep;
	} else {
	  log2comproto("Endpoint of unknown type: "+ep.getType(),2);
	}
      }
      if(protocol==PROTO_FREETECWHITE) {
      /* Only a single INPUT endpoint is required*/
        if(epIn == null) {
	  log2comproto("ERROR: endpoint not found.",0);
	  mActivity.displaymessage("ERROR: endpoint not found.",5);
	  return;
        } else log2comproto("i Endpoint found.",1);
	if(epOut == null) {
	  log2comproto("Output endpoint not found.",1);
        } else log2comproto("o Endpoint found.",1);
      
      } else {
        if(epOut == null || epIn == null) {
	  log2comproto("ERROR: not all (2) endpoints found.",0);
	  mActivity.displaymessage("ERROR: not all (2) endpoints found.",5);
	  return;
        } else log2comproto("2 Endpoints found.",1);
      }
    } else {
      /* device should have only one (interrupt capable) endpoint
         Output endpoint is not required. */
      if(intf.getEndpointCount() != 1) {
	log2comproto("ERROR: could not find endpoint.",0);
	mActivity.displaymessage("ERROR: could not find endpoint.",5);
	return;
      }
      // look for our interrupt endpoint
      for(int i=0; i<intf.getEndpointCount(); i++) {
	log2comproto("Endpoint: "+intf.toString(),2);
	UsbEndpoint ep = intf.getEndpoint(i);
	if(ep.getType() == UsbConstants.USB_ENDPOINT_XFER_INT) {
	  if(ep.getDirection()==UsbConstants.USB_DIR_IN) {
	    epIn = ep;
	  }
	}
      }
      if(epIn==null) {
        log2comproto("ERROR: single endpoint not found.",0);
	mActivity.displaymessage("ERROR: single endpoint not found.",5);
	return;
      } else log2comproto("Single endpoint found.",1);
    }

    EndpointOut = epOut;
    EndpointIn = epIn; 
    Device = device;
    Vid=device.getVendorId();
    Pid=device.getProductId();
    if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP) { /* 21 */
      Serial_id=device.getSerialNumber();
      Manufacturer=device.getManufacturerName();
      Product=device.getProductName();
    } else {  /* Old Android Versions */
      Serial_id="00_0000000000";
      Manufacturer=null;
      Product=null;
      /* Get Vendor name, Product name and Serial-Nr of Logger*/
      String ana=USBTools.getUsbInfoViaShell();
      String[] sep=ana.split("\n");
      int i;
      String a=String.format("%04x", Vid);
      String b=String.format("%04x", Pid);

      for(i=0;i<sep.length; i++) {
              String[] sep2=sep[i].split(",");
              if(sep2.length>2) {
        	      String c=sep2[0].replace("\"","");
        	      String d=sep2[1].replace("\"","");
        	      if(a.equalsIgnoreCase(c) && b.equalsIgnoreCase(d)) {
        		      if(sep2.length>2) {
        			      Serial_id=sep2[2].replace("\"","");
        		      } 
        		      if(sep2.length>3) {
        			      Manufacturer=sep2[3].replace("\"","");
        		      } 
        		      if(sep2.length>4) {
        			      Product=sep2[4].replace("\"","");
        		      } 
        	      }
              }
      }
    }
    if(Manufacturer==null || Manufacturer.equalsIgnoreCase("") || Manufacturer.equalsIgnoreCase(" ")) Manufacturer="UKN";
    if(Product==null || Product.equalsIgnoreCase("")) Product="unknown logger";
    Serial_id=clean_string(Serial_id,"0000");
    

    UsbDeviceConnection connection = UsbManager.openDevice(device);
    if(connection != null && connection.claimInterface(intf, true)) {
      log2comproto("open SUCCESS: "+connection.toString(),1);
      isconnected=true;
      Connection = connection;
      if(Vid==6465 && (Pid==2010 || Pid==32801)) {  /* Freetec black */
	/*TODO: HID specific:*/
	//	Connection.controlTransfer(UsbConstants.USB_DIR_OUT|0x21, 0x9,0x0, 0x1, null, 0, WAIT_WRITE);
	loggertype=LTYP_TH;
	packet_size=8;
	memory_size=0x10000;
	if(Manufacturer.equals("UKN") && Product.equals("unknown logger")) {
	  Manufacturer="FreeTec";
	  Product="NC7004-675 (black)";
	}
      } else if(Vid==4292 && Pid==33896 ){ /* FreeTec new (white) */
	protocol=PROTO_FREETECWHITE;
	packet_size=64;
	memory_size=0x10000;
	loggertype=LTYP_TH;
	if(Manufacturer.equals("UKN") && Product.equals("unknown logger")) {
	  Manufacturer="FreeTec";
	  Product="NC7004-675 V.2 (white)";
	}
      } else if(Vid==4292 && Pid==60000) {  // VDL-141 ...
	setConfigSingle(CP210x.SILABSER_IFC_ENABLE_REQUEST_CODE, CP210x.UART_ENABLE);
	setConfigSingle(CP210x.SILABSER_SET_MHS_REQUEST_CODE, CP210x.MCR_ALL | CP210x.CONTROL_WRITE_DTR | CP210x.CONTROL_WRITE_RTS);
	setConfigSingle(CP210x.SILABSER_SET_BAUDDIV_REQUEST_CODE, CP210x.BAUD_RATE_GEN_FREQ / CP210x.DEFAULT_BAUD_RATE);
	// setParameters(DEFAULT_BAUD_RATE, DEFAULT_DATA_BITS, DEFAULT_STOP_BITS, DEFAULT_PARITY);
	protocol=PROTO_CP210X;
	packet_size=64;
	memory_size=0x10000;
	loggertype=LTYP_TH;
	set_calibration((float)0.1,0,(float)0.1,0);
      } else if(Vid==4292 && Pid==60001) {
	reset();         /* Das scheint nicht noetig zu sein ... */
	lock_device();   /* USB lock device*/
	                 /* USB unlock device (hier weglassen, da spaeter?)*/
	Connection.controlTransfer(0x40, 2, 2, 0, null, 0, WAIT_WRITE);
	/* Hier jetzt unterscheiden, welcher Typ Datenlogger angeschlossen ist.*/
	if(Product.equals("Weather Datalogger")) {
	  protocol=PROTO_VOLTCRAFT_WEATHER;
	  loggertype=LTYP_THP;
	  memory_size=0x10000;
	  if(Manufacturer.equals("UKN")) Manufacturer="Voltcraft";
	} else {
	  protocol=PROTO_VOLTCRAFT_NEW;
	  /* TODO: loggertype= */
	  memory_size=0xfe00;
	}
	packet_size=64;
	set_calibration((float)0.1,0,(float)0.1,0,(float)0.1,(float)1013.25);
      } else if(Vid==4292 && Pid==2) {
	protocol=PROTO_ELV;
	if(Manufacturer.equals("UKN") && Product.equals("unknown logger")) {
	  Manufacturer="USB500";
	  Product="data logger";
	}
	/* TODO */
	/* Hier muss dann eigentlich sofort get_config ausgeführt
	 * werden, sonst inst die Calibration inkonsistent, sollte man gleich ein 
	 * read data machen....*/
	set_calibration((float)1,0,(float)0.5,0);
	config.block_type=Lascar.BLOCK_TYPE_UNKNOWN;  /*Set as unknown*/
      } else {
	/*The old types do not need configuration */
	protocol=PROTO_VOLTCRAFT;
	set_calibration((float)0.1,0,(float)0.1,0);
	packet_size=64;
	memory_size=0xfe00;
	if(Manufacturer.equals("UKN") && Product.equals("unknown logger")) {
	  Manufacturer="generic";
	  Product="data logger";
	}
      }
      mActivity.displaystatus(Manufacturer+" "+Product+" (#"+Serial_id+") connected.",0);
    } else {
      log2comproto("ERROR: Device open failed!",0);
      mActivity.displaymessage("ERROR: Device open failed!",5);
      Connection = null;
    }
  }

  public void close() {
    log2comproto("### close",1);
    if(Connection!=null) {
      Connection.close();
      Connection=null;
    }
    simulator.sim_configbuf=null;
    Configfilecontent=null;
    Path=null;
    isconnected=false;
  }

  public int receive(byte[] cbuf)  {
    if(do_simulate) return simulator.send(cbuf);
    int ret=Error.ERR;
    if(Connection != null) {
      ret=Connection.bulkTransfer(EndpointIn, cbuf, cbuf.length,WAIT_READ);
    } else log2comproto("receive: connection problem",0);
    log2comproto("received: ret="+ret+" "+bytearray2string(cbuf),2);

    /*Mache es etwas langsamer um Übertragungsfehler bei altem Logger
     * zu vermeiden. Es wurden ab dem 17. Block daten schonmal ganz andere
     * seltsame Speicherbereiche empfangen...*/
    if(protocol==PROTO_VOLTCRAFT) {
      try {Thread.sleep(50);} 
      catch (InterruptedException e1) {log2comproto("sleep was interrupted: "+e1.getMessage(),1);}
    }
    return(ret);
  }

  public int receive(byte[] cbuf,int n) {
    if(do_simulate) return simulator.send(cbuf,n);
    int ret=Error.ERR;
    if(Connection != null) {
      ret=Connection.bulkTransfer(EndpointIn, cbuf,n,WAIT_READ);
    } else log2comproto("receive: connection problem",0);
    log2comproto("received: ret="+ret+" "+bytearray2string(cbuf),1);
    return(ret);
  }

  public int receive(byte[] cbuf,int n,int timeout) {
    if(do_simulate) return simulator.send(cbuf,n);
    int ret=Error.ERR;
    if(Connection != null) {
      ret=Connection.bulkTransfer(EndpointIn, cbuf,n,timeout);
    } else log2comproto("receive: connection problem",0);
    log2comproto("received: ret="+ret+" "+bytearray2string(cbuf),2);
    return(ret);
  }

  public int send(byte[] cbuf) {
    log2comproto("send "+bytearray2string(cbuf),2); 
    if(do_simulate) return simulator.receive(cbuf);
    int ret=Error.ERR;
    if(Connection != null) {
    	    ret=Connection.bulkTransfer(EndpointOut, cbuf, cbuf.length,WAIT_WRITE);
    } else log2comproto("send: connection problem",0);
    return(ret);
  }

  private static final int REQTYPE_HOST_TO_DEVICE = 0x41;

  private int setConfigSingle(int request, int value) {
    return Connection.controlTransfer(REQTYPE_HOST_TO_DEVICE, request, value, 
                0, null, 0, WAIT_WRITE);
  }


  public void unlock_device(){
    log2comproto("unlock",1);
    if(Connection!=null) Connection.controlTransfer(0x40, 2, 2, 0, null, 0, WAIT_WRITE);
  }
  public void lock_device(){
    log2comproto("lock",1);
    if(Connection!=null) Connection.controlTransfer(0x40, 2, 4, 0, null, 0, WAIT_WRITE);
  }
  public void reset(){
    log2comproto("reset",1);
    if(Connection!=null) Connection.controlTransfer(0x40, 0, 0xff, 0xff, null, 0, WAIT_WRITE);
  }


  /*Send a command to the device
   * */

  public void sendCommand(byte a,byte b,byte c) {
    log2comproto("sendCommand " + a+":"+b+":"+c,1);
    byte[] message = new byte[3];
    message[0] = a;
    message[1] = b;
    message[2] = c;
    send(message);
  }

  /*Read a 1 byte Acknoledge message, all protocols*/

  public int readack() {
    byte ack[]=new byte[1];
    ack[0]=0x00;
    int ret=0;
    int trys=0;
    /* Sometimes restarting the logger takes several seconds before it sends the ACK*/
    while((ack[0]&0xff)!=0xff && trys<5) {
      ret=receive(ack);
      log2comproto("Wait for ACK, response: ret="+ret+" ack="+ack[0],1);
      trys++;
    }
    if(ret==1) {
      if((ack[0]&0xff)==0xff) return 1;
      else return 0;
    }
    return Error.ERR;  /*Something is wrong: timeout*/
  }

  /*Read a 3 bytes response or status message*/

  public int readresponse(byte a,byte b,byte c) {
    log2comproto("Readresponse",1);
    int ret;
    int trys=0;
    int size;
    byte ack[]=new byte[3];
    ack[0]=(byte) 0;
    if(protocol==PROTO_VOLTCRAFT_WEATHER) {
      while((ack[0]&0xff)!=0x0f && (ack[0]&0xff)!=0xfe) {
    	ret=receive(ack);
    	log2comproto(" ret="+ret+" ack="+ack[0],1);
    	if(ret==1) {  /* could be an acknoledge */
    	  if((ack[0]&0xff)==0xff) return -3; /*handshake packet 0xFF, after FLASH write */
    	  else return 0;
    	}
	if(trys>10) {
	  log2comproto(" read_abc: Stopping now.",0);
	  return(Error.ERR);
	}
	trys++;
      }
    } else {
      while(ack[0]!=0x02) {
	ret=receive(ack);
	log2comproto(" ret="+ret+" ack="+ack[0],1);
	if((ret==3 && ack[0] != 0x2)) {
	  log2comproto(" read_abc: try = "+trys+" byte = "+ack[0],1);
	  sendCommand(a,b,c);
	} else if(ret==1) {  /* could be an acknoledge */
	  if((ack[0]&0xff)==0xff) return -3; /*handshake packet 0xFF, after FLASH write */
	  else return 0;
	}
	if(trys>10) {
	  log2comproto(" read_abc: Stopping now.",0);
	  return(Error.ERR);
	}
	trys++;
      }
    }
    log2comproto(" ack="+ack[0]+" "+ack[1]+" "+ack[2],1);
    if(protocol==PROTO_VOLTCRAFT_WEATHER && (ack[0]&0xff)==0xf) size = (((ack[1]&0xff) << 8) | (ack[2]&0xff));
    else if(protocol==PROTO_VOLTCRAFT_WEATHER && (ack[0]&0xff)==0xfe && ack[1]==1) size = 0;
    else size = (((ack[2]&0xff) << 8) | (ack[1]&0xff));
    log2comproto(" size="+size,1);
    return(size);
  }

  /* Basic I/O Function: HID protocol:
   */

  public void HIDwrite(byte[] message) {
    if(protocol==PROTO_HID || EndpointOut==null) {
      // Send command via a control request on endpoint zero
      Connection.controlTransfer(0x21, 0x9, 0x200, 0, message, message.length, 0);
    }
    else if(protocol==PROTO_FREETECWHITE) {
      int bufferMaxLength=EndpointOut.getMaxPacketSize();
      ByteBuffer buffer = ByteBuffer.allocate(bufferMaxLength);
    /*
    	res = libusb_interrupt_transfer(dev->device_handle,
			dev->output_endpoint,
			(unsigned char*)data,
			length,
			&actual_length, 1000);*/
      UsbRequest outRequest = new UsbRequest();
      outRequest.initialize(Connection,EndpointOut);
      buffer.put(message);
      boolean retval = outRequest.queue(buffer, message.length);
      /* Wait for completion of this request */
      if(Connection.requestWait()==outRequest) {
        ; /* Everything is OK. */
      } else log2comproto("ERROR: requestWait on send failed.",0);
    }
  }
  public byte [] HIDread(int maxlength) {
    if(maxlength<0) maxlength=64;
    byte [] cbuf;
    ByteBuffer buffer = ByteBuffer.allocate(maxlength);
    if(Connection != null) {
      buffer.put(0,(byte)0x20);  /* Is this necessary ?*/
      UsbRequest request = new UsbRequest();
      request.initialize(Connection,EndpointIn);
      request.queue(buffer, maxlength);
      if(Connection.requestWait()==request) {
        try {Thread.sleep(1);} 
	catch (InterruptedException e) {}
      } else log2comproto("ERROR: requestWait failed.",0);
        // ret=Connection.bulkTransfer(EndpointIn, cbuf,n,WAIT_READ);
    } else log2comproto("HIDread: connection problem",0);
    cbuf=buffer.array();
    log2comproto("read: "+bytearray2string(cbuf),2);
    return(cbuf);
  }
  
  public void sendHIDCommand(int control,int bank, int adr,int data) {
    if(do_simulate) simulator.sendHIDCommand(control,bank,adr,data);
    else {
      synchronized (this) {
        if(Connection!=null) {
          byte[] message = new byte[8];
          message[0] = (byte)control; 
          message[1] = (byte)(bank & 0xff);
          message[2] = (byte)(adr & 0xff);
          message[3] = (byte)0x20;       // Length to read
          message[4] = (byte)control;
          message[5] = (byte)data;
          message[6] = (byte)0;
          message[7] = (byte)0x20;
	  HIDwrite(message);
          log2comproto("SendHIDCommand: "+bytearray2string(message),2);
        }
      }
    }
  }
  public void sendHIDData(byte[] message) {
    if(do_simulate) simulator.sendHIDData(message);
    else {
      synchronized (this) {
  	if(Connection!=null) {
	  HIDwrite(message);
  	  log2comproto("SendHIDData: "+bytearray2string(message),2);
  	}
      }
    }
  }

  public byte [] receiveHID() {
    if(do_simulate) return simulator.receiveHID();
    else {
      byte [] cbuf;
      ByteBuffer buffer = ByteBuffer.allocate(8);
      if(Connection != null) {
        buffer.put(0,(byte)0x20);
        UsbRequest request = new UsbRequest();
        request.initialize(Connection,EndpointIn);
        request.queue(buffer, 8);
        if (Connection.requestWait() == request) {
       	  try {Thread.sleep(1);} 
	  catch (InterruptedException e) {}
        } else log2comproto("requestWait failed on receive",0);
        // ret=Connection.bulkTransfer(EndpointIn, cbuf,n,WAIT_READ);
      } else log2comproto("receive: connection problem",0);
      cbuf=buffer.array();
      log2comproto("received: "+bytearray2string(cbuf),2);
      return(cbuf);
    }
  }

  /*Read configuration from device.
    Return Error.OK on success.
   * */
  public int get_config() {
	  log2comproto("### getconfig",1);
	  switch(protocol) {
		case PROTO_ELV:               return Lascar.get_config(this);
		case PROTO_HID:		      return Freetec.get_config(this);
		case PROTO_FREETECWHITE:      return Freetecwhite.get_config(this);
		case PROTO_CP210X:            return CP210x.get_config(this);
		case PROTO_FILE:              return MediaLogger.get_config(this);
		case PROTO_VOLTCRAFT:
		case PROTO_VOLTCRAFT_NEW:
		case PROTO_VOLTCRAFT_WEATHER: return Voltcraft.get_config(this);
		case PROTO_TFD:               
		default:                      return(Error.ERR_NOTSUP);
	  }
  }

  public int get_data() {
    log2comproto("### getdata",1);
    data.clear();
    data.set_calibration(calibration_tA,calibration_tB,calibration_rA,calibration_rB);
    int ret;
    if(config.num_data_rec==0) return(Error.OK); 
    switch(protocol) {
    case PROTO_ELV:           ret=Lascar.get_data(this);       break;
    case PROTO_HID:           ret=Freetec.get_data(this);      break;
    case PROTO_FREETECWHITE:  ret=Freetecwhite.get_data(this); break;
    case PROTO_CP210X:        ret=CP210x.get_data(this);       break;
    case PROTO_FILE:	      ret=MediaLogger.get_data(this);  break;
    case PROTO_VOLTCRAFT:
    case PROTO_VOLTCRAFT_NEW:
    case PROTO_VOLTCRAFT_WEATHER:  
      ret=Voltcraft.get_data(this);
      break;
    case PROTO_TFD:
    default: 
      ret=Error.ERR_NOTSUP;
    }
    log2comproto("num_data ="+ data.anzdata,1);
    if(protocol!=PROTO_HID) data.anzdata=config.num_data_rec;  /*Hm....*/
    isreaddata=true;
    return ret;
  }

  public int chk_conf() {
		log2comproto("chkconf",1);
		int ret=config.chk_name();
		if(ret!=Error.OK) return ret;
		switch(protocol) {
		case PROTO_ELV:            return Lascar.chk_conf(config, loggertype, do_repair);
		case PROTO_HID:            return Freetec.chk_conf(config, loggertype);
		case PROTO_FREETECWHITE:   return Freetecwhite.chk_conf(config, loggertype);
		case PROTO_CP210X:         return CP210x.chk_conf(config, loggertype,memory_size);
		case PROTO_FILE:           return MediaLogger.chk_conf(config, loggertype,memory_size);
		case PROTO_VOLTCRAFT:  
		case PROTO_VOLTCRAFT_NEW:
		case PROTO_VOLTCRAFT_WEATHER: return Voltcraft.chk_conf(config, loggertype,memory_size);   
		case PROTO_TFD:
		default: return(Error.ERR_NOTSUP);
		}
	}

  public byte[] build_conf() {
    log2comproto("buildconf",1);
    switch(protocol) {
      case PROTO_ELV:          return Lascar.build_conf(this);
      case PROTO_FREETECWHITE: return Freetecwhite.build_conf(this);
      case PROTO_HID:          return Freetec.build_conf(this);
      case PROTO_CP210X:       return CP210x.build_conf(this);
      case PROTO_FILE:         return MediaLogger.build_conf(this);
      case PROTO_VOLTCRAFT:  
      case PROTO_VOLTCRAFT_NEW:
      case PROTO_VOLTCRAFT_WEATHER: return Voltcraft.build_conf(config, loggertype, protocol);
      default: return(new byte[2]);
    }
  }

  public String get_status() {
    log2comproto("getstatus",1);
    switch(protocol) {
    case PROTO_ELV:    return Lascar.get_status(this);
    case PROTO_FREETECWHITE: return Freetecwhite.get_status(this);
    case PROTO_HID:    return Freetec.get_status(this);
    case PROTO_CP210X: return CP210x.get_status(this);
    case PROTO_FILE:   return MediaLogger.get_status(this);
    case PROTO_VOLTCRAFT:  
    case PROTO_VOLTCRAFT_NEW:
    case PROTO_VOLTCRAFT_WEATHER: return Voltcraft.get_status(this);
    default: return("logger unsupported.");
    }
  }

  public int send_config(byte[] confmessagebuf) {
    log2comproto("### sendconfig "+bytearray2string(confmessagebuf),2);
    if(!isconnected) {
      log2comproto("sendconfig: connection problem",0);
      return(Error.ERR);
    
    }
    switch(protocol) {
      case PROTO_ELV:
        Lascar.stop_logger(this);
        // unlock_device();
        return Lascar.write_configblock(confmessagebuf,this);
        // lock_device();
      case PROTO_HID:
        return Freetec.write_configblock(confmessagebuf,this);
      case PROTO_FREETECWHITE: 
        return Freetecwhite.write_configblock(confmessagebuf,this);
      case PROTO_CP210X:
        return CP210x.write_configblock(confmessagebuf,this);
      case PROTO_FILE:   
        return MediaLogger.write_configblock(confmessagebuf,this);
      case PROTO_VOLTCRAFT_WEATHER:
        unlock_device();
        sendCommand((byte)0xe,(byte)(packet_size&0xff),(byte)((packet_size>>8)&0xff));
        send(confmessagebuf);
        if(readack()!=1) return(Error.ERR);
        lock_device();
        return(Error.OK);
      case PROTO_VOLTCRAFT_NEW:
        unlock_device();
        sendCommand((byte)0x1,(byte)(packet_size&0xff),(byte)((packet_size>>8)&0xff));
        send(confmessagebuf);
        if(readack()!=1) return(Error.ERR);
        lock_device();
        return(Error.OK);
      case PROTO_VOLTCRAFT:
        sendCommand((byte)0x1,(byte)(packet_size&0xff),(byte)((packet_size>>8)&0xff));
        send(confmessagebuf);
        if(readack()!=1) return(Error.ERR);
        return(Error.OK);
      default: 
        return(Error.ERR_NOTSUP);
    }
  }

  public String getStatistics() {	
    String infostring="["+String.format(Locale.US,"%04d-%02d-%02d %02d:%02d:%02d",config.time_year,(int)(config.time_mon&0xff),
			(int)(config.time_mday&0xff),(int)(config.time_hour&0xff),(int)(config.time_min&0xff),
			(int)(config.time_sec&0xff))+"] "+config.num_data_rec+" points @ "+config.got_interval+" sec.";

    String loggerinfostring=Manufacturer+"-"+Product+"/"+loggertype+"("+typestring()+")"+" #"+Serial_id+" blocktype="+config.block_type
			+" Session: "+config.getname()+" ConfigFile: "+Configfile;
    String calibrationinfostring="A="+data.calibration_Avalue+" B="+data.calibration_Bvalue+" "+
				"A2="+data.calibration_A2value+" B2="+data.calibration_B2value+" "+
				"M="+config.calibration_Mvalue+" C="+config.calibration_Cvalue;
    String settingsinfo="";
    String statistics="";
    String eventstat="";
    if(loggertype==LTYP_THP) 
	settingsinfo+=String.format(Locale.US,"# Pressure limits: [%.2f:%.2f] hPa<br/>",config.preslimit_low,config.preslimit_high);
    if(loggertype==LTYP_THP || loggertype==LTYP_TH) 
	settingsinfo+=String.format(Locale.US,"# Humidity limits: [%.1f:%.1f] %%<br/>",config.humilimit_low,config.humilimit_high);
    if(loggertype==LTYP_THP || loggertype==LTYP_TH || loggertype==LTYP_TEMP) 
	settingsinfo+=String.format(Locale.US,"# Temperature limits: [%.1f:%.1f] %s<br/>",config.templimit_low,config.templimit_high,
	((config.temp_unit==UNIT_F)?"°F":"°C"));
    if(loggertype==LTYP_VOLT || loggertype==LTYP_CURR)
	settingsinfo+=String.format(Locale.US,"# Limits: [%f:%f] %s<br/>",config.templimit_low,config.templimit_high,config.getunit());

    if(config.start==1) settingsinfo+=String.format(Locale.US,"# Start delay=%d sec<br/>",config.time_delay);
    settingsinfo+="Rawinput="+config.rawinputreading+" flagbits="+config.flag_bits+"<br/>";

    /*Datenstatistik erstellen
     * */
    Calendar cal = Calendar.getInstance();
    cal.set(config.time_year, (int)(config.time_mon&0xff)-1, (int)(config.time_mday&0xff), (int)(config.time_hour&0xff), (int)(config.time_min&0xff), (int)(config.time_sec&0xff));
    long ss=cal.getTimeInMillis()/1000;
    statistics+="";

    int duration=data.anzdata*config.got_interval;
    if(duration>0) statistics+="Time range: ["+ss+":"+(ss+duration)+"] "+Physics.duration_dhms(duration)+"<br/>";

    float tmin=9999,tmax=-9999,t;
    for(int i=0;i<data.anzdata;i++) {
      t=data.get_temp(i);
      tmin=Math.min(tmin,t);
      tmax=Math.max(tmax,t);
    }
    if(loggertype==LTYP_THP || loggertype==LTYP_TH || loggertype==LTYP_TEMP) 
      statistics=statistics+String.format(Locale.US,"Temperature range: [%.1f:%.1f] %s<br/>",
	tmin,tmax,((config.temp_unit==UNIT_F)?"°F":"°C"));
    else if(loggertype==LTYP_VOLT || loggertype==LTYP_CURR) 
      statistics=statistics+String.format(Locale.US,"Range: [%f:%f] %s<br/>",tmin,tmax,config.getunit());;
    if(loggertype==LTYP_THP || loggertype==LTYP_TH) {
      float rhmin=9999,rhmax=-9999,r;
      for(int i=0;i<data.anzdata;i++) {
        r=data.get_rh(i);
        rhmin=Math.min(rhmin,r);
        rhmax=Math.max(rhmax,r);
      }
      statistics=statistics+String.format(Locale.US,"Humidity range:    [%.1f:%.1f] %%<br/>",
        rhmin,rhmax);
      /* Events zusammenfassen   */

      if(loggertype==LTYP_THP || loggertype==LTYP_TH) {
        if(data.eventliste!=null && data.eventliste.size()>0) {
          eventstat+="<p/> Detected "+data.eventliste.size()+" Events:<br/>";
          Lueftungsevent a;
          for(int i=0;i<data.eventliste.size();i++) {
            a=data.eventliste.get(i);
            eventstat+="# "+i+": "+Physics.timestamp2datetime_short(ss+config.got_interval*a.idx)+
            	    " for "+Physics.gerundet(config.got_interval*(a.idx2-a.idx)/60,1)+" minutes.<br/>";
          }
          eventstat+="<p/>";
        }
      }
    }
    if(loggertype==LTYP_THP) {
      float pmin=9999,pmax=-9999;
      for(int i=0;i<data.anzdata;i++) {
        pmin=Math.min(pmin,data.p[i]);
        pmax=Math.max(pmax,data.p[i]);
      } 
      statistics=statistics+String.format(Locale.US,"# Pressure range:    [%.2f:%.2f] hPa<br/>",
        Voltcraft.raw2p((short)pmin),Voltcraft.raw2p((short)pmax));
    }
    if(eventstat.length()>0) statistics+=eventstat;
    return "<h2>Logger:</h2> "+loggerinfostring+
	   "<h2>Calibration:</h2>"+calibrationinfostring+
	   "<h2>Settings:</h2>"+settingsinfo+
	   "<h2>Data Statistics:</h2>"+statistics+"<p/>"+infostring;
  }

  public int get_capabilities() {
    switch(protocol) {
      case PROTO_FILE:         return MediaLogger.capabilities;
      case PROTO_HID:          return(CAPA_ROLLOVER|CAPA_NUMDATACONF);
      case PROTO_FREETECWHITE: return Freetecwhite.capabilities;
      case PROTO_ELV:	       return(CAPA_ROLLOVER|CAPA_LCD|CAPA_DELAY|CAPA_LIMITS);
      case PROTO_CP210X:       return(CAPA_STARTBUTTON|CAPA_ROLLOVER|CAPA_LCD|CAPA_LED|CAPA_NUMDATACONF|CAPA_LIMITS);
      case PROTO_VOLTCRAFT:  
      case PROTO_VOLTCRAFT_NEW:
      case PROTO_VOLTCRAFT_WEATHER: return(CAPA_STARTBUTTON|CAPA_LED|CAPA_NUMDATACONF|CAPA_LIMITS);
      default: return(CAPA_NONE);
    }
  }

  public boolean has_stopbutton()  { return((get_capabilities()&CAPA_STOPBUTTON)!=0); }
  public boolean has_stoptime()    { return((get_capabilities()&CAPA_STOPTIME)!=0); }
  public boolean has_startbutton() { return((get_capabilities()&CAPA_STARTBUTTON)!=0); }
  public boolean has_starttime()   { return((get_capabilities()&CAPA_STARTTIME)!=0); }
  public boolean has_rollover()    { return((get_capabilities()&CAPA_ROLLOVER)!=0); }
  public boolean has_lcd()         { return((get_capabilities()&CAPA_LCD)!=0); }
  public boolean has_led()         { return((get_capabilities()&CAPA_LED)!=0); }
  public boolean has_lcd30()       { return((get_capabilities()&CAPA_LCD30)!=0); }
  public boolean has_numdataconf() { return((get_capabilities()&CAPA_NUMDATACONF)!=0); }
  public boolean has_delay()       { return((get_capabilities()&CAPA_DELAY)!=0); }
  public boolean has_pause()       { return((get_capabilities()&CAPA_PAUSE)!=0); }
  public boolean has_limits()      { return((get_capabilities()&CAPA_LIMITS)!=0); }
  public boolean has_actualval()   { return((get_capabilities()&CAPA_ACTUALVAL)!=0); }
  public boolean has_minmaxval()   { return((get_capabilities()&CAPA_MINMAXVAL)!=0); }

  public boolean hasset_timeformat() {
    return(protocol==PROTO_FREETECWHITE || protocol==PROTO_FILE);
  }
  public boolean hasset_dateformat() {
    return(protocol==PROTO_FREETECWHITE || protocol==PROTO_FILE || protocol==PROTO_HID);
  }
  public boolean hasset_tempunit() {
    return(protocol==PROTO_FREETECWHITE || 
           protocol==PROTO_FILE || 
           protocol==PROTO_ELV || 
           protocol==PROTO_VOLTCRAFT || 
           protocol==PROTO_VOLTCRAFT_NEW || 
           protocol==PROTO_CP210X || 
           protocol==PROTO_VOLTCRAFT_WEATHER || 
	   protocol==PROTO_HID);
  }
  public boolean hasset_tempoffset() {
    return(protocol==PROTO_FREETECWHITE);
  }
  public boolean hasset_humioffset() {
    return(protocol==PROTO_FREETECWHITE);
  }

  
  
  
  public boolean hasset_presunit() {
    return(protocol==PROTO_FILE && loggertype==LTYP_THP);
  }


  public String protocolstring() {
     switch(protocol) {
     case PROTO_VOLTCRAFT:         return "Voltcraft1";
     case PROTO_VOLTCRAFT_NEW:     return "Voltcraft2";
     case PROTO_VOLTCRAFT_WEATHER: return "Voltcraft3";
     case PROTO_ELV:          return "Lascar";
     case PROTO_HID:          return "HID/NC7004/black";
     case PROTO_TFD:          return "TFD";
     case PROTO_CP210X:       return "CP210X";
     case PROTO_FILE:         return "MOUNT/FILE";
     case PROTO_FREETECWHITE: return "NC7004/white";
     default: return "?";
    }
  }

  public String typestring() {
    switch(loggertype) {
      case LTYP_TEMP:  return "Temperature";
      case LTYP_TH:    return "Temperature+Humidity";
      case LTYP_THP:   return "Temp+Humi+Pressure";
      case LTYP_VOLT:  return "Voltage";
      case LTYP_CURR:  return "Current";
      case LTYP_GAS:   return "Gas concentration";
      case LTYP_SOUND: return "Sound level";
      default: return "?";
    }
  }
  
  private String clean_string(String a, String d) {
    if(a==null) return(d);
    int i;
    char un[]=a.toCharArray();
    for(i=0;i<un.length;i++) {
      if((int)un[i]==0 || (int)un[i]==24) break;
      else if((int)un[i]==32 || (int)un[i]==9) un[i]='-';
      else if((int)un[i]>=0x30 && (int)un[i]<=0x3a) ;
      else if((int)un[i]>=0x41 && (int)un[i]<=0x5a) ;
      else if((int)un[i]==0x5f) ;
      else if((int)un[i]>=0x61 && (int)un[i]<=0x7a) ;
      else un[i]='.';
    }
    if(i==0) return(d);
    a=String.valueOf(un).substring(0,i);
    if(a.equals("")) return(d);
    return(a);
  }

  public void updateProgress(int n)         { if(!be_quiet && mActivity!=null) mActivity.updateProgress(n);}
  public void updateMessage(String s,int n) { if(!be_quiet && mActivity!=null) mActivity.updateMessage(s,n);}
  public void updateStatus(String s,int n)  { if(!be_quiet && mActivity!=null) mActivity.updateStatus(s,n);}	
  public void updateStatus(int n)           { if(!be_quiet && mActivity!=null) mActivity.updateStatus(n);}

  /* Formulas for raw data to real data conversions */

  public float raw2t(short r)  { return (float)calibration_tB+(float)r*calibration_tA; }
  public short t2raw(double p) { return (short)((p-calibration_tB)/calibration_tA); }
  public float raw2r(short r)  { return (float)calibration_rB+(float)r*calibration_rA; }
  public short r2raw(double p) { return (short)((p-calibration_rB)/calibration_rA); }
  public float raw2p(short r)  { return (float)calibration_pB+(float)r*calibration_pA; }
  public short p2raw(double p) { return (short)((p-calibration_pB)/calibration_pA); }
}

package com.drhoffmannstoolsdataloggerreader;

/* Error.java (c) 2011-2020 by Markus Hoffmann
 *
 * This file is part of USB-Dataloggerreader for Android, (c) by Markus Hoffmann 2011-2020
 * =============================================================================
 * USB-Dataloggerreader for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details.
 */ 


public class Error {
  /* Errors (config errors */
  public final static int OK=0; 	      /* OK */
  public final static int ERR=-1;	      /* ERROR failed */

  public final static int ERR_DNC=-11;        /* Device not connected*/
  public final static int ERR_CNFI=-12;       /* Could not find interface*/
  public final static int ERR_CNFE=-13;       /* Could not find endpoints*/
  public final static int ERR_DOF=-14;        /* Device open failed*/
  public final static int ERR_NOTSUP=-15;     /* Logger not supported */

  public final static int ERR_BAD=-16;        /* Config bad */
  public final static int ERR_CNONAME=-17;    /* Config bad */
  public final static int ERR_CNAME=-18;      /* Config bad */
  public final static int ERR_NUMDATA=-19;    /* Config bad */
  public final static int ERR_INTERVAL=-20;   /* Config bad */
  public final static int ERR_ILLNAME=-21;

  public final static int WARN_NOTSUP=-32;    /* Config bad */
  public final static int WARN_NOTTESTED=-33; /* Config bad */
}

Guide to contributing to USBDataloggerreader for Android
========================================================

USBDataloggerreader for Android was originally written in 2011 to read out the
USB-Dataloggers I bought to measure humidity in my new home. Many people
expressed their interest in this app, so I published a commercial version on the
Android Market (later Google Play store). In time more and more different
data logger types were supported. However in 2016 the development slowed down
and not many new features have been planned since then. Many users of the app
seemed satisfied with the functionality which is there. In 2019 I closed my
official app business and released all the apps into open source with GPL
license. So I consider the current state of the app pretty much mature. 

However, if you want to improve the app, extend it or implement an interface
for new devices, please feel free to do so. You work is much welcome. For 
example one can add more translations to other languages, make the user 
interface nicer and adapt the look-end-feel to the most recent generation of 
Android phones. Also a user manual could be useful (in addition to the in-app
help).

If you find bugs please post your issues here. 
This repository here is the right place to do so.


## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
license GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 


## Contributing with a Pull Request

The best way to contribute to this project is by making a pull-request:

1. Login with your Codeberg account or create one now
2. [Fork](https://codeberg.org/kollo/USBDataloggerreader-Android.git) the USBDataloggerreader for Android repository. 
Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the `contrib` directory if you're not sure where your contribution might fit.
5. Edit `doc/ACKNOWLEGEMENTS` and add your own name to the list of contributors under the section with the current year. Use your name, or a codeberg ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a pull-request against the USBDataloggerreader for Android repository.



## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a pull-request, then you can file an Issue. Filing an Issue will help us
see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/USBDataloggerreader-Android/issues/new?issue) now!


## Thanks

We are very grateful for your support. With your help, this implementation
will be a great project. 

